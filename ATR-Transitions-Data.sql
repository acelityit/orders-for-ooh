
DROP TABLE IF EXISTS reporting_comops.hero_Round_ATR_Transitions;


CREATE TABLE reporting_comops.hero_Round_ATR_Transitions stored as parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/reporting_comops.db/hero_Round_ATR_Transitions/' AS
SELECT 
      A.rpt_month as MONTH_ID,
      CAST(concat(strleft(CAST(A.rpt_month as string),4),'-',strright(CAST(A.rpt_month as string),2),'-','01') as TIMESTAMP) AS RPT_MONTH,
      A.geography,
      A.REGION,
      A.rvp_name  AS RVP_NAME,
      A.DISTRICT,
      A.dm_name AS DM_NAME,
      A.territory_code AS TERRITORY, 
      A.salesrep_name  AS REP_NAME,
      A.customer_shipto_market_category AS ACUTE_MKT_CATEGORY,
      A.transition_facility_name AS ACUTE_CUSTOMER_NAME,
      A.transition_facility_account_number AS ACUTE_CUSTOMER_ACCOUNT_NUMBER,
      A.previous_ro AS ACUTE_RO_NUMBER,
      A.ro_number AS POST_ACUTE_RO_NUMBER,
      B.customer_shipto_market_category AS POST_ACUTE_MKT_CATEGORY, 
      B.customer_shipto_account_number AS POST_ACUTE_CUSTOMER_ACCOUNT_NUMBER,
      B.customer_shipto_site_use_id AS POST_ACUTE_SHIPTO_SITE_USE_ID, 
      B.customer_shipto_city AS POST_ACUTE_CUSTOMER_CITY,
      CAST(B.customer_shipto_zip_code AS STRING) AS POST_ACUTE_ZIP_CODE,
      CAST(B.customer_shipto_zip4 AS STRING) AS POST_ACUTE_ZIP4,
      CONCAT(CAST(B.customer_shipto_zip_code AS STRING),"-", CAST(B.customer_shipto_zip4 AS STRING)) AS POST_ACUTE_ZIP,
      B.customer_shipto_state AS POST_ACUTE_CUSTOMER_STATE, 
      B.customer_shipto_name AS POST_ACUTE_CUSTOMER_NAME,
      CAST(SUM(A.transitions) AS DECIMAL(38,10)) AS transitions,
      CAST(2 AS INT) flag,
      CASE
                     WHEN A.geography='EAST' THEN 'Patton,Jennifer'
                     WHEN A.geography='WEST' THEN  'Moller,Kim'
                     ELSE  'Bauer,Chris'
                 END nsl_name
FROM 

(
SELECT 
            f.rpt_month,
            f.order_key, 
            f.acute_order_key, 
            f.customer_shipto_key, 
            f.customer_billto_key, 
            f.created_orders, 
            f.cancelled_orders, 
            f.commissionable_orders, 
            f.transitions,
            s.customer_shipto_account_number transition_facility_account_number,
            s.customer_shipto_name transition_facility_name,
            s.customer_shipto_city transition_facility_city,
            s.customer_shipto_zip_code transition_facility_zip_code,
            s.customer_shipto_zip4 transition_facility_zip, 
            pd.smr_product_group,pd.product_brand,pd.product_sub_brand,pd.product_sku,
            p.rop_number, 
            p.rop_effective_timestamp, 
            p.rop_end_timestamp,
            p.previous_ro, 
            p.ro_number, 
            p.ro_ship_to_site_use_id, 
            p.prev_ro_ship_to_site_use_id, 
            p.rop_rec_flag,
            t.customer_shipto_market_category,
            t.geography,
            t.region region,
            t.rvp_name,
            t.district, 
            t.dm_name,
            t.territory_code, 
            t.sales_credit_type_code,
            t.salesrep_name
      FROM  aim.aim_ordrev_mthly_snapshot_fact f,
            aim.aim_orders_dim p,
            (SELECT * FROM aim.aim_product_dim WHERE etl_delete_flag <> 'Y') pd, 
            aim.aim_customer_zipterr_xref_transposed_vw t,
            
            aim.aim_customer_shipto_dim s

      WHERE f.order_key = p.order_key
            AND f.product_key=pd.product_key
            AND f.transition_facility_key = t.customer_key
            AND t.customer_key=s.customer_shipto_key
            AND t.customer_shipto_market_category ='ACUTE'
            AND t.sales_credit_type_code='ATR'
            AND t.region LIKE 'A%' AND t.region <>'A0'
            AND f.rpt_month >= 201701
            AND f.load_source = 'ORDERS'
            AND f.transitions !=0
            AND s.customer_shipto_market_segment IN (1, 2, 4 , 5, 6, 17, 20, 40, 89)
      ) A

LEFT JOIN


      (
        SELECT 
            customer_shipto_key,
            customer_shipto_market_category, 
            customer_shipto_account_number,
            customer_shipto_site_use_id,
            customer_shipto_city, 
            customer_shipto_zip_code,
            customer_shipto_zip4,
            customer_shipto_state, 
            customer_shipto_name
        FROM aim.aim_customer_shipto_dim
      ) B
ON A.customer_shipto_key = B.customer_shipto_key
GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23;


compute stats reporting_comops.hero_Round_ATR_Transitions;


DROP TABLE IF EXISTS reporting_comops.hero_round_ATR_TRANS_initial_layer;


CREATE TABLE reporting_comops.hero_round_ATR_TRANS_initial_layer stored AS parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/reporting_comops.db/hero_round_ATR_TRANS_initial_layer/' AS
SELECT '-' AS item_description,
     '-' AS inventory_item_id,
     '-' AS roa_pm_fk,
     '-' AS ship_date,
     '-' AS description,
     nvl(svc.svc_pk,
         '-') svc_pk,
        nvl(svc.svc_description,
            '-') svc_description,
            nvl(ro.geography,'-') geography,
           nvl(ro.region,
               '-') region,
              nvl(ro.district,
                  '-') district,
                 nvl(ro.TERRITORY,'-') TERRITORY_CODE,
                     cast(null as string) territory_name,
                    nvl(ro.rep_name,
                           '-') salesrep_name_align,
                          nvl(ro.rvp_name,
                              '-') regionvicepresident_name,
                             nvl(ro.dm_name,
                                 '-') districtmanager_name,
                                'ATR' jca_role,
                                      aimcust.customer_shipto_market_category AS market_category,
                                      CASE
                                          WHEN ra_customer_number = 'null' THEN '-'
                                          WHEN ra_customer_number IS NULL THEN '-'
                                          ELSE ra_customer_number
                                      END ra_customer_number,
                                      nvl(cst.cst_pk,
                                          0) cst_pk,
                                         nvl(cst.cst_name,
                                             '-') cst_name,
                                            CASE
                                                WHEN aimcust.customer_shipto_market_category IN('HOME') THEN CASE
                                                                                                                 WHEN concat(cgv.caregiver_account_number, ' ', '-', ' ', cgv.caregiver_name_1)='null' THEN 'NO CAREGIVER AVAILABLE'
                                                                                                                 WHEN concat(cgv.caregiver_account_number, ' ', '-', ' ', cgv.caregiver_name_1) IS NULL THEN 'NO CAREGIVER AVAILABLE'
                                                                                                                 ELSE concat(cgv.caregiver_account_number, ' ', '-', ' ', cgv.caregiver_name_1)
                                                                                                             END
                                                ELSE CASE
                                                         WHEN concat(ra_customer_number, ' ', '-', ' ', cst_name)='null' THEN 'NO FACILITY AVAILABLE'
                                                         WHEN concat(ra_customer_number, ' ', '-', ' ', cst_name) IS NULL THEN 'NO FACILITY AVAILABLE'
                                                         ELSE concat(ra_customer_number, ' ', '-', ' ', cst_name)
                                                     END
                                            END Facility_Caregiver,
                                            CASE
                                                WHEN aimcust.customer_shipto_market_category IN('HOME') THEN CASE
                                                                                                                 WHEN cgv.caregiver_account_number ='null' THEN 'NO CAREGIVER AVAILABLE'
                                                                                                                 WHEN cgv.caregiver_account_number IS NULL THEN 'NO CAREGIVER AVAILABLE'
                                                                                                                 ELSE cgv.caregiver_account_number 
                                                                                                             END
                                                                                                                 
                                              ELSE  CASE
                                                         WHEN  ra_customer_number ='null' THEN 'NO FACILITY AVAILABLE'
                                                         WHEN  ra_customer_number IS NULL THEN 'NO FACILITY AVAILABLE'
                                                         ELSE  ra_customer_number
                                                     END
                                            END Facility_Number,
                                            CASE
                                                WHEN aimcust.customer_shipto_market_category IN('HOME') THEN CASE
                                                                                                                 WHEN cgv.caregiver_physical_address_1 ='null' THEN '-'
                                                                                                                 WHEN cgv.caregiver_physical_address_1 IS NULL THEN '-'
                                                                                                                 ELSE cgv.caregiver_physical_address_1
                                                                                                             END
                                                ELSE CASE
                                                         WHEN cst.cst_ship_address_1='null' THEN '-'
                                                         WHEN cst.cst_ship_address_1 IS NULL THEN '-'
                                                         ELSE cst.cst_ship_address_1
                                                     END
                                            END Facility_Caregiver_Address,
                                            CASE
                                                WHEN aimcust.customer_shipto_market_category IN('HOME') THEN CASE
                                                                                                                 WHEN cgv.caregiver_physical_city ='null' THEN '-'
                                                                                                                 WHEN cgv.caregiver_physical_city IS NULL THEN '-'
                                                                                                                 ELSE cgv.caregiver_physical_city
                                                                                                             END
                                                ELSE CASE
                                                         WHEN cst.cst_ship_city ='null' THEN '-'
                                                         WHEN cst.cst_ship_city IS NULL THEN '-'
                                                         ELSE cst.cst_ship_city
                                                     END
                                            END Facility_Caregiver_City,
                                            CASE
                                                WHEN aimcust.customer_shipto_market_category IN('HOME') THEN CASE
                                                                                                                 WHEN cgv.caregiver_physical_zip ='null' THEN '-'
                                                                                                                 WHEN cgv.caregiver_physical_zip IS NULL THEN '-'
                                                                                                                 ELSE cgv.caregiver_physical_zip
                                                                                                             END
                                                ELSE CASE
                                                         WHEN cst.cst_ship_zip_code ='null' THEN '-'
                                                         WHEN cst.cst_ship_zip_code IS NULL THEN '-'
                                                         ELSE cst.cst_ship_zip_code
                                                     END
                                            END Facility_Caregiver_Zip,
                                            CASE
                                                WHEN aimcust.customer_shipto_market_category IN('HOME') THEN CASE
                                                                                                                 WHEN cgv.caregiver_physical_phone='null' THEN '-'
                                                                                                                 WHEN cgv.caregiver_physical_phone IS NULL THEN '-'
                                                                                                                 ELSE cgv.caregiver_physical_phone
                                                                                                             END
                                                ELSE CASE
                                                         WHEN cst.cst_contact_phone_number ='null' THEN '-'
                                                         WHEN cst.cst_contact_phone_number IS NULL THEN '-'
                                                         ELSE cst.cst_contact_phone_number
                                                     END
                                            END Facility_Caregiver_Phone_Number,
                                            CASE
                                                WHEN cgv.caregiver_physical_address_2 = 'null' THEN '-'
                                                WHEN cgv.caregiver_physical_address_2 IS NULL THEN '-'
                                                ELSE cgv.caregiver_physical_address_2
                                            END name_2,
                                            nvl(cgv.caregiver_bus_type,
                                                '-') caregiver_type,
                                               CASE
                                                   WHEN concat(phy.physician_last_name, ', ', phy.physician_first_name) IS NULL THEN '-'
                                                   WHEN concat(phy.physician_last_name, ', ', phy.physician_first_name) ='null, null' THEN '-'
                                                   WHEN concat(phy.physician_last_name, ', ', phy.physician_first_name)=', ' THEN '-'
                                                   ELSE concat(phy.physician_last_name, ', ', phy.physician_first_name)
                                               END physician_name,
                                               nvl(cast(ro.ro_npi AS string),
                                                   '-') ro_npi,
                                                  CASE
                                                      WHEN phy.physician_bus_mail_phone='null' THEN '-'
                                                      WHEN phy.physician_bus_mail_phone='nul-l-' THEN '-'
                                                      WHEN phy.physician_bus_mail_phone IS NULL THEN '-'
                                                      ELSE phy.physician_bus_mail_phone
                                                  END ro_doctor_phone,
                                                  CASE
                                                      WHEN aimcust.customer_shipto_market_category IN('ACUTE',
                                                                                                      'EXTENDED') THEN nvl(concat(ro.ro_patient_last_name, ", ", ro.ro_patient_first_name), '-')
                                                      WHEN aimcust.customer_shipto_market_category IN('HOME') THEN nvl(cst.cst_name, '-')
                                                      ELSE '-'
                                                  END patient_name,
                                                  CASE
                                                      WHEN aimcust.customer_shipto_market_category IN('ACUTE',
                                                                                                      'EXTENDED') THEN '-'
                                                      WHEN aimcust.customer_shipto_market_category IN('HOME') THEN nvl(cst.cst_ship_address_1, '-')
                                                      ELSE '-'
                                                  END patient_adress,
                                                  CASE
                                                      WHEN aimcust.customer_shipto_market_category IN('ACUTE',
                                                                                                      'EXTENDED') THEN '-'
                                                      WHEN aimcust.customer_shipto_market_category IN('HOME') THEN nvl(cst.cst_ship_city, '-')
                                                      ELSE '-'
                                                  END patient_city,
                                                  CASE
                                                      WHEN aimcust.customer_shipto_market_category IN('ACUTE',
                                                                                                      'EXTENDED') THEN '-'
                                                      WHEN aimcust.customer_shipto_market_category IN('HOME') THEN nvl(cst.cst_ship_zip_code, '-')
                                                      ELSE '-'
                                                  END patient_zip,
                                                  CASE
                                                      WHEN aimcust.customer_shipto_market_category IN('ACUTE',
                                                                                                      'EXTENDED') THEN CASE
                                                                                                                           WHEN (from_unixtime(unix_timestamp(TO_DATE(ro.ro_patient_dob), 'yyyy-MM-dd'), 'MM-dd-yyyy'))='null' THEN '-'
                                                                                                                           WHEN (from_unixtime(unix_timestamp(TO_DATE(ro.ro_patient_dob), 'yyyy-MM-dd'), 'MM-dd-yyyy')) IS NULL THEN '-'
                                                                                                                           ELSE from_unixtime(unix_timestamp(TO_DATE(ro.ro_patient_dob), 'yyyy-MM-dd'), 'MM-dd-yyyy')
                                                                                                                       END
                                                      ELSE '-'
                                                  END birth,
                                                  CASE
                                                      WHEN aimcust.customer_shipto_market_category IN('ACUTE',
                                                                                                      'EXTENDED') THEN CASE
                                                                                                                           WHEN ro_patient_location='null' THEN '-'
                                                                                                                           WHEN ro_patient_location IS NULL THEN '-'
                                                                                                                           ELSE ro_patient_location
                                                                                                                       END
                                                      ELSE '-'
                                                  END ro_patient_location,
                                                  CASE
                                                      WHEN aimcust.customer_shipto_market_category IN('ACUTE',
                                                                                                      'EXTENDED') THEN CASE
                                                                                                                           WHEN ro_patient_phone='null' THEN '-'
                                                                                                                           WHEN ro_patient_phone IS NULL THEN '-'
                                                                                                                           ELSE ro_patient_phone
                                                                                                                       END
                                                      ELSE '-'
                                                  END ro_patient_phone,
                                                  nvl(prd.prd_description,
                                                      '-') prd_description,
                                                     CASE
                                                         WHEN rop_ar_serial_number_fk='null' THEN '-'
                                                         WHEN rop_ar_serial_number_fk IS NULL THEN '-'
                                                         ELSE rop_ar_serial_number_fk
                                                     END rop_ar_serial_number_fk,
                                                     nvl(ro.POST_ACUTE_RO_NUMBER,
                                                         0) ro_number,
                                                        nvl(rop.rop_status_code,
                                                            '-') rop_status_code,
                                                            
                                                           CASE
                                                               WHEN ro_purchase_order_number='null' THEN '-'
                                                               WHEN ro_purchase_order_number IS NULL THEN '-'
                                                               ELSE ro_purchase_order_number
                                                           END ro_purchase_order_number,
                                                           nvl(cast(rop.rop_placed_time_stamp AS string),
                                                               '-') rop_placed_time_stamp,
                                                              nvl(cast(rop.rop_last_round_time_stamp AS string),
                                                                  '-') rop_last_round_time_stamp,
                                                                 nvl(cast(rop.rop_dc_time_stamp AS string),
                                                                     '-') rop_dc_time_stamp,
                                                                    nvl(ro.ro_customer_group,
                                                                        '-') ro_customer_group,
                                                                       nvl(rac.customer_number,
                                                                           '-') customer_number,
                                                                          nvl(rac.customer_name,
                                                                              '-') customer_name,
                                                                             CASE
                                                                                 WHEN rop_vac_transitionable_flag='null' THEN '-'
                                                                                 WHEN rop_vac_transitionable_flag IS NULL THEN '-'
                                                                                 ELSE rop_vac_transitionable_flag
                                                                             END rop_vac_transitionable_flag,
                                                                             nvl(rop.rop_pk,
                                                                                 0) rop_pk,
                                                                                CASE
                                                                                    WHEN rop_claims_release_emp_fk ='null' THEN '-'
                                                                                    WHEN rop_claims_release_emp_fk IS NULL THEN '-'
                                                                                    ELSE rop_claims_release_emp_fk
                                                                                END CSR,
                                                                                CASE
                                                                                    WHEN cst_ship_address_1='null' THEN '-'
                                                                                    WHEN cst_ship_address_1 IS NULL THEN '-'
                                                                                    ELSE cst_ship_address_1
                                                                                END cst_ship_address_1,
                                                                                CASE
                                                                                    WHEN cst_ship_address_2='null' THEN '-'
                                                                                    WHEN cst_ship_address_2 IS NULL THEN '-'
                                                                                    ELSE cst_ship_address_2
                                                                                END cst_ship_address_2,
                                                                                nvl(concat(cast(ro.ro_linked_ship_to_cst_fk AS string), ' ', '-', ' ', ro.ro_linked_facility_name),
                                                                                    '-') Transitioned_Facility,
                                                                                   CASE
                                                                                       WHEN ro.ro_linked_facility_address1= 'null' THEN '-'
                                                                                       WHEN ro.ro_linked_facility_address1 IS NULL THEN '-'
                                                                                       ELSE ro.ro_linked_facility_address1
                                                                                   END Transitioned_Facility_address,
                                                                                   CASE
                                                                                       WHEN ro.ro_linked_facility_city= 'null' THEN '-'
                                                                                       WHEN ro.ro_linked_facility_city IS NULL THEN '-'
                                                                                       ELSE ro.ro_linked_facility_city
                                                                                   END Transitioned_Facility_city,
                                                                                   CASE
                                                                                       WHEN ro.ro_linked_facility_zip_code= 'null' THEN '-'
                                                                                       WHEN ro.ro_linked_facility_zip_code IS NULL THEN '-'
                                                                                       ELSE ro.ro_linked_facility_zip_code
                                                                                   END Transitioned_Facility_zip,
                                                                                   nvl(cast(rop.rop_stop_bill_time_stamp AS string),
                                                                                       '-') rop_stop_bill_time_stamp,
                                                                                      nvl(cast(rop.rop_start_bill_time_stamp AS string),
                                                                                          '-') rop_start_bill_time_stamp,
                                                                                         nvl(cast(rop.rop_created_time_stamp AS string),
                                                                                             '-') rop_created_time_stamp,
                                                                                            nvl(cast(rop.rop_void_verify_time_stamp AS string),
                                                                                                '-') rop_void_verify_time_stamp,
                                                                                               CASE
                                                                                                   WHEN (rop.rop_avail_for_ship_date IS NULL
                                                                                                         AND rop.rop_void_verify_time_stamp IS NULL)
                                                                                                        OR (rop.rop_placed_time_stamp IS NULL
                                                                                                            AND rop.rop_void_verify_time_stamp IS NULL) THEN 'PENDING'
                                                                                                   WHEN rop.rop_placed_time_stamp IS NOT NULL
                                                                                                        AND rop.rop_void_verify_time_stamp IS NULL
                                                                                                        AND rop.rop_stop_bill_time_stamp IS NULL THEN 'PLACED'
                                                                                                   WHEN rop.rop_void_verify_time_stamp IS NOT NULL THEN 'CANCELLED'
                                                                                                   WHEN rop.rop_placed_time_stamp IS NOT NULL
                                                                                                        AND rop.rop_void_verify_time_stamp IS NULL
                                                                                                        AND rop.rop_stop_bill_time_stamp IS NOT NULL THEN 'CLOSED'
                                                                                               END order_status,
                                                                                               CASE
                                                                                                   WHEN concat(wo_requestor_account_number, ' ', '-', ' ', wo_caller_company)='null,null' THEN '-'
                                                                                                   WHEN concat(wo_requestor_account_number, ' ', '-', ' ', wo_caller_company) IS NULL THEN '-'
                                                                                                   WHEN (wo_requestor_account_number='null'
                                                                                                         OR wo_requestor_account_number IS NULL)
                                                                                                        AND (wo_caller_company<>'null'
                                                                                                             OR wo_caller_company IS NOT NULL) THEN wo_caller_company
                                                                                                   WHEN (wo_requestor_account_number<>'null'
                                                                                                         OR wo_requestor_account_number IS NOT  NULL)
                                                                                                        AND (wo_caller_company='null'
                                                                                                             OR wo_caller_company IS NULL) THEN wo_requestor_account_number
                                                                                                   ELSE concat(wo_requestor_account_number, ' ', '-', ' ', wo_caller_company)
                                                                                               END requestor_name,
                                                                                               CASE
                                                                                                   WHEN wo.wo_requestor_address_1= 'null' THEN '-'
                                                                                                   WHEN wo.wo_requestor_address_1 IS NULL THEN '-'
                                                                                                   ELSE wo.wo_requestor_address_1
                                                                                               END requestor_address,
                                                                                               CASE
                                                                                                   WHEN wo.wo_requestor_city= 'null' THEN '-'
                                                                                                   WHEN wo.wo_requestor_city IS NULL THEN '-'
                                                                                                   ELSE wo.wo_requestor_city
                                                                                               END requestor_city,
                                                                                               CASE
                                                                                                   WHEN wo.wo_requestor_zip_code= 'null'THEN '-'
                                                                                                   WHEN wo.wo_requestor_zip_code IS NULL THEN '-'
                                                                                                   ELSE wo.wo_requestor_zip_code
                                                                                               END requestor_zip,
                                                                                               CASE
                                                                                                   WHEN wo.wo_caller_phone= 'null' THEN '-'
                                                                                                   WHEN wo.wo_caller_phone IS NULL THEN '-'
                                                                                                   ELSE wo.wo_caller_phone
                                                                                               END requestor_phone_number,
                                                                                               CASE
                                                                                                   WHEN wo_rs_tracking_number='null' THEN '-'
                                                                                                   WHEN wo_rs_tracking_number IS NULL THEN '-'
                                                                                                   ELSE wo_rs_tracking_number
                                                                                               END tracking_number,
                                                                                               CASE
                                                                                                   WHEN payor.customer_billto_name='null' THEN '-'
                                                                                                   WHEN payor.customer_billto_name IS NULL THEN '-'
                                                                                                   ELSE payor.customer_billto_name
                                                                                               END payer_name,
                                                                                               nvl(ro.ro_status_code,
                                                                                                   '-') ro_status_code,
                                                                                                   CASE
                                                                                                      WHEN aimcust.mmc_flag='null' THEN '-'
                                                                                                      WHEN aimcust.mmc_flag IS NULL THEN '-'
                                                                                                      ELSE aimcust.mmc_flag
                                                                                                  END mmc_flag,
                                                                                                  
                                                                                                  CASE
                                                                                                      WHEN rop.rop_consign = 'HOM' THEN 'Y'
                                                                                                      ELSE 'N'
                                                                                                  END Ready_Care_Flag,
                                                                                                  CASE
                                                                                                      WHEN aimcust.customer_sva_enabled_flag='null' THEN '-'
                                                                                                      WHEN aimcust.customer_sva_enabled_flag IS NULL THEN '-'
                                                                                                      ELSE aimcust.customer_sva_enabled_flag
                                                                                                  END customer_sva_enabled_flag,
                                                                                                  CASE
                                                                                                      WHEN ((aimcust.customer_sva_enabled_flag = 'Y')
                                                                                                            AND (payor.customer_billto_name LIKE '%MARATHON%'
                                                                                                                 OR payor.customer_billto_name LIKE '%VA MEDICAL%'
                                                                                                                 OR payor.customer_billto_name LIKE '%V A MEDICAL%'
                                                                                                                 OR payor.customer_billto_name LIKE '%TRICAR%')) THEN 'Y'
                                                                                                      ELSE 'N'
                                                                                                  END VA_System,
                                                                                                  CASE
                                                                                                      WHEN rop.rop_free_reason_code IN ('60',
                                                                                                                                        '45') THEN 'Y'
                                                                                                      ELSE 'N'
                                                                                                  END charity_care_Status,
                                                                                                  CASE
                                                                                                      WHEN ro.ro_status_code IN ('SNC',
                                                                                                                                 'VNC',
                                                                                                                                 'MNC',
                                                                                                                                 'HNC',
                                                                                                                                 'ANC') THEN 'Y'
                                                                                                      ELSE 'N'
                                                                                                  END cba ,
                                                                                                  CASE
                                                                                                      WHEN rop.rop_created_time_stamp>rop.rop_avail_for_ship_date THEN 'Rebill'
                                                                                                      ELSE '-'
                                                                                                  END Rebill,
                                                                                                  CASE
                                                                                                      WHEN wo_code.ct_description ='null' THEN '-'
                                                                                                      WHEN wo_code.ct_description IS NULL THEN '-'
                                                                                                      ELSE wo_code.ct_description
                                                                                                  END wo_cancel_code_description,
                                                                                                  CASE
                                                                                                      WHEN void_code.ct_description ='null' THEN '-'
                                                                                                      WHEN void_code.ct_description IS NULL THEN '-'
                                                                                                      ELSE void_code.ct_description
                                                                                                  END ROP_void_code_description,
                                                                                                  ro.nsl_name
FROM
(SELECT a.*,
        b.*
 FROM reporting_comops.hero_Round_ATR_Transitions a
 LEFT JOIN hero.genesis_rental_order b ON b.ro_pk = a.POST_ACUTE_RO_NUMBER) ro,

(SELECT *
 FROM hero.genesis_customer
 WHERE cst_pk NOT IN (9999999999,
                      0,
                      5555555555,
                      8888888888,
                      216200) ) cst,
   hero.genesis_rental_order_product rop
INNER JOIN
(SELECT *
 FROM hero.genesis_service_center
 WHERE svc_pk NOT IN ('341',
                      '405',
                      '0') )svc on(rop.ROP_SVC_FK = svc.SVC_PK)
LEFT OUTER JOIN hero.genesis_ro_claim_data cd on(ro.RO_PK=cd.ROC_RO_FK)
LEFT JOIN aim.aim_caregiver_dim cgv ON cgv.caregiver_mstrid = cd.roc_hha_smg_mstr_id_fk
LEFT OUTER JOIN hero.genesis_product prd on(rop.ROP_PRODUCT_USED_FK = prd.PRD_PRODUCT_PK)
LEFT OUTER JOIN
(SELECT wo_rop_fk,
        wo_requestor_account_number,
        wo_caller_company,
        wo_rs_tracking_number,
        wo_requestor_address_1,
        wo_requestor_city,
        wo_requestor_zip_code,
        wo_caller_phone,
        wo_cancel_reason,
        wo_type_code
 FROM hero.genesis_work_order
 WHERE (wo_requestor_account_number<>'null'
        OR wo_requestor_account_number IS NULL)
   AND wo_type_code='DEL'
 GROUP BY 1,2,3,4,5,6,7,8,9,10) wo 
 ON rop.ROP_PK = wo.WO_ROP_FK
LEFT OUTER JOIN hero.genesis_employee e on(e.EMP_PK = cst_last_order_emp_fk)
LEFT OUTER JOIN hero.genesis_ra_site_uses_all_snap rss ON (ro.RO_BILL_TO_SITE_USE_ID = rss.SITE_USE_ID)
LEFT OUTER JOIN hero.genesis_ra_addresses_all_snap ras ON (rss.ADDRESS_ID = ras.ADDRESS_ID)
LEFT OUTER JOIN hero.genesis_ra_customers_snap rac on(ras.CUSTOMER_ID = rac.CUSTOMER_ID)
LEFT OUTER JOIN

(SELECT *
 FROM aim.aim_customer_shipto_dim
 WHERE customer_shipto_status NOT LIKE 'Pending'
   AND customer_shipto_indirect_flag = 'N') aimcust ON cst.ra_shipto_site_id = aimcust.customer_shipto_site_use_id

LEFT OUTER JOIN

(SELECT *
 FROM aim.aim_customer_zipterr_xref_transposed_vw t
 WHERE ((t.sales_credit_type_code IN ('TDV','TMV','TSV') AND t.primary_flag='Y')
     OR (t.sales_credit_type_code ='ATR' and t.customer_shipto_market_category='ACUTE')) AND t.region LIKE 'A%' AND t.region <>'A0') t ON t.customer_key =aimcust.customer_shipto_key

LEFT OUTER JOIN aim.aim_customer_billto_dim payor ON ro.ro_bill_to_site_use_id=payor.customer_billto_site_use_id
LEFT OUTER JOIN hero.genesis_code wo_code ON ((upper(wo_code.ct_column) LIKE '%WO_CANCEL_REASON_ALL%')
                                            AND wo_code.ct_code = wo_cancel_reason
                                            AND wo.wo_rop_fk=rop.rop_pk)
LEFT OUTER JOIN hero.genesis_code void_code on((upper(void_code.ct_column) IN ('ROP_VOID_TYPE',
                                                                             'ROP_VOID_TYPE_SUB_REQ'))
                                             AND void_code.ct_code = rop_void_type_code
                                             AND wo.wo_rop_fk=rop.rop_pk)
LEFT OUTER JOIN aim.aim_physician_dim phy ON phy.physician_npi = ro.ro_npi
WHERE ro.RO_CST_FK=cst.CST_PK
AND ro.RO_PK=rop.ROP_RO_FK
AND (rop.rop_stop_bill_time_stamp IS NULL
     OR round(datediff(now(), rop.rop_stop_bill_time_stamp)) <= 30)
AND rop.ROP_PRODUCT_USED_FK LIKE 'WND%'
AND (svc.svc_pk IS NOT NULL
     OR svc_pk<>'000')
AND (rop.rop_void_verify_time_stamp IS NULL
     OR round(datediff(now(), rop.rop_void_verify_time_stamp)) <= 30)
AND (aimcust.customer_shipto_market_segment NOT IN (51,
                                                    52,
                                                    53,
                                                    99)
     OR aimcust.customer_shipto_market_segment IS NULL)
AND ((concat(ro.ro_patient_last_name, ", ", ro.ro_patient_first_name)) NOT LIKE '%UNIT %'
     AND (concat(ro.ro_patient_last_name, ", ", ro.ro_patient_first_name)) NOT LIKE '%PLUS%'
     AND (concat(ro.ro_patient_last_name, ", ", ro.ro_patient_first_name)) NOT LIKE '%CUST%')
GROUP BY 1,	2,	3,	4,	5,	6,	7,	8,	9,	10,	11,	12,	13,	14,	15,	16,	17,	18,	19,	20,	21,	22,	23,	24,	25,	26,	27,	28,	29,	30,	31,	32,	33,	34,	35,	36,	37,	38,	39,	40,
	41,	42,	43,	44,	45,	46,	47,	48,	49,	50,	51,	52,	53,	54,	55,	56,	57,	58,	59,	60,	61,	62,	63,	64,	65,	66,	67,	68,	69,	70,	71,	72,	73,	74,	75,	76,77,78,79,80,81;

compute stats reporting_comops.hero_round_ATR_TRANS_initial_layer;
