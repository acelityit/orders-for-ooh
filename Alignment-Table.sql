DROP TABLE IF EXISTS reporting_comops.aim_order_rvp_dm_rep_names;
CREATE TABLE reporting_comops.aim_order_rvp_dm_rep_names stored as parquet 
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/reporting_comops.db/aim_order_rvp_dm_rep_names/' AS
SELECT sales_credit_type_code,
       region,
        district,
        territory_code,
        rvp_name,
        dm_name,
        salesrep_name,
        'TERRITORY' rollup,
        --add isr and tsr        
FROM aim.aim_customer_zipterr_xref_transposed_vw 
WHERE sales_credit_type_code IN ('TDV','TSV','ATR','TMV') 
AND territory_code NOT LIKE '%999%' AND region LIKE 'A%' AND region <>'A0'
GROUP BY 1,2,3,4,5,6,7,8