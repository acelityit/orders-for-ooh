
DROP TABLE IF EXISTS reporting_comops.aim_asset_initial_layer;


CREATE TABLE reporting_comops.aim_asset_initial_layer stored as parquet
 location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/reporting_comops.db/aim_asset_initial_layer/' AS
SELECT CASE
         WHEN prd.prd_description='null' THEN '-'
         WHEN prd.prd_description IS NULL THEN '-'
         ELSE prd.prd_description
     END product,
     nvl(ar.ar_dpt_fk,
         0) ar_dept_fk,
        CASE
            WHEN ar.ar_svc_fk='null' THEN '-'
            WHEN ar.ar_svc_fk IS NULL THEN '-'
            ELSE ar.ar_svc_fk
        END ar_svc_fk,
        CASE
            WHEN skl.skl_short_description='null' THEN '-'
            WHEN skl.skl_short_description IS NULL THEN '-'
            ELSE skl.skl_short_description
        END skl_short_description,
        nvl(cst.cst_pk,
            0) cst_pk,
           CASE
               WHEN cst.ra_customer_number='null' THEN '-'
               WHEN cst.ra_customer_number IS NULL THEN '-'
               ELSE cst.ra_customer_number
           END facility_number,
           t.geography  geography,
           t.region region,
           t.district,
           nvl(t.territory_code,'-') territory_code,
           cast(null as string) territory_name,
           cast(null as string) salesrep_id,
           CASE
                  WHEN t.salesrep_name='null' THEN '-'
                  WHEN t.salesrep_name IS NULL THEN '-'
                  ELSE t.salesrep_name
              END salesrep_name,
              CASE
                  WHEN t.rvp_name='null' THEN '-'
                  WHEN t.rvp_name IS NULL THEN '-'
                  ELSE t.rvp_name
              END regionvicepresident_name,
              CASE
                  WHEN t.dm_name='null' THEN '-'
                  WHEN t.dm_name IS NULL THEN '-'
                  ELSE t.dm_name
              END districtmanager_name,                  
              nvl(t.sales_credit_type_code,'-') jca_role ,
                 CASE
                     WHEN ucase(ar.ar_serial_number_pk) ='null' THEN '-'
                     WHEN ucase(ar.ar_serial_number_pk) IS NULL THEN '-'
                     ELSE ucase(ar.ar_serial_number_pk)
                 END unit_number,
                 CASE
                     WHEN ar.ar_barcode ='null' THEN '-'
                     WHEN ar.ar_barcode IS NULL THEN '-'
                     ELSE ar.ar_barcode
                 END barcode_number,
                 CASE
                     WHEN cast(ar.ar_movement_time_stamp AS string) ='null' THEN '-'
                     WHEN cast(ar.ar_movement_time_stamp AS string) IS NULL THEN '-'
                     ELSE cast(ar.ar_movement_time_stamp AS string)
                 END last_movement_date,
                 CASE
                     WHEN dept.dpt_description='null' THEN '-'
                     WHEN dept.dpt_description IS NULL THEN '-'
                     ELSE dpt_description
                 END dpt_description,
                 CASE
                     WHEN svc.svc_pk='null' THEN '-'
                     WHEN svc.svc_pk IS NULL THEN '-'
                     ELSE svc.svc_pk
                 END svc_pk,
                 CASE
                     WHEN svc.svc_address_1='null' THEN '-'
                     WHEN svc.svc_address_1 IS NULL THEN '-'
                     ELSE svc.svc_address_1
                 END svc_address_1,
                 CASE
                     WHEN svc.svc_address_2='null' THEN '-'
                     WHEN svc.svc_address_2 IS NULL THEN '-'
                     ELSE svc.svc_address_2
                 END svc_address_2,
                 CASE
                     WHEN svc.svc_city='null' THEN '-'
                     WHEN svc.svc_city IS NULL THEN '-'
                     ELSE svc.svc_city
                 END svc_city,
                 CASE
                     WHEN svc.svc_county='null' THEN '-'
                     WHEN svc.svc_county IS NULL THEN '-'
                     ELSE svc.svc_county
                 END svc_county,
                 CASE
                     WHEN svc.svc_state='null' THEN '-'
                     WHEN svc.svc_state IS NULL THEN '-'
                     ELSE svc.svc_state
                 END svc_state,
                 CASE
                     WHEN svc.svc_zip_code='null' THEN '-'
                     WHEN svc.svc_zip_code IS NULL THEN '-'
                     ELSE svc.svc_zip_code
                 END svc_zip_code,
                 CASE
                     WHEN svc.svc_region_code='null' THEN '-'
                     WHEN svc.svc_region_code IS NULL THEN '-'
                     ELSE svc.svc_region_code
                 END svc_region_code,
                 CASE
                     WHEN concat(cst.ra_customer_number, ' ', '-', ' ', skl.skl_description)='null' THEN '-'
                     WHEN concat(cst.ra_customer_number, ' ', '-', ' ', skl.skl_description) IS NULL THEN '-'
                     ELSE concat(cst.ra_customer_number, ' ', '-', ' ', skl.skl_description)
                 END facility_name,
                 CASE
                     WHEN svc.svc_description ='null' THEN '-'
                     WHEN svc.svc_description IS NULL THEN '-'
                     ELSE svc.svc_description
                 END svc,
                 datediff(now(),
                          ar.ar_movement_time_stamp)+1 AS "Days_Aged",
                 CASE
                     WHEN ar.ar_barcode LIKE '%RPLN%' THEN "KCI Repair Loaner"
                     WHEN prd.prd_product_pk LIKE '%WNDSL%' THEN "KCI Repair Loaner"
                     WHEN prd.prd_product_pk LIKE '%WND%CO%' THEN "Owned Unit"
                     WHEN prd.prd_product_pk LIKE '%WNDINC%' THEN "Owned Unit"
                     WHEN prd.prd_product_pk LIKE '%WNDACT%' THEN "Ready Care"
                     ELSE "KCI Unit"
                 END Unit_Type,
                 CASE
                     WHEN aimcust.customer_sva_enabled_flag='null' THEN '-'
                     WHEN aimcust.customer_sva_enabled_flag IS NULL THEN '-'
                     ELSE aimcust.customer_sva_enabled_flag
                 END customer_sva_enabled_flag,
                 CASE
                     WHEN t.geography='EAST' THEN 'Patton,Jennifer'
                     WHEN t.geography='WEST' THEN  'Moller,Kim'
                     ELSE  'Bauer,Chris'
                 END nsl_name
FROM hero.genesis_product prd
INNER JOIN hero.genesis_asset_record ar ON ar.ar_product_fk=prd.prd_product_pk
INNER JOIN hero.genesis_stock_keeping_location skl ON ar.ar_skl_fk=skl.skl_pk
INNER JOIN hero.genesis_service_center svc ON ar.ar_svc_fk=svc.svc_pk
INNER JOIN hero.genesis_customer cst ON cast(cst.cst_pk AS string)=skl.skl_short_description
INNER JOIN hero.genesis_department dept ON ar.ar_dpt_fk=dept.dpt_pk
INNER JOIN

(SELECT customer_shipto_site_use_id,
        customer_shipto_account_number,
        customer_sva_enabled_flag,
        customer_shipto_key
 FROM AIM.AIM_CUSTOMER_shipto_DIM
 WHERE customer_shipto_status NOT LIKE 'Pending'
   AND customer_shipto_indirect_flag = 'N') aimcust ON cst.ra_shipto_site_id = aimcust.customer_shipto_site_use_id

INNER JOIN 

(SELECT *
 FROM aim.aim_customer_zipterr_xref_transposed_vw t
 WHERE (t.sales_credit_type_code IN ('TDV','TMV','TSV') AND t.primary_flag='Y')
        OR (t.sales_credit_type_code ='ATR' and t.customer_shipto_market_category='ACUTE')) t ON t.customer_key =aimcust.customer_shipto_key

WHERE (t.region LIKE 'A%' AND t.region <>'A0'
     AND ar.ar_product_fk LIKE 'WND%'
     AND dept.dpt_description IN ('STORAGE'));

compute stats reporting_comops.aim_asset_initial_layer;


DROP TABLE IF EXISTS reporting_comops.aim_asset;


CREATE TABLE reporting_comops.aim_asset stored as parquet 
 location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/reporting_comops.db/aim_asset/' AS
SELECT A.product,
     A.ar_dept_fk,
     A.ar_svc_fk,
     A.skl_short_description,
     A.cst_pk,
     A.facility_number,
     A.geography,
     A.region,
     A.district,
     A.territory_code,
     A.territory_name,
     B.salesrep_name AS salesrep_name,
     B.rvp_name AS regionvicepresident_name,
     B.dm_name AS districtmanager_name,
     A.jca_role,
     A.unit_number,
     A.barcode_number,
     A.last_movement_date,
     A.dpt_description,
     A.svc_pk,
     A.svc_address_1,
     A.svc_address_2,
     A.svc_city,
     A.svc_county,
     A.svc_state,
     A.svc_zip_code,
     A.svc_region_code,
     A.facility_name,
     A.svc,
     A.days_aged,
     A.unit_type,
     A.customer_sva_enabled_flag,
     A.nsl_name
FROM reporting_comops.aim_asset_initial_layer A
INNER JOIN
(SELECT *
 FROM reporting_comops.aim_order_rvp_dm_rep_names
 WHERE territory_code IS NOT NULL) B
WHERE A.territory_code = B.territory_code
AND A.region = B.region;

compute stats reporting_comops.aim_asset;
