
DROP TABLE IF EXISTS reporting_comops.hero_round_pending;


CREATE TABLE reporting_comops.hero_round_pending stored as parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/reporting_comops.db/hero_round_pending/' AS
SELECT A.product_type,
     A.item_description,
     A.inventory_item_id,
     A.roa_pm_fk,
     A.ship_date,
     A.description,
     A.svc_pk,
     A.svc_description,
     A.geography,
     A.region,
     A.district,
     A.territory_code,
     A.territory_name,
     B.salesrep_name AS salesrep_name_align,
     B.rvp_name AS regionvicepresident_name,
     B.dm_name AS districtmanager_name,
     A.jca_role,
     A.market_category ,
     A.ra_customer_number,
     A.cst_pk,
     A.cst_name,
     A.facility_caregiver,
     A.facility_Number,
     A.facility_caregiver_address,
     A.facility_caregiver_city,
     A.facility_caregiver_zip,
     A.facility_caregiver_phone_number,
     A.name_2,
     A.caregiver_type,
     A.physician_name,
     A.ro_npi,
     A.ro_doctor_phone,
     A.patient_name,
     A.patient_adress,
     A.patient_city,
     A.patient_zip,
     A.birth,
     A.ro_patient_location,
     A.ro_patient_phone,
     A.prd_description,
     A.rop_ar_serial_number_fk,
     A.ro_number,
     A.rop_status_code,
     A.ro_purchase_order_number,
     A.rop_placed_time_stamp,
     A.rop_last_round_time_stamp,
     A.rop_dc_time_stamp,
     A.ro_customer_group,
     A.customer_number,
     A.customer_name,
     A.rop_vac_transitionable_flag,
     A.rop_pk,
     A.csr,
     A.cst_ship_address_1,
     A.cst_ship_address_2,
     A.transitioned_facility,
     A.transitioned_facility_address,
     A.transitioned_facility_city,
     A.transitioned_facility_zip,
     A.rop_stop_bill_time_stamp,
     A.rop_start_bill_time_stamp,
     A.rop_created_time_stamp,
     A.rop_void_verify_time_stamp,
     A.order_status,
     A.requestor_name,
     A.requestor_address,
     A.requestor_city,
     A.requestor_zip,
     A.requestor_phone_number,
     A.tracking_number,
     A.payer_name,
     A.ro_status_code,
     A.mmc_flag,
     A.ready_care_flag,
     A.customer_sva_enabled_flag,
     A.va_system,
     A.charity_care_status,
     A.cba,
     A.rebill,
     A.wo_cancel_code_description,
     A.rop_void_code_description,
     A.placement_type,
     A.aged,
     A.ord_cnt,
     A.pending_reasons,
     A.pending_sub_reasons,
     A.pending_or_cancel_reason,
     A.pending_or_cancel_sub_reason,
     (adddate(now(),-1)) as data_refresh_date,
     A.nsl_name
FROM reporting_comops.hero_round_final A
INNER JOIN
(SELECT *
 FROM reporting_comops.aim_order_rvp_dm_rep_names
 WHERE territory_code is not null) B
WHERE A.territory_code = B.territory_code
AND A.region = B.region
AND ((A.jca_role = 'TMV'
         AND A.market_category = 'ACUTE'
         AND A.product_type = 'VAC'
         AND A.placement_type = 'Acute Orders')
     OR (A.jca_role = 'ATR'
         AND A.market_category = 'ACUTE'
         AND A.product_type = 'VAC'
         AND A.placement_type = 'Acute Orders')
     OR (A.jca_role = 'ATR'
         AND A.market_category = 'EXTENDED'
         AND A.product_type = 'VAC'
         AND A.placement_type <> 'Acute Orders')
     OR (A.jca_role = 'ATR'
         AND A.market_category = 'HOME'
         AND A.product_type = 'VAC'
         AND A.placement_type <> 'Acute Orders'
         AND A.mmc_flag = 'N')
     OR (A.jca_role = 'TSV'
         AND A.market_category = 'ACUTE'
         AND A.product_type = 'SNAP')
     OR (A.jca_role = 'TSV'
         AND A.market_category IN ('EXTENDED',
                                   'HOME'))
          OR (A.jca_role='TDV'));

compute stats reporting_comops.hero_round_pending;