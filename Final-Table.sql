
DROP TABLE IF EXISTS reporting_comops.hero_round_base_layer;


CREATE TABLE reporting_comops.hero_round_base_layer stored AS parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/reporting_comops.db/hero_round_base_layer/' AS
SELECT CASE
         WHEN roa_pm_fk='-' THEN 'VAC'
         ELSE 'SNAP'
     END 'Product_type',
         *,
         CASE
             WHEN Market_Category='ACUTE' THEN 'Acute Orders'
             WHEN Transitioned_Facility<>'-'
                  AND Market_Category IN('HOME',
                                         'EXTENDED') THEN 'Post Acute Transitions'
             WHEN Transitioned_Facility='-'
                  AND Market_Category IN('HOME',
                                         'EXTENDED') THEN 'Organic'
         END 'Placement_Type',
             CASE
                 WHEN order_status='PENDING' THEN (round((unix_timestamp(trunc(now(), 'MI'))-unix_timestamp(rop_created_time_stamp))/3600))
                 WHEN order_status='CLOSED' THEN nvl(round(datediff(rop_stop_bill_time_stamp, rop_start_bill_time_stamp))+1, 0)
                 WHEN order_status='PLACED' THEN nvl(round(datediff(now(), rop_start_bill_time_stamp))+1, 0)
                 WHEN order_status='CANCELLED' THEN 0
             END 'aged'
FROM
(SELECT *
 FROM reporting_comops.hero_Round_initial_layer
 UNION ALL SELECT *
 FROM reporting_comops.hero_round_snap_base_layer
 UNION ALL SELECT *
 FROM reporting_comops.hero_round_ATR_TRANS_initial_layer
 WHERE market_category <> 'UNKNOWN'
   AND market_category IS NOT NULL
   AND market_category <> '-') tmp ;

compute stats reporting_comops.hero_round_base_layer;


DROP TABLE IF EXISTS reporting_comops.hero_round_rpt_layer;


CREATE TABLE reporting_comops.hero_round_rpt_layer stored AS parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/reporting_comops.db/hero_round_rpt_layer/' AS
SELECT rop_pk,
     CASE
         WHEN rdr_description='null' THEN '-'
         WHEN rdr_description IS NULL THEN '-'
         ELSE rdr_description
     END pending_reasons,
     CASE
         WHEN rdsr_description='null' THEN '-'
         WHEN rdsr_description IS NULL THEN '-'
         ELSE rdsr_description
     END pending_sub_reasons
FROM reporting_comops.hero_round_base_layer a
INNER  JOIN reporting_comops.ship_pending_reasons b ON a.rop_pk=b.rdr_rop_fk
AND order_status='PENDING';

compute stats reporting_comops.hero_round_rpt_layer;


DROP TABLE IF EXISTS reporting_comops.hero_round_final;


CREATE TABLE reporting_comops.hero_round_final stored as parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/reporting_comops.db/hero_round_final/' AS
SELECT tmp3.*,
     CASE
         WHEN order_status='PENDING' THEN pending_reasons
         WHEN order_status='CANCELLED' THEN wo_cancel_code_description
         ELSE '-'
     END pending_or_cancel_reason,
     CASE
         WHEN order_status='PENDING' THEN pending_sub_reasons
         WHEN order_status='CANCELLED' THEN rop_void_code_description
         ELSE '-'
     END pending_or_cancel_sub_reason
FROM
(SELECT tmp.*,
        CASE
            WHEN tmp2.pending_reasons='null' THEN '-'
            WHEN tmp2.pending_reasons IS NULL THEN '-'
            ELSE tmp2.pending_reasons
        END pending_reasons,
        CASE
            WHEN tmp2.pending_sub_reasons='null' THEN '-'
            WHEN tmp2.pending_sub_reasons IS NULL THEN '-'
            ELSE tmp2.pending_sub_reasons
        END pending_sub_reasons
 FROM
   (SELECT x.*,
           y.ord_cnt
    FROM
      (SELECT *
       FROM reporting_comops.hero_round_base_layer
       WHERE territory_code <> '-'
         AND rop_created_time_stamp>=to_date('2018-07-01')
         AND territory_code NOT LIKE 'DCS99%') x,

      (SELECT facility_caregiver,
              order_status,
              territory_code,
              count(DISTINCT ro_number) ord_cnt
       FROM reporting_comops.hero_round_base_layer
       WHERE territory_code <> '-'
         AND rop_created_time_stamp>=to_date('2018-07-01')
       GROUP BY 1,2,3) y
    WHERE x.facility_caregiver = y.facility_caregiver
      AND x.order_status = y.order_status
      AND x.territory_code=y.territory_code) tmp
 LEFT OUTER JOIN reporting_comops.hero_round_rpt_layer tmp2 ON tmp.rop_pk=tmp2.rop_pk) tmp3;

compute stats reporting_comops.hero_round_final;