
DROP TABLE IF EXISTS reporting_comops.hero_round_snap_initial_layer;


CREATE TABLE reporting_comops.hero_round_snap_initial_layer stored AS parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/reporting_comops.db/hero_round_snap_initial_layer/' AS
SELECT item_description,
     cast(inventory_item_id AS string) inventory_item_id,
         roa_pm_fk,
         ship_date,
         description,
         nvl(svc.svc_pk,
             '-') svc_pk,
            nvl(svc.svc_description,
                '-') svc_description,
                nvl(cst.geography,
                   '-') geography,
               nvl(t.region,
                   '-') region,
                  nvl(t.district,
                      '-') district,
                     nvl(t.TERRITORY_CODE,'-') TERRITORY_CODE,
                     CAST(NULL AS STRING) territory_name,
                        nvl(t.salesrep_name,
                               '-') salesrep_name_align,
                              nvl(t.rvp_name,
                                  '-') regionvicepresident_name,
                                 nvl(t.dm_name,
                                     '-') districtmanager_name,
                                    nvl(t.sales_credit_type_code,'-') jca_role,
                                       nvl(cst.customer_shipto_market_category,
                                           '-') Market_Category,
                                          CASE
                                              WHEN cst.ra_customer_number = 'null' THEN '-'
                                              WHEN cst.ra_customer_number IS NULL THEN '-'
                                              ELSE cst.ra_customer_number
                                          END ra_customer_number,
                                          nvl(cst.cst_pk,
                                              0) cst_pk,
                                             nvl(cst.cst_name,
                                                 '-') cst_name,
                                                CASE
                                                    WHEN cst.customer_shipto_market_category IN('HOME') THEN CASE
                                                                                                                 WHEN concat(cgv.caregiver_account_number, ' ', '-', ' ', cgv.caregiver_name_1)='null' THEN 'NO CAREGIVER AVAILABLE'
                                                                                                                 WHEN concat(cgv.caregiver_account_number, ' ', '-', ' ', cgv.caregiver_name_1) IS NULL THEN 'NO CAREGIVER AVAILABLE'
                                                                                                                 ELSE concat(cgv.caregiver_account_number, ' ', '-', ' ', cgv.caregiver_name_1)
                                                                                                             END
                                                    ELSE CASE
                                                             WHEN concat(ra_customer_number, ' ', '-', ' ', cst_name)='null' THEN 'NO FACILITY AVAILABLE'
                                                             WHEN concat(ra_customer_number, ' ', '-', ' ', cst_name) IS NULL THEN 'NO FACILITY AVAILABLE'
                                                             ELSE concat(ra_customer_number, ' ', '-', ' ', cst_name)
                                                         END
                                                END Facility_Caregiver,
                                                 CASE
                                                WHEN cst.customer_shipto_market_category IN('HOME') THEN CASE
                                                                                                                 WHEN cgv.caregiver_account_number ='null' THEN 'NO CAREGIVER AVAILABLE'
                                                                                                                 WHEN cgv.caregiver_account_number IS NULL THEN 'NO CAREGIVER AVAILABLE'
                                                                                                                 ELSE cgv.caregiver_account_number 
                                                                                                             END
                                                    ELSE CASE                                                         
                                                         WHEN  ra_customer_number ='null' THEN 'NO FACILITY AVAILABLE'
                                                         WHEN  ra_customer_number IS NULL THEN 'NO FACILITY AVAILABLE'
                                                         ELSE  ra_customer_number
                                                     END
                                            END Facility_Number,
                                                CASE
                                                    WHEN cst.customer_shipto_market_category IN('HOME') THEN CASE
                                                                                                                 WHEN cgv.caregiver_physical_address_1 ='null' THEN '-'
                                                                                                                 WHEN cgv.caregiver_physical_address_1 IS NULL THEN '-'
                                                                                                                 ELSE cgv.caregiver_physical_address_1
                                                                                                             END
                                                    ELSE CASE
                                                             WHEN cst.cst_ship_address_1='null' THEN '-'
                                                             WHEN cst.cst_ship_address_1 IS NULL THEN '-'
                                                             ELSE cst.cst_ship_address_1
                                                         END
                                                END Facility_Caregiver_Address,
                                                CASE
                                                    WHEN cst.customer_shipto_market_category IN('HOME') THEN CASE
                                                                                                                 WHEN cgv.caregiver_physical_city ='null' THEN '-'
                                                                                                                 WHEN cgv.caregiver_physical_city IS NULL THEN '-'
                                                                                                                 ELSE cgv.caregiver_physical_city
                                                                                                             END
                                                    ELSE CASE
                                                             WHEN cst.cst_ship_city ='null' THEN '-'
                                                             WHEN cst.cst_ship_city IS NULL THEN '-'
                                                             ELSE cst.cst_ship_city
                                                         END
                                                END Facility_Caregiver_City,
                                                CASE
                                                    WHEN cst.customer_shipto_market_category IN('HOME') THEN CASE
                                                                                                                 WHEN cgv.caregiver_physical_zip ='null' THEN '-'
                                                                                                                 WHEN cgv.caregiver_physical_zip IS NULL THEN '-'
                                                                                                                 ELSE cgv.caregiver_physical_zip
                                                                                                             END
                                                    ELSE CASE
                                                             WHEN cst.cst_ship_zip_code ='null' THEN '-'
                                                             WHEN cst.cst_ship_zip_code IS NULL THEN '-'
                                                             ELSE cst.cst_ship_zip_code
                                                         END
                                                END Facility_Caregiver_Zip,
                                                CASE
                                                    WHEN cst.customer_shipto_market_category IN('HOME') THEN CASE
                                                                                                                 WHEN cgv.caregiver_physical_phone='null' THEN '-'
                                                                                                                 WHEN cgv.caregiver_physical_phone IS NULL THEN '-'
                                                                                                                 ELSE cgv.caregiver_physical_phone
                                                                                                             END
                                                    ELSE CASE
                                                             WHEN cst.cst_contact_phone_number ='null' THEN '-'
                                                             WHEN cst.cst_contact_phone_number IS NULL THEN '-'
                                                             ELSE cst.cst_contact_phone_number
                                                         END
                                                END Facility_Caregiver_Phone_Number,
                                                CASE
                                                    WHEN cgv.caregiver_physical_address_2 = 'null' THEN '-'
                                                    WHEN cgv.caregiver_physical_address_2 IS NULL THEN '-'
                                                    ELSE cgv.caregiver_physical_address_2
                                                END name_2,
                                                nvl(cgv.caregiver_bus_type,
                                                    '-') caregiver_type,
                                                   CASE
                                                       WHEN concat(phy.physician_last_name, ', ', phy.physician_first_name) IS NULL THEN '-'
                                                       WHEN concat(phy.physician_last_name, ', ', phy.physician_first_name) ='null, null' THEN '-'
                                                       WHEN concat(phy.physician_last_name, ', ', phy.physician_first_name)=', ' THEN '-'
                                                       ELSE concat(phy.physician_last_name, ', ', phy.physician_first_name)
                                                   END physician_name,
                                                   nvl(cast(ro.ro_npi AS string),
                                                       '-') ro_npi,
                                                      CASE
                                                          WHEN phy.physician_bus_mail_phone='null' THEN '-'
                                                          WHEN phy.physician_bus_mail_phone='nul-l-' THEN '-'
                                                          WHEN phy.physician_bus_mail_phone IS NULL THEN '-'
                                                          ELSE phy.physician_bus_mail_phone
                                                      END ro_doctor_phone,
                                                      CASE
                                                          WHEN cst.customer_shipto_market_category IN('ACUTE',
                                                                                                      'EXTENDED') THEN nvl(concat(ro.ro_patient_last_name, ", ", ro.ro_patient_first_name), '-')
                                                          WHEN cst.customer_shipto_market_category IN('HOME') THEN nvl(cst.cst_name, '-')
                                                          ELSE '-'
                                                      END patient_name,
                                                      CASE
                                                          WHEN cst.customer_shipto_market_category IN('ACUTE',
                                                                                                      'EXTENDED') THEN '-'
                                                          WHEN cst.customer_shipto_market_category IN('HOME') THEN nvl(cst.cst_ship_address_1, '-')
                                                          ELSE '-'
                                                      END patient_adress,
                                                      CASE
                                                          WHEN cst.customer_shipto_market_category IN('ACUTE',
                                                                                                      'EXTENDED') THEN '-'
                                                          WHEN cst.customer_shipto_market_category IN('HOME') THEN nvl(cst.cst_ship_city, '-')
                                                          ELSE '-'
                                                      END patient_city,
                                                      CASE
                                                          WHEN cst.customer_shipto_market_category IN('ACUTE',
                                                                                                      'EXTENDED') THEN '-'
                                                          WHEN cst.customer_shipto_market_category IN('HOME') THEN nvl(cst.cst_ship_zip_code, '-')
                                                          ELSE '-'
                                                      END patient_zip,
                                                      CASE
                                                          WHEN cst.customer_shipto_market_category IN('ACUTE',
                                                                                                      'EXTENDED') THEN CASE
                                                                                                                           WHEN (from_unixtime(unix_timestamp(TO_DATE(ro.ro_patient_dob), 'yyyy-MM-dd'), 'MM-dd-yyyy'))='null' THEN '-'
                                                                                                                           WHEN (from_unixtime(unix_timestamp(TO_DATE(ro.ro_patient_dob), 'yyyy-MM-dd'), 'MM-dd-yyyy')) IS NULL THEN '-'
                                                                                                                           ELSE from_unixtime(unix_timestamp(TO_DATE(ro.ro_patient_dob), 'yyyy-MM-dd'), 'MM-dd-yyyy')
                                                                                                                       END
                                                          ELSE '-'
                                                      END birth,
                                                      CASE
                                                          WHEN cst.customer_shipto_market_category IN('ACUTE',
                                                                                                      'EXTENDED') THEN CASE
                                                                                                                           WHEN ro_patient_location='null' THEN '-'
                                                                                                                           WHEN ro_patient_location IS NULL THEN '-'
                                                                                                                           ELSE ro_patient_location
                                                                                                                       END
                                                          ELSE '-'
                                                      END ro_patient_location,
                                                      CASE
                                                          WHEN cst.customer_shipto_market_category IN('ACUTE',
                                                                                                      'EXTENDED') THEN CASE
                                                                                                                           WHEN ro_patient_phone='null' THEN '-'
                                                                                                                           WHEN ro_patient_phone IS NULL THEN '-'
                                                                                                                           ELSE ro_patient_phone
                                                                                                                       END
                                                          ELSE '-'
                                                      END ro_patient_phone,
                                                      nvl(prd.prd_description,
                                                          '-') prd_description,
                                                         rop_ar_serial_number_fk,
                                                         nvl(ro.ro_pk,
                                                             0) ro_number,
                                                            rop_status_code,
                                                            CASE
                                                                WHEN ro_purchase_order_number='null' THEN '-'
                                                                WHEN ro_purchase_order_number IS NULL THEN '-'
                                                                ELSE ro_purchase_order_number
                                                            END ro_purchase_order_number,
                                                            rop_placed_time_stamp,
                                                            rop_last_round_time_stamp,
                                                            rop_dc_time_stamp,
                                                            nvl(ro.ro_customer_group,
                                                                '-') ro_customer_group,
                                                               nvl(rac.customer_number,
                                                                   '-') customer_number,
                                                                  nvl(rac.customer_name,
                                                                      '-') customer_name,
                                                                     rop_vac_transitionable_flag,
                                                                     rop_pk,
                                                                     CSR,
                                                                     CASE
                                                                         WHEN cst.cst_ship_address_1='null' THEN '-'
                                                                         WHEN cst.cst_ship_address_1 IS NULL THEN '-'
                                                                         ELSE cst.cst_ship_address_1
                                                                     END cst_ship_address_1,
                                                                     CASE
                                                                         WHEN cst.cst_ship_address_2='null' THEN '-'
                                                                         WHEN cst.cst_ship_address_2 IS NULL THEN '-'
                                                                         ELSE cst.cst_ship_address_2
                                                                     END cst_ship_address_2,
                                                                     nvl(concat(cast(ro.ro_linked_ship_to_cst_fk AS string), ' ', '-', ' ', ro.ro_linked_facility_name),
                                                                         '-') Transitioned_Facility,
                                                                        CASE
                                                                            WHEN ro.ro_linked_facility_address1= 'null' THEN '-'
                                                                            WHEN ro.ro_linked_facility_address1 IS NULL THEN '-'
                                                                            ELSE ro.ro_linked_facility_address1
                                                                        END Transitioned_Facility_address,
                                                                        CASE
                                                                            WHEN ro.ro_linked_facility_city= 'null' THEN '-'
                                                                            WHEN ro.ro_linked_facility_city IS NULL THEN '-'
                                                                            ELSE ro.ro_linked_facility_city
                                                                        END Transitioned_Facility_city,
                                                                        CASE
                                                                            WHEN ro.ro_linked_facility_zip_code= 'null' THEN '-'
                                                                            WHEN ro.ro_linked_facility_zip_code IS NULL THEN '-'
                                                                            ELSE ro.ro_linked_facility_zip_code
                                                                        END Transitioned_Facility_zip,
                                                                        rop_stop_bill_time_stamp,
                                                                        rop_start_bill_time_stamp,
                                                                        rop_created_time_stamp,
                                                                        rop_void_verify_time_stamp,
                                                                        order_status,
                                                                        CASE
                                                                            WHEN concat(wo_requestor_account_number, ' ', '-', ' ', wo_caller_company)='null,null' THEN '-'
                                                                            WHEN concat(wo_requestor_account_number, ' ', '-', ' ', wo_caller_company) IS NULL THEN '-'
                                                                            WHEN (wo_requestor_account_number='null'
                                                                                  OR wo_requestor_account_number IS NULL)
                                                                                 AND (wo_caller_company<>'null'
                                                                                      OR wo_caller_company IS NOT NULL) THEN wo_caller_company
                                                                            WHEN (wo_requestor_account_number<>'null'
                                                                                  OR wo_requestor_account_number IS NOT  NULL)
                                                                                 AND (wo_caller_company='null'
                                                                                      OR wo_caller_company IS NULL) THEN wo_requestor_account_number
                                                                            ELSE concat(wo_requestor_account_number, ' ', '-', ' ', wo_caller_company)
                                                                        END requestor_name,
                                                                        CASE
                                                                            WHEN wo.wo_requestor_address_1= 'null' THEN '-'
                                                                            WHEN wo.wo_requestor_address_1 IS NULL THEN '-'
                                                                            ELSE wo.wo_requestor_address_1
                                                                        END requestor_address,
                                                                        CASE
                                                                            WHEN wo.wo_requestor_city= 'null' THEN '-'
                                                                            WHEN wo.wo_requestor_city IS NULL THEN '-'
                                                                            ELSE wo.wo_requestor_city
                                                                        END requestor_city,
                                                                        CASE
                                                                            WHEN wo.wo_requestor_zip_code= 'null'THEN '-'
                                                                            WHEN wo.wo_requestor_zip_code IS NULL THEN '-'
                                                                            ELSE wo.wo_requestor_zip_code
                                                                        END requestor_zip,
                                                                        CASE
                                                                            WHEN wo.wo_caller_phone= 'null' THEN '-'
                                                                            WHEN wo.wo_caller_phone IS NULL THEN '-'
                                                                            ELSE wo.wo_caller_phone
                                                                        END requestor_phone_number,
                                                                        CASE
                                                                            WHEN wo_rs_tracking_number='null' THEN '-'
                                                                            WHEN wo_rs_tracking_number IS NULL THEN '-'
                                                                            ELSE wo_rs_tracking_number
                                                                        END tracking_number,
                                                                        CASE
                                                                            WHEN payor.customer_billto_name='null' THEN '-'
                                                                            WHEN payor.customer_billto_name IS NULL THEN '-'
                                                                            ELSE payor.customer_billto_name
                                                                        END payer_name,
                                                                        nvl(ro.ro_status_code,
                                                                            '-') ro_status_code,
                                                                            CASE
                                                                                                      WHEN cst.mmc_flag='null' THEN '-'
                                                                                                      WHEN cst.mmc_flag IS NULL THEN '-'
                                                                                                      ELSE cst.mmc_flag
                                                                                                  END mmc_flag,
                                                                           
                                                                           CASE
                                                                               WHEN rop_consign = 'HOM' THEN 'Y'
                                                                               ELSE 'N'
                                                                           END Ready_Care_Flag,
                                                                           CASE
                                                                               WHEN cst.customer_sva_enabled_flag='null' THEN '-'
                                                                               WHEN cst.customer_sva_enabled_flag IS NULL THEN '-'
                                                                               ELSE cst.customer_sva_enabled_flag
                                                                           END customer_sva_enabled_flag,
                                                                           CASE
                                                                               WHEN ((cst.customer_sva_enabled_flag = 'Y')
                                                                                     OR (payor.customer_billto_name LIKE '%MARATHON%'
                                                                                         OR payor.customer_billto_name LIKE '%VA MEDICAL%'
                                                                                         OR payor.customer_billto_name LIKE '%V A MEDICAL%'
                                                                                         OR payor.customer_billto_name LIKE '%TRICAR%')) THEN 'Y'
                                                                               ELSE 'N'
                                                                           END VA_System,
                                                                           charity_care_status,
                                                                           CASE
                                                                               WHEN ro.ro_status_code IN ('SNC',
                                                                                                          'VNC',
                                                                                                          'MNC',
                                                                                                          'HNC',
                                                                                                          'ANC') THEN 'Y'
                                                                               ELSE 'N'
                                                                           END cba,
                                                                           rebill,
                                                                           CASE
                                                                               WHEN wo_code.ct_description ='null' THEN '-'
                                                                               WHEN wo_code.ct_description IS NULL THEN '-'
                                                                               ELSE wo_code.ct_description
                                                                           END wo_cancel_code_description,
                                                                           CASE
                                                                               WHEN void_code.ct_description ='null' THEN '-'
                                                                               WHEN void_code.ct_description IS NULL THEN '-'
                                                                               ELSE void_code.ct_description
                                                                           END ROP_void_code_description,
                                                                           CASE
                                                                               WHEN cst.geography='EAST' THEN 'Patton,Jennifer' 
                                                                               WHEN cst.geography='WEST' THEN 'Moller,Kim'
                                                                               ELSE  'Bauer,Chris'
                                                                           END nsl_name
FROM reporting_comops.hero_snap a
LEFT OUTER JOIN hero.genesis_rental_order ro ON a.rop_ro_fk=ro.ro_pk
LEFT OUTER JOIN
(SELECT cst.*,t.geography,t.region,t.district,t.territory_code,t.salesrep_name,t.dm_name,t.rvp_name,t.sales_credit_type_code
        
 FROM
   (SELECT x.*,
           aimcust.customer_shipto_market_category,
           aimcust.customer_shipto_market_segment,
           aimcust.customer_sva_enabled_flag,
           aimcust.customer_shipto_key,
           aimcust.mmc_flag
    FROM
      (SELECT *
       FROM hero.genesis_customer
       WHERE cst_pk NOT IN (9999999999,
                            0,
                            5555555555,
                            8888888888,
                            216200) ) x
    LEFT OUTER JOIN
      (SELECT customer_shipto_site_use_id,
              customer_shipto_market_category,
              customer_sva_enabled_flag,
              customer_shipto_account_number,
              customer_shipto_market_segment,
              customer_shipto_key,
              mmc_flag
       FROM aim.aim_customer_shipto_dim
       WHERE customer_shipto_status NOT LIKE 'Pending'
         AND customer_shipto_indirect_flag = 'N' ) aimcust ON x.ra_shipto_site_id = aimcust.customer_shipto_site_use_id) cst
 LEFT OUTER JOIN
   (SELECT *
 FROM aim.aim_customer_zipterr_xref_transposed_vw t
 WHERE ((t.sales_credit_type_code IN ('TDV','TMV','TSV') AND t.primary_flag='Y')
        OR (t.sales_credit_type_code ='ATR' and t.customer_shipto_market_category='ACUTE')) AND t.region LIKE 'A%' AND t.region <>'A0') t ON t.customer_key =cst.customer_shipto_key) cst ON ro.RO_CST_FK=cst.CST_PK
LEFT OUTER JOIN
(SELECT *
 FROM aim.aim_zip_terr_rep_dim_vw t
 WHERE current_align_flag='Y') t ON t.zip_code=substr(cst.cst_ship_zip_code,
                                                      1,
                                                      5)
INNER JOIN
(SELECT *
 FROM hero.genesis_service_center
 WHERE svc_pk NOT IN ('341',
                      '405',
                      '0') )svc ON a.ROP_SVC_FK = svc.SVC_PK
LEFT OUTER JOIN hero.genesis_ro_claim_data cd ON ro.RO_PK=cd.ROC_RO_FK
LEFT JOIN aim.aim_caregiver_dim cgv ON cgv.caregiver_mstrid = cd.roc_hha_smg_mstr_id_fk
LEFT OUTER JOIN hero.genesis_product prd ON a.ROP_PRODUCT_USED_FK = prd.PRD_PRODUCT_PK
LEFT OUTER JOIN hero.genesis_employee e ON e.EMP_PK = cst_last_order_emp_fk
LEFT OUTER JOIN hero.genesis_ra_site_uses_all_snap rss ON ro.RO_BILL_TO_SITE_USE_ID = rss.SITE_USE_ID
LEFT OUTER JOIN hero.genesis_ra_addresses_all_snap ras ON rss.ADDRESS_ID = ras.ADDRESS_ID
LEFT OUTER JOIN hero.genesis_ra_customers_snap rac ON ras.CUSTOMER_ID = rac.CUSTOMER_ID
LEFT OUTER JOIN
(SELECT wo_rop_fk,
        wo_requestor_account_number,
        wo_caller_company,
        wo_rs_tracking_number,
        wo_requestor_address_1,
        wo_requestor_city,
        wo_requestor_zip_code,
        wo_caller_phone,
        wo_cancel_reason,
        wo_type_code
 FROM hero.genesis_work_order
 WHERE (wo_requestor_account_number<>'null'
        OR wo_requestor_account_number IS NULL)
   AND wo_type_code='DEL'
 GROUP BY 1,	2,	3,	4,	5,	6,	7,	8,	9,	10) wo
  ON a.ROP_PK = wo.WO_ROP_FK
LEFT OUTER JOIN aim.aim_customer_billto_dim payor ON ro.ro_bill_to_site_use_id=payor.customer_billto_site_use_id
LEFT OUTER JOIN hero.genesis_code wo_code ON ((upper(wo_code.ct_column) LIKE '%WO_CANCEL_REASON_ALL%')
                                            AND wo_code.ct_code = wo_cancel_reason
                                            AND wo.wo_rop_fk=a.rop_pk)
LEFT OUTER JOIN hero.genesis_code void_code on((upper(void_code.ct_column) IN ('ROP_VOID_TYPE',
                                                                             'ROP_VOID_TYPE_SUB_REQ'))
                                             AND void_code.ct_code = rop_void_type_code
                                             AND wo.wo_rop_fk=a.rop_pk)
LEFT OUTER JOIN aim.aim_physician_dim phy ON phy.physician_npi = ro.ro_npi
WHERE (cst.customer_shipto_market_segment NOT IN (51,
                                                52,
                                                53,
                                                99)
     OR cst.customer_shipto_market_segment IS NULL)
AND (svc.svc_pk IS NOT NULL
     OR svc_pk<>'000')
AND ((concat(ro.ro_patient_last_name, ", ", ro.ro_patient_first_name)) NOT LIKE '%UNIT %'
     AND (concat(ro.ro_patient_last_name, ", ", ro.ro_patient_first_name)) NOT LIKE '%PLUS%'
     AND (concat(ro.ro_patient_last_name, ", ", ro.ro_patient_first_name)) NOT LIKE '%CUST%')
GROUP BY 1,	2,	3,	4,	5,	6,	7,	8,	9,	10,	11,	12,	13,	14,	15,	16,	17,	18,	19,	20,	21,	22,	23,	24,	25,	26,	27,	28,	29,	30,	31,	32,	33,	34,	35,	36,	37,	38,	39,	40,
	41,	42,	43,	44,	45,	46,	47,	48,	49,	50,	51,	52,	53,	54,	55,	56,	57,	58,	59,	60,	61,	62,	63,	64,	65,	66,	67,	68,	69,	70,	71,	72,	73,	74,	75,	76,77,78,79,80,81;

compute stats reporting_comops.hero_round_snap_initial_layer;