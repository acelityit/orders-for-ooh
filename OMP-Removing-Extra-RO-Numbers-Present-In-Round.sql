
DROP TABLE IF EXISTS reporting_comops.omp_data_base_layer;


CREATE TABLE reporting_comops.omp_data_base_layer stored as parquet 
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/reporting_comops.db/omp_data_base_layer/' AS
SELECT A.*
FROM reporting_comops.omp_data A
INNER JOIN
(SELECT DISTINCT ro_number
 FROM reporting_comops.hero_round_pending) B ON A.ro_number = B.ro_number;

compute stats reporting_comops.omp_data_base_layer;


DROP TABLE IF EXISTS reporting_comops.omp_data;


CREATE TABLE reporting_comops.omp_data stored as parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/reporting_comops.db/omp_data/' AS
SELECT *
FROM reporting_comops.omp_data_base_layer;

compute stats reporting_comops.omp_data;

drop table IF EXISTS reporting_comops.zip_terr_1008_v2 ;
create table reporting_comops.zip_terr_1008_v2 stored as parquet 
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/reporting_comops.db/zip_terr_1008_v2/' AS

SELECT (CASE WHEN sales_credit_type_code = 'AE' THEN regexp_replace (territory_code,'WCR','AE') 
             WHEN sales_credit_type_code IN ('SAE','SAE9') THEN regexp_replace (territory_code,'AES','SAE') 
        ELSE territory_code END) region,
       concat('K',salesrep_number) userid,
       salesrep_name rvp_name,
       (CASE WHEN name LIKE 'DUAL%' AND sales_credit_type_code IN ('TMV','TSV') THEN 'TDV' ELSE sales_credit_type_code END) jca_role
FROM aim.aim_territory_salesrep_xref_vw
WHERE territory_code IS NOT NULL AND sales_credit_type_code NOT IN ('BDM','BDM9','ISR','ISR9','VCS','TSA')
GROUP BY 1,2,3,4
union all
select district region,concat('K',employee_number) userid,
dm_name rvp_name,'DMS' jca_role
from aim.aim_district_dm_xref_vw where District like ('R%')
group by 1,2,3,4
union all
select translate(region,'R','A') region,concat('K',employee_number) userid,
rvp_name rvp_name,'RVP' jca_role
from aim.aim_region_rvp_xref_vw  where translate(region,'R','A') like ('A%') and translate(region,'R','A')<>'A0' and start_date>='2020-01-01'
group by 1,2,3,4;

compute stats reporting_comops.zip_terr_1008_v2;

drop table IF EXISTS reporting_comops.zip_terr_1008_v3 ;
create table reporting_comops.zip_terr_1008_v3 stored as parquet 
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/reporting_comops.db/zip_terr_1008_v3/' AS

select a.*,p.email_address from 
reporting_comops.zip_terr_1008_v2 a
Left join
(select employee_number,email_address, max(effective_end_date) from ebs_usawt.apps_per_all_people_f
where trunc(now(),'dd') between trunc(effective_start_date,'dd') and trunc(effective_end_date,'dd') 
and employee_number <> 'null'
group by 1,2
) p
on a.userid = concat('K',p.employee_number);

compute stats reporting_comops.zip_terr_1008_v3;

drop table IF EXISTS reporting_comops.orders_userprofile_table_2019 ;
create table reporting_comops.orders_userprofile_table_2019 stored as parquet 
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/reporting_comops.db/orders_userprofile_table_2019/' AS
Select 
  jca_role,
  region RDT,
  rvp_name name,
  case  when userid = 'K19068' then 'beesond'
        when userid = 'K20174' then 'dragoa'
        when userid = 'K20054' then 'johnsore'
        when userid = 'K20016' then 'dalyk'
        when userid = 'K19363' then 'deschenp'
        when userid = 'K13586' then 'dillonc'
        when userid = 'K17267' then 'fruges'
        when userid = 'K20055' then 'Jacobsod'
        when userid = 'K13723' then 'lallyr'
        when userid = 'K16740' then 'lemairet'
        when userid = 'K20042' then 'Oconnorm'
        when userid = 'K14726' then 'parhams'
        when userid = 'K14680' then 'sandersr'
        when userid = 'K10513' then 'schutzep'
       when userid = 'K20035' then 'Sholb'
        when userid = 'K14785' then 'Sommerfd'
        when userid = 'K20196' then 'KS458'
        when userid = 'K17806' then 'stollm'
        when userid = 'K16699' then 'brownk'
        when userid = 'K14557' then 'budnikb'
        when userid = 'KD305' then 'donohoem'
        when userid = 'K13992' then 'Dunawayd'
        when userid = 'KJ100' then 'Jacksonr'
        when userid = 'KK098' then 'kilbanep'
        when userid = 'KM171' then 'myersr'
        when userid = 'K15394' then 'greved'
when userid = 'K18797' then 'adamsg'
when userid = 'K15715' then 'arvinp'
when userid = 'K18410' then 'boudreap'
when userid = 'K14669' then 'boykinj'
when userid = 'K20058' then 'KC004'
when userid = 'K17541' then 'weberb'
when userid = 'K19371' then 'dominguc'
when userid = 'K20046' then 'KW501'
when userid = 'K17138' then 'falconev'
when userid = 'K10335' then 'KimmelG'
when userid = 'K18644' then 'ladnerp'
when userid = 'K18000' then 'lightr'
when userid = 'K18491' then 'loffmanj'
when userid = 'K16515' then 'manningk'
when userid = 'K16964' then 'maragose'
when userid = 'K12833' then 'morrissc'
when userid = 'K18084' then 'overstrm'
when userid = 'K18384' then 'rodmaris'
when userid = 'K15584' then 'steubem'
when userid = 'K20050' then 'Wadesonj'
when userid = 'K15167' then 'wilsonc'
when userid = 'K72446' then 'hugerm'
        Else userid
  end userid,  
email_address
from 
reporting_comops.zip_terr_1008_v3 ;

compute stats reporting_comops.orders_userprofile_table_2019;

drop table if exists reporting_comops.userprofile_round_table_2019;
create table reporting_comops.userprofile_round_table_2019 stored as parquet 
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/reporting_comops.db/userprofile_round_table_2019/' AS

select y.jca_role,y.rdt as territory_code,y.name as salesrep_name, y.userid as t_userid,y.email_address as t_email_address,
x.rdt as district,x.name as dm_name, x.userid as d_userid,x.email_address as d_email_address,
z.rdt as region,z.name as rvp_name, z.userid as r_userid,z.email_address as r_email_address,
concat(x.userid,y.userid) dt_userid
from 
(select a.*,b.territory_code, 
b.region, 
b.district
from reporting_comops.orders_userprofile_table_2019 a
left join 
(select sales_credit_type_code,territory_code,region,district 
from aim.aim_customer_zipterr_xref_transposed_vw where region like 'A%' and region <>'A0'
group by sales_credit_type_code,territory_code,region,district) b on
b.territory_code = a.rdt) y
left join
reporting_comops.orders_userprofile_table_2019 x
on x.rdt=y.district
left join 
reporting_comops.orders_userprofile_table_2019 z
on z.rdt=y.region
where district is not null;
compute stats reporting_comops.userprofile_round_table_2019;
