/*refresh hero.genesis_product;     
refresh hero.genesis_asset_record;
refresh hero.genesis_stock_keeping_location;
refresh hero.genesis_service_center;
refresh hero.genesis_customer;
refresh hero.genesis_department;
refresh hero.genesis_rental_order;
refresh hero.genesis_rental_order_product;
refresh hero.genesis_ro_claim_data;
refresh hero.genesis_work_order;
refresh hero.genesis_employee;
refresh hero.genesis_ra_site_uses_all_snap;
refresh hero.genesis_ra_addresses_all_snap;
refresh hero.genesis_ra_customers_snap;
refresh hero.genesis_code;
refresh hero.genesis_mtl_system_items_snap;
refresh hero.genesis_rental_order_prod_accessory;
refresh hero.genesis_mtl_oracle_categories_mv;
refresh hero.genesis_rop_delay_reason;
refresh hero.genesis_rop_delay_sub_reason;
refresh hero.genesis_rop_delay_reason_option;
refresh delair.patient_ptmaster;
refresh delair.patient_ptorder;
refresh delair.patient_ptaddr;
refresh hero.genesis_vac_wound_information;
refresh hero.genesis_vac_wound_status_tracking;
refresh hero.genesis_vac_wound_types;
refresh hero.genesis_vac_wound_codes;*/
--------------------------------------------------------- Alignment table code start ------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS sand_cast.aim_order_Non_Home;
CREATE TABLE sand_cast.aim_order_Non_Home stored as parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/aim_order_Non_Home/' AS
select customer_key from aim.aim_customer_zipterr_xref_transposed_vw where territory_code like '%999%'
group by 1;
DROP TABLE IF EXISTS sand_cast.aim_order_rvp_dm_rep_names;
CREATE TABLE sand_cast.aim_order_rvp_dm_rep_names stored as parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/aim_order_rvp_dm_rep_names/' AS
SELECT sales_credit_type_code,region,district,territory_code,rvp_name,dm_name,salesrep_name,'TERRITORY' rollup
FROM aim.aim_customer_zipterr_xref_transposed_vw
--#MP Mario Change here
--# CTS update : Alignment
WHERE sales_credit_type_code IN ('SSR','WHS','ATR','TSV','TMV','AWT','ISR','TSR')
AND territory_code NOT LIKE '%999%' AND region LIKE 'A%' AND region <>'A0'
GROUP BY 1,2,3,4,5,6,7,8
----DM names----
UNION ALL
SELECT 'DM' sales_credit_type_code,region,district,CAST(NULL AS STRING) territory_code,rvp_name,
dm_name,CAST(NULL AS STRING) salesrep_name,'DISTRICT' rollup
FROM aim.aim_customer_zipterr_xref_transposed_vw
--#MP Mario Change here
--# CTS update : Alignment
WHERE sales_credit_type_code IN ('SSR','WHS','ATR','TSV','TMV','AWT','ISR','TSR')
AND territory_code NOT LIKE '%999%' AND region LIKE 'A%' AND region <>'A0'
GROUP BY 1,2,3,4,5,6,7,8
----RVP names----
UNION ALL
SELECT 'RVP' sales_credit_type_code,region,CAST(NULL AS STRING) district,CAST(NULL AS STRING) territory_code,
rvp_name,CAST(NULL AS STRING) dm_name,CAST(NULL AS STRING) salesrep_name,'REGION' rollup
FROM aim.aim_customer_zipterr_xref_transposed_vw
--#MP Mario Change here
--# CTS update : Alignment
WHERE sales_credit_type_code IN ('SSR','WHS','ATR','TSV','TMV','AWT','ISR','TSR')
AND territory_code NOT LIKE '%999%' AND region LIKE 'A%' AND region <>'A0'
GROUP BY 1,2,3,4,5,6,7,8;
compute stats sand_cast.aim_order_rvp_dm_rep_names;

----------------------------------------------------- Alignment table code end ------------------------------------------------------------------------------------------------

------------------------------------------------------ ASSET QUERY start -------------------------------------------------------------------------------------------------------

--#MP 1/15/2021 [CASTP-165] #3.1-Add fields from Gold Customer Assets table to aim_asset reporting table.Add gold_asset_barcode as a new column in addition to BARCODE_NUMBER in order to verify if the gold customers asset barcode matches the asset barcode in aim_asset
--#MP 1/15/2021 [CASTP-156] #5-Update table filters in 'aim_asset' table to update Assets by Location.Additional dept_descriptions should flow through
--#MP 1/15/2021 [CASTP-164] #1.1-Add Gold Program (gold_cust_flag) Flag to aim_asset reporting table.
--#MP 1/15/2021 [CASTP-160] #2.1-Add fields from Gold Customer Roster table to aim_asset reporting table. Add FACILITY ID, CHAIN NAME
--#MP 1/15/2021 [CASTP-166] Add column ar_row_bin
--#MP 1/15/2021 [CASTP-181] Add column customer_class to pass to final table
--#MP 1/15/2021 [CASTP-172] Add column ,mat_mia_tracking_status,mat_contact_notes_1,mat_asset_retrieval_date,ct_description to pass to final table
DROP TABLE IF EXISTS sand_cast.aim_asset_initial_layer;
CREATE TABLE sand_cast.aim_asset_initial_layer stored as parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/aim_asset_initial_layer/' AS
select a.* from (SELECT CASE
WHEN prd.prd_description='null' THEN '-'WHEN prd.prd_description IS NULL THEN '-' ELSE prd.prd_description END product,
nvl(ar.ar_dpt_fk,0) ar_dept_fk,
CASE WHEN ar.ar_svc_fk='null' THEN '-' WHEN ar.ar_svc_fk IS NULL THEN '-' ELSE ar.ar_svc_fk END ar_svc_fk,
CASE WHEN skl.skl_short_description='null' THEN '-'WHEN skl.skl_short_description IS NULL THEN '-'ELSE skl.skl_short_description END skl_short_description,
nvl(cst.cst_pk,0) cst_pk,
CASE WHEN cst.ra_customer_number='null' THEN '-'WHEN cst.ra_customer_number IS NULL THEN '-'ELSE cst.ra_customer_number END facility_number,
--# CTS update : removed geography
--t.geography geography,
t.region region,t.district,nvl(t.territory_code,'-') territory_code,
cast(null as string) territory_name,
cast(null as string) salesrep_id,
CASE WHEN t.salesrep_name='null' THEN '-'WHEN t.salesrep_name IS NULL THEN '-'ELSE t.salesrep_name END salesrep_name,
CASE WHEN t.rvp_name='null' THEN '-'WHEN t.rvp_name IS NULL THEN '-'ELSE t.rvp_name END regionvicepresident_name,
CASE WHEN t.dm_name='null' THEN '-'WHEN t.dm_name IS NULL THEN '-'ELSE t.dm_name END districtmanager_name,
nvl(t.sales_credit_type_code,'-') jca_role ,
--#MP 11/21/2020 included fileds as part of Inside sales initiative 
nvl(t.inside_sales_flag,'-') inside_sales_flag,nvl(t.ins_supervisor,'-') ins_supervisor,nvl(t.supervisor_name,'-') supervisor_name,
CASE WHEN ucase(ar.ar_serial_number_pk) ='null' THEN '-'WHEN ucase(ar.ar_serial_number_pk) IS NULL THEN '-'ELSE ucase(ar.ar_serial_number_pk) END unit_number,
CASE WHEN ar.ar_barcode ='null' THEN '-'WHEN ar.ar_barcode IS NULL THEN '-'ELSE ar.ar_barcode END barcode_number,
CASE WHEN cast(ar.ar_movement_time_stamp AS string) ='null' THEN '-' WHEN cast(ar.ar_movement_time_stamp AS string) IS NULL THEN '-'ELSE cast(ar.ar_movement_time_stamp AS string) END last_movement_date,
CASE WHEN dept.dpt_description='null' THEN '-'WHEN dept.dpt_description IS NULL THEN '-'ELSE dpt_description END dpt_description,
CASE WHEN svc.svc_pk='null' THEN '-' WHEN svc.svc_pk IS NULL THEN '-'ELSE svc.svc_pk END svc_pk,
CASE WHEN svc.svc_address_1='null' THEN '-'WHEN svc.svc_address_1 IS NULL THEN '-'ELSE svc.svc_address_1 END svc_address_1,
CASE WHEN svc.svc_address_2='null' THEN '-'WHEN svc.svc_address_2 IS NULL THEN '-'ELSE svc.svc_address_2 END svc_address_2,
CASE WHEN svc.svc_city='null' THEN '-'WHEN svc.svc_city IS NULL THEN '-'ELSE svc.svc_city END svc_city,
CASE WHEN svc.svc_county='null' THEN '-'WHEN svc.svc_county IS NULL THEN '-'ELSE svc.svc_county END svc_county,
CASE WHEN svc.svc_state='null' THEN '-'WHEN svc.svc_state IS NULL THEN '-'ELSE svc.svc_state END svc_state,
CASE WHEN svc.svc_zip_code='null' THEN '-'WHEN svc.svc_zip_code IS NULL THEN '-'ELSE svc.svc_zip_code END svc_zip_code,
CASE WHEN svc.svc_region_code='null' THEN '-' WHEN svc.svc_region_code IS NULL THEN '-'ELSE svc.svc_region_code END svc_region_code,
CASE WHEN concat(cst.ra_customer_number, ' ', '-', ' ', skl.skl_description)='null' THEN '-'WHEN concat(cst.ra_customer_number, ' ', '-', ' ', skl.skl_description) IS NULL THEN '-'ELSE concat(cst.ra_customer_number, ' ', '-', ' ', skl.skl_description) END facility_name,
CASE WHEN svc.svc_description ='null' THEN '-' WHEN svc.svc_description IS NULL THEN '-'ELSE svc.svc_description END svc,
datediff(now(),ar.ar_movement_time_stamp)+1 AS "Days_Aged",
CASE
WHEN ar.ar_barcode LIKE '%RPLN%' THEN "KCI Repair Loaner"
WHEN prd.prd_product_pk LIKE '%WNDSL%' THEN "KCI Repair Loaner"
WHEN prd.prd_product_pk LIKE '%WND%CO%' THEN "Owned Unit"
WHEN prd.prd_product_pk LIKE '%WNDINC%' THEN "Owned Unit"
ELSE "KCI Unit"
END Unit_Type,
CASE
WHEN aimcust.customer_sva_enabled_flag='null' THEN '-' WHEN aimcust.customer_sva_enabled_flag IS NULL THEN '-' ELSE aimcust.customer_sva_enabled_flag 
END customer_sva_enabled_flag,
--#MP CAST1-92
--# CTS update : removed nsl_name 
---t.geography_leader nsl_name, ----------------
AIMCUST.CUSTOMER_SHIPTO_MARKET_CATEGORY,
------#CTS Update : Added Market_segment
AIMCUST.customer_shipto_market_segment market_segment,
--#MP 1/12/2021 Added CASTP-164 - Gold_Customer_flag Add Gold Program Flag to aim_asset reporting table
CASE WHEN coalesce(cst_flags.gold_cust_flag,'null') ='null' THEN 'N'ELSE cst_flags.gold_cust_flag END gold_cust_flag
--#MP 1/12/2021 Added CASTP-160 - FACILITY ID, CHAIN NAME Add fields from Gold Customer Roster table to aim_asset reporting table.
,CASE WHEN length(coalesce(cst_rstr.facility_id,'')) = 0  THEN '-'ELSE cst_rstr.facility_id END facility_id
,CASE WHEN coalesce(cst_rstr.chain_name,'') = ''  THEN '-'ELSE cst_rstr.chain_name END chain_name
--#MP 1/12/2021 Added CASTP-165 - Columns gold_asset_barcode Add fields from Gold Customer Assets table to aim_asset reporting table.
,CASE WHEN coalesce(cst_asst.barcode,'') <> '' THEN cst_asst.barcode ELSE '-' END gold_asset_barcode
,ar.ar_row_bin,acd2.customer_class,mat.mat_mia_tracking_status,
mat.mat_contact_notes_1,MAT.mat_asset_retrieval_date,gc2.ct_description
FROM hero.genesis_product prd
INNER JOIN hero.genesis_asset_record ar ON ar.ar_product_fk=prd.prd_product_pk
INNER JOIN hero.genesis_stock_keeping_location skl ON ar.ar_skl_fk=skl.skl_pk
INNER JOIN hero.genesis_service_center svc ON ar.ar_svc_fk=svc.svc_pk
INNER JOIN hero.genesis_customer cst ON cast(cst.cst_pk AS string)=skl.skl_short_description
INNER JOIN hero.genesis_department dept ON ar.ar_dpt_fk=dept.dpt_pk
INNER JOIN
(SELECT customer_shipto_site_use_id,customer_shipto_account_number,customer_sva_enabled_flag,customer_shipto_key,CUSTOMER_SHIPTO_MARKET_CATEGORY,
---#CTS Update : Added Market_segment
customer_shipto_market_segment
FROM AIM.AIM_CUSTOMER_shipto_DIM
WHERE customer_shipto_status NOT LIKE 'PENDING'
AND customer_shipto_indirect_flag = 'N') aimcust ON cst.ra_shipto_site_id = aimcust.customer_shipto_site_use_id
INNER JOIN (SELECT * FROM aim.aim_customer_zipterr_xref_transposed_vw t
WHERE 
--# CTS update : Alignment
((t.sales_credit_type_code IN ('SSR','WHS','ATR','TSV','TMV','AWT','ISR','TSR')))
and (t.customer_key not in (select * from sand_cast.aim_order_Non_Home))
) t ON t.customer_key =aimcust.customer_shipto_key
--#MP Changed source where customer info is pulled from
left join  aim.aim_customer_dim	acd on acd.customer_account_number          = cst.ra_customer_number 
									and aimcust.customer_shipto_site_use_id = acd.customer_site_use_id
									and  customer_site_use_code             = 'SHIP_TO'  and customer_indirect_flag='N' 
LEFT  JOIN aim.aim_customer_flag_dim cst_flags on cst_flags.customer_key = acd.customer_key
LEFT  JOIN aim.gold_customer_roster cst_rstr on cst_rstr.customer_account_number  = cst.ra_customer_number
LEFT  JOIN aim.gold_customer_assets cst_asst on cst_asst.serial_number  = ar.ar_serial_number_pk 
LEFT  JOIN aim.aim_customer_dim acd2 on acd2.customer_account_number    = cst.ra_customer_number 
									and acd2.customer_site_use_id    	    = cst.ra_billto_site_id 
									and acd2.customer_site_use_code         = 'BILL_TO'  
									and acd2.customer_indirect_flag         = 'N' 
left join (select * from (select mat_asset_serial_number_fk ,mat_contact_notes_1,
            mat_asset_retrieval_date,mat_mia_tracking_status,mat_seq_pk ,row_number() 
            OVER (PARTITION BY mat_asset_serial_number_fk ORDER BY MAX(mat_seq_pk) DESC) AS rn
            from  hero.genesis_mia_asset_tracking
             group by 1,2,3,4,5) a where rn = 1) mat on mat.mat_asset_serial_number_fk = AR.ar_serial_number_pk 
left join hero.genesis_code gc2 on gc2.ct_column = 'mia_tracking_status' and gc2.ct_code = mat.mat_mia_tracking_status 
WHERE (t.region LIKE 'A%' AND t.region <>'A0'
AND ar.ar_product_fk LIKE 'WND%'
--#MP 1/12/2021 CASTP-156 expanded dpt description to include more than one description
AND upper(dept.dpt_description) IN ('STORAGE','FLOOR','MISSING IN ACTION','DISPATCHED','SOLD','PERMANENTLY OUT OF SERVICE','IN REPAIR',
							 'CLEAN AND WAITING QC','BRANCH READY','PROCESSING (DIRTY)'))) a ;
compute stats sand_cast.aim_asset_initial_layer;


--#MP 1/15/2021 [CASTP-165] #3.1-Add fields from Gold Customer Assets table to aim_asset reporting table.Add gold_asset_barcode as a new column in addition to BARCODE_NUMBER in order to verify if the gold customers asset barcode matches the asset barcode in aim_asset
--#MP 1/15/2021 [CASTP-156] #5-Update table filters in 'aim_asset' table to update Assets by Location.Additional dept_descriptions should flow through
--#MP 1/15/2021 [CASTP-164] #1.1-Add Gold Program (gold_cust_flag) Flag to aim_asset reporting table.
--#MP 1/15/2021 [CASTP-160] #2.1-Add fields from Gold Customer Roster table to aim_asset reporting table. Add FACILITY ID, CHAIN NAME
--#MP 6/15/2021 [CASTP-181] Classify Payor Type for each VAC(R) Rental Order in Asset Management context.
--#MP 1/15/2021 [CASTP-172] Add column ,mat_mia_tracking_status,mat_contact_notes_1,mat_asset_retrieval_date,ct_description
--#MP 1/15/2021 [CASTP-166] Add column ar_row_bin
DROP TABLE IF EXISTS sand_cast.aim_asset;
CREATE TABLE sand_cast.aim_asset stored as parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/aim_asset/' AS
SELECT A.product,A.ar_dept_fk,A.ar_svc_fk,A.skl_short_description,A.cst_pk,A.facility_number,
--# CTS update : Removed geography
--A.geography,
A.region,A.district,A.territory_code,
A.territory_name,B.salesrep_name AS salesrep_name,B.rvp_name AS regionvicepresident_name,B.dm_name AS districtmanager_name,
--#MP 11/22/2020 CASTP-176 
T.inside_sales_flag,T.ins_supervisor,T.supervisor_name,T.sales_credit_type_code jca_role,A.unit_number,A.barcode_number,
A.last_movement_date,A.dpt_description,A.svc_pk,A.svc_address_1,A.svc_address_2,A.svc_city,A.svc_county,A.svc_state,A.svc_zip_code,A.svc_region_code,
A.facility_name,A.svc,A.days_aged,A.unit_type,A.customer_sva_enabled_flag,
--# CTS update : removed nsl_name
--A.nsl_name,
--Added 1/12/2021 CASTP-160 - FACILITY ID, CHAIN NAME Add fields from Gold Customer Roster table to aim_asset reporting table.
--Added 1/12/2021 CASTP-164 - Gold_Customer_flag Add Gold Program Flag to aim_asset reporting table
--Added 1/12/2021 CASTP-165 - Columns gold_asset_barcode Add fields from Gold Customer Assets table to aim_asset reporting table.
gold_cust_flag,facility_id,chain_name,gold_asset_barcode, 
case
	when unit_type in('Owned Unit','KCI Repair Loaner') then 'Facility Owned'
	when a.customer_Class  in ('001 MEDICARE', '002 PATIENT', '007 INSURANCE', '008 MANAGED CARE', '009 MEDICAID', "012 WORKER\'S COMP", '016 OTHER') then 'Third Party'
		 ELSE 'Facility Pay'
end Payor_type,ar_row_bin,mat_contact_notes_1,mat_asset_retrieval_date
,mat_mia_tracking_status,ct_description 
FROM sand_cast.aim_asset_initial_layer A
INNER JOIN
(SELECT * FROM sand_cast.aim_order_rvp_dm_rep_names
WHERE territory_code IS NOT NULL) B on A.territory_code = B.territory_code AND A.region = B.region
--#MP 11/22/2020 CASTP-176 ADDED CODE TO insure territory alingments are correct
left join hero.genesis_customer gc on a.cst_pk=gc.cst_pk
left join aim.aim_customer_dim c   on c.customer_site_use_id=gc.ra_shipto_site_id and customer_account_number not like '%Z%' 
									  and customer_key>0 and customer_indirect_flag='N'
LEFT JOIN aim.aim_customer_zipterr_xref_transposed_vw t on t.customer_key = c.customer_key 
-- # CTS update : removed customer_shipto_market_category
							--and t.customer_shipto_market_category         = a.customer_shipto_market_category 
							and t.territory_code                          = a.territory_code 

							--#MP 11/22/2020 # CTS update : Alignment exclusion rule for market_segement and jca_role
WHERE (jca_role in('ATR','TSR','TMV') AND market_segment in( 1, 2, 4 , 5, 6, 17, 20, 40, 89))
OR (jca_role in ('SSR','WHS') AND market_segment IN ( 1, 2, 4 , 5, 6, 40, 89))
OR (jca_role in ('ISR','AWT','TSV') AND market_segment IN (14, 15, 16, 17, 20, 19, 33, 34, 50))
OR (jca_role in ('ATR','TSR') AND market_segment IN (14, 15, 16, 19, 33, 34, 50))
OR (jca_role in ('ATR','AWT','TSV','TMV') AND market_segment IN (22));  
compute stats sand_cast.aim_asset;

----------------------------------------------------------ASSET QUERY end -------------------------------------------------------------------------------------------------------
----------------------------------------------------------ASSET STORAGE QUERY START -------------------------------------------------------------------------------------------------------
--#MP 6/04/2021 [CASTP-163]#9-Create a Consignment Tracker model through creation of aim_gold_program_loaner_storage table
DROP TABLE IF EXISTS sand_cast.aim_asset_storage;
CREATE TABLE sand_cast.aim_asset_storage stored as parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/aim_asset_storage/' AS
select a.customer_account_number ,facility_name,
case when cast(approved_loaner_qty as int) >= 1 then 'Y' else 'N' end Loaner_approved,
a.approved_loaner_product_code,COALESCE(b.prd_product_pk,'-') STORED_PRODUCT,C.UNIT_NUMBER,C.barcode_number,a.approved_loaner_qty
,case when Storage_qty is null then 0 else Storage_qty end Quantity_in_Storage 
, NVL(Storage_qty,0)- cast(approved_loaner_qty as int) Gap
from aim.gold_customer_roster a 
left join 
(SELECT PRD.prd_product_pk, A.* FROM
	(select FACILITY_NUMBER, PRODUCT,COUNT(DISTINCT(UNIT_NUMBER)) Storage_QTY
		from sand_comops.aim_asset where dpt_description = 'STORAGE' GROUP BY 1,2 ) A
LEFT JOIN hero.genesis_product prd ON PRD.prd_description = PRODUCT
ORDER BY 1) b on b.facility_number = a.customer_account_number 
LEFT JOIN (SELECT FACILITY_NUMBER,UNIT_NUMBER,PRODUCT,barcode_number FROM sand_comops.aim_asset 
WHERE DPT_DESCRIPTION = 'STORAGE' GROUP BY 1,2,3,4 ORDER BY 1 ) C ON 
							C.FACILITY_NUMBER = A.customer_account_number 
							AND C.PRODUCT = CASE WHEN COALESCE(B.PRODUCT,'') = '' THEN C.PRODUCT
																			      ELSE B.PRODUCT END
order by customer_account_number;
compute stats sand_cast.aim_asset_storage;
----------------------------------------------------------ASSET STORAGE QUERY END -------------------------------------------------------------------------------------------------------
------------------------------------------------------------------ ATR TRANSITIONS QUERY start ------------------------------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS sand_cast.hero_Round_ATR_Transitions;
CREATE TABLE sand_cast.hero_Round_ATR_Transitions stored as parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/hero_Round_ATR_Transitions/' AS
SELECT
A.rpt_month as MONTH_ID,
CAST(concat(strleft(CAST(A.rpt_month as string),4),'-',strright(CAST(A.rpt_month as string),2),'-','01') as TIMESTAMP) AS RPT_MONTH,
--# CTS update : removed geography
--A.geography
A.REGION,A.rvp_name AS RVP_NAME,A.DISTRICT,
A.dm_name AS DM_NAME,A.territory_code AS TERRITORY,a.inside_sales_flag,
A.ins_supervisor,A.supervisor_name,
A.salesrep_name AS REP_NAME,
A.customer_shipto_market_category AS ACUTE_MKT_CATEGORY,
A.transition_facility_name AS ACUTE_CUSTOMER_NAME,
A.transition_facility_account_number AS ACUTE_CUSTOMER_ACCOUNT_NUMBER,
A.previous_ro AS ACUTE_RO_NUMBER,
A.ro_number AS POST_ACUTE_RO_NUMBER,
B.customer_shipto_market_category AS POST_ACUTE_MKT_CATEGORY,
B.customer_shipto_account_number AS POST_ACUTE_CUSTOMER_ACCOUNT_NUMBER,
B.customer_shipto_site_use_id AS POST_ACUTE_SHIPTO_SITE_USE_ID,
B.customer_shipto_city AS POST_ACUTE_CUSTOMER_CITY,
CAST(B.customer_shipto_zip_code AS STRING) AS POST_ACUTE_ZIP_CODE,
CAST(B.customer_shipto_zip4 AS STRING) AS POST_ACUTE_ZIP4,
CONCAT(CAST(B.customer_shipto_zip_code AS STRING),"-", CAST(B.customer_shipto_zip4 AS STRING)) AS POST_ACUTE_ZIP,
B.customer_shipto_state AS POST_ACUTE_CUSTOMER_STATE,
B.customer_shipto_name AS POST_ACUTE_CUSTOMER_NAME,
--#MP CAST1-92
	--# CTS update : removed nsl_name
-----a.geography_leader nsl_name,
(CASE WHEN A.territory_type IN ('AWT','RSM','TSV') THEN 3 
      WHEN A.territory_type IN ('ASM','RSMASC') THEN 4 
      WHEN A.territory_type IN ('ISR','DISR')  THEN 5
      WHEN A.territory_type IN ('TSR','DTSR') THEN 6 ELSE 2 END )  flag
FROM
/*GET ACUTE INFORMATION FOR TRANSITIONS*/
(
SELECT
f.rpt_month,f.order_key,f.acute_order_key,f.customer_shipto_key,f.customer_billto_key,f.created_orders,f.cancelled_orders,f.commissionable_orders,
f.transitions,s.customer_shipto_account_number transition_facility_account_number,s.customer_shipto_name transition_facility_name,
s.customer_shipto_city transition_facility_city,s.customer_shipto_zip_code transition_facility_zip_code,
s.customer_shipto_zip4 transition_facility_zip,pd.smr_product_group,pd.product_brand,pd.product_sub_brand,pd.product_sku,
p.rop_number,p.rop_effective_timestamp,p.rop_end_timestamp,p.previous_ro,p.ro_number,p.ro_ship_to_site_use_id,--p.prev_ro_ship_to_site_use_id,
p.rop_rec_flag,t.customer_shipto_market_category,
--# CTS update :removed geography
---t.geography,
t.region region,t.rvp_name,t.district,t.dm_name,t.territory_code,
substr(t.sales_credit_type_code,1,3) territory_type,
--#MP Mario Change here
t.salesrep_name,t.inside_sales_flag,t.ins_supervisor,t.supervisor_name
--# CTS update :removed nsl_name
----t.geography_leader
FROM aim.aim_ordrev_mthly_snapshot_fact f,
aim.aim_orders_dim p,
(SELECT * FROM aim.aim_product_dim WHERE etl_delete_flag <> 'Y') pd,
(select * from aim.aim_customer_zipterr_xref_transposed_vw where customer_key not in (select  * from sand_cast.aim_order_Non_Home))t,
 aim.aim_customer_shipto_dim s,
(SELECT * FROM reporting_comops.smrplus_product_segment_ins_mapping WHERE product='TRANSITIONS') b
--#MP Mario Change here
WHERE f.order_key = p.order_key
AND f.product_key=pd.product_key
AND t.customer_key = f.transition_facility_key 
AND t.customer_key = s.customer_shipto_key
--AND t.customer_shipto_market_category ='ACUTE'
--AND t.sales_credit_type_code in ('ATR') --Only on transition side Per Dinu 
AND t.region LIKE 'A%' AND t.region <>'A0'
AND s.customer_shipto_market_segment=b.mkt_segment
AND s.inside_sales_flag=b.inside_sales_flag
AND substr(t.sales_credit_type_code,1,3)=b.territory_type
AND f.rpt_month >= 201801
AND f.load_source = 'ORDERS'
AND f.transitions !=0
) A
LEFT JOIN
/*GET POST ACUTE DETAILS*/
(
SELECT
customer_shipto_key,customer_shipto_market_category,customer_shipto_account_number,customer_shipto_site_use_id,customer_shipto_city,
customer_shipto_zip_code,customer_shipto_zip4,customer_shipto_state,customer_shipto_name,customer_shipto_market_segment
FROM aim.aim_customer_shipto_dim
) B
ON A.customer_shipto_key = B.customer_shipto_key
-- # CTS update : market_segment similar to SMR
INNER JOIN (SELECT * FROM reporting_comops.smrplus_product_segment_ins_mapping WHERE product='LANDING_FACILITY_TRANSITION_MKT_SEGMENTS') C
ON A.territory_type=C.territory_type AND B.customer_shipto_market_segment=C.mkt_segment
WHERE A.territory_code NOT LIKE '%999%' 
GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26;
compute stats sand_cast.hero_round_ATR_transitions;

--#MP
--#MP 1/15/2021 [CASTP-138] #13-Update sand_cast.hero_round_pending to include orders with a 'Ship Pending' status.
--#MP 1/15/2021 [CASTP-141] #15-Add additional column to sand_cast.hero_round_pending from hero.genesis_rental_order_product which identify an order's 'Needed By' date. 
--#MP 1/15/2021 [CASTP-142] #20-Add additional column to sand_cast.hero_round_pending from hero.genesis_rental_order_product which identify an order's delivered date timestamp.
--#MP 1/15/2021 [CASTP-170] #16-Add additional column to sand_cast.hero_round_pending from hero.genesis_rental_order_product which identify an order's Pick Up Date.
--#MP 1/15/2021 [CASTP-158] #10-Identify product substitution within sand_cast.hero_round_pending for each VAC(R) Rental Order.
--#MP 1/15/2021 [CASTP-162] #6-Update the sand_cast.hero_round_pending table with barcode of delivered asset. 
--#MP 1/15/2021 [CASTP-236] #1.2-Add Gold Program Flag to hero_round_pending reporting table. - 
--#MP 1/15/2021 [CASTP-240] #2.2-Add fields from Gold Customer Roster table to hero_round_pending reporting table. - FACILITY ID, CHAIN NAME
--#MP 1/15/2021 [CASTP-239] #3.2-Add fields from Gold Customer Assets table to hero_round_pending reporting table - BARCODE as gold_asset_barcode
--#MP 4/26/2021 [CASTP-137] Update accounts where same account number has different facility name
--#MP 4/26/2021 [CASTP-146] #21-Identify the order source for each VAC(R) rental order created.
--#MP 4/26/2021 [CASTP-148] #27-Update reporting_comops.hero_round_pending to include a 'Time to Release' metric.
--#MP 4/26/2021 [CASTP-167] #17-Calculate the age of Pick Up work orders.
--#MP 4/26/2021 [CASTP-171] #22-Identify delivery method for each VAC(R) Rental Order.
--#MP 5/10/2021 [CASTP-161] #8-Flag Gold Asset Delivery errors in reporting_comops.hero_round_pending
DROP TABLE IF EXISTS sand_cast.hero_round_ATR_TRANS_initial_layer;
CREATE TABLE sand_cast.hero_round_ATR_TRANS_initial_layer stored AS parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/hero_round_ATR_TRANS_initial_layer/' AS
SELECT '-' AS item_description,'-' AS inventory_item_id,'-' AS roa_pm_fk,'-' AS ship_date,
'-' AS description,nvl(svc.svc_pk,'-') svc_pk,nvl(svc.svc_description,'-') svc_description,
--# CTS update :removed geography
---nvl(ro.geography,'-') geography,
--#MP Mario Change here
nvl(ro.region,'-') region,nvl(ro.district,'-') district,nvl(ro.TERRITORY,'-') TERRITORY_CODE,nvl(ro.inside_sales_flag,'_') inside_sales_flag,
nvl(ro.ins_supervisor,'-') ins_supervisor,nvl(ro.supervisor_name,'-') supervisor_name,cast(null as string) territory_name,
nvl(ro.rep_name,'-') salesrep_name_align,nvl(ro.rvp_name,'-') regionvicepresident_name,nvl(ro.dm_name,'-') districtmanager_name,
substr(ro.territory,1,3) jca_role,aimcust.customer_shipto_market_category AS market_category,
----#CTS Update : Added Market_Segment
aimcust.customer_shipto_market_segment AS market_segment,
CASE WHEN ra_customer_number = 'null' THEN '-'WHEN ra_customer_number IS NULL THEN '-'ELSE ra_customer_number END ra_customer_number,
nvl(cst.cst_pk,0) cst_pk,nvl(cst.cst_name,'-') cst_name,
-- # CTS Update : replaced market_category with market_segment logic
--#MP 4/26/2021 [CASTP-137]Update accounts where same account number has different facility name
CASE
WHEN aimcust.customer_shipto_market_segment IN(22) THEN CASE
WHEN concat(cgv.caregiver_account_number, ' ', '-', ' ', cgv.caregiver_name_1)='null' THEN 'NO CAREGIVER AVAILABLE'
WHEN concat(cgv.caregiver_account_number, ' ', '-', ' ', cgv.caregiver_name_1) IS NULL THEN 'NO CAREGIVER AVAILABLE'
ELSE concat(cgv.caregiver_account_number, ' ', '-', ' ', CASE WHEN cgv.caregiver_account_number = '0' THEN cgv.caregiver_name_1 ELSE cgv2.customer_name END)
END
ELSE CASE
WHEN concat(ra_customer_number, ' ', '-', ' ', cst_name)='null' THEN 'NO FACILITY AVAILABLE'
WHEN concat(ra_customer_number, ' ', '-', ' ', cst_name) IS NULL THEN 'NO FACILITY AVAILABLE'
ELSE concat(ra_customer_number, ' ', '-', ' ', cgv2.customer_name)
END
END Facility_Caregiver,
CASE
WHEN aimcust.customer_shipto_market_segment IN(22) THEN CASE
WHEN cgv.caregiver_account_number ='null' THEN 'NO CAREGIVER AVAILABLE'
WHEN cgv.caregiver_account_number IS NULL THEN 'NO CAREGIVER AVAILABLE'
ELSE cgv.caregiver_account_number
END
ELSE CASE
WHEN ra_customer_number ='null' THEN 'NO FACILITY AVAILABLE'
WHEN ra_customer_number IS NULL THEN 'NO FACILITY AVAILABLE'
ELSE ra_customer_number
END
END Facility_Number,
CASE
WHEN aimcust.customer_shipto_market_segment IN(22) THEN CASE
WHEN cgv.caregiver_physical_address_1 ='null' THEN '-'
WHEN cgv.caregiver_physical_address_1 IS NULL THEN '-'
ELSE cgv.caregiver_physical_address_1
END
ELSE CASE
WHEN cst.cst_ship_address_1='null' THEN '-'
WHEN cst.cst_ship_address_1 IS NULL THEN '-'
ELSE cst.cst_ship_address_1
END
END Facility_Caregiver_Address,
CASE
WHEN aimcust.customer_shipto_market_segment IN(22) THEN CASE
WHEN cgv.caregiver_physical_city ='null' THEN '-'
WHEN cgv.caregiver_physical_city IS NULL THEN '-'
ELSE cgv.caregiver_physical_city
END
ELSE CASE
WHEN cst.cst_ship_city ='null' THEN '-'
WHEN cst.cst_ship_city IS NULL THEN '-'
ELSE cst.cst_ship_city
END
END Facility_Caregiver_City,
CASE
WHEN aimcust.customer_shipto_market_segment IN(22) THEN CASE
WHEN cgv.caregiver_physical_zip ='null' THEN '-'
WHEN cgv.caregiver_physical_zip IS NULL THEN '-'
ELSE cgv.caregiver_physical_zip
END
ELSE CASE
WHEN cst.cst_ship_zip_code ='null' THEN '-'
WHEN cst.cst_ship_zip_code IS NULL THEN '-'
ELSE cst.cst_ship_zip_code
END
END Facility_Caregiver_Zip,
CASE
WHEN aimcust.customer_shipto_market_segment IN(22) THEN CASE
WHEN cgv.caregiver_physical_phone='null' THEN '-'
WHEN cgv.caregiver_physical_phone IS NULL THEN '-'
ELSE cgv.caregiver_physical_phone
END
ELSE CASE
WHEN cst.cst_contact_phone_number ='null' THEN '-'
WHEN cst.cst_contact_phone_number IS NULL THEN '-'
ELSE cst.cst_contact_phone_number
END
END Facility_Caregiver_Phone_Number,
CASE
WHEN cgv.caregiver_physical_address_2 = 'null' THEN '-'
WHEN cgv.caregiver_physical_address_2 IS NULL THEN '-'
ELSE cgv.caregiver_physical_address_2
END name_2,nvl(cgv.caregiver_bus_type,'-') caregiver_type,
CASE
WHEN concat(phy.physician_last_name, ', ', phy.physician_first_name) IS NULL THEN '-'
WHEN concat(phy.physician_last_name, ', ', phy.physician_first_name) ='null, null' THEN '-'
WHEN concat(phy.physician_last_name, ', ', phy.physician_first_name)=', ' THEN '-'
ELSE concat(phy.physician_last_name, ', ', phy.physician_first_name)
END physician_name,
nvl(cast(ro.ro_npi AS string),'-') ro_npi,
CASE
WHEN phy.physician_bus_mail_phone='null' THEN '-'
WHEN phy.physician_bus_mail_phone='nul-l-' THEN '-'
WHEN phy.physician_bus_mail_phone IS NULL THEN '-'
ELSE phy.physician_bus_mail_phone
END ro_doctor_phone,
CASE
WHEN aimcust.customer_shipto_market_segment IN(22) THEN nvl(cst.cst_name, '-')
WHEN aimcust.customer_shipto_market_segment IS NULL THEN '-' 
ELSE nvl(concat(ro.ro_patient_last_name, ", ", ro.ro_patient_first_name), '-')
END patient_name,
CASE
WHEN aimcust.customer_shipto_market_segment IN(22) THEN nvl(cst.cst_ship_address_1, '-')
ELSE '-'
END patient_adress,
CASE
WHEN aimcust.customer_shipto_market_segment IN(22) THEN nvl(cst.cst_ship_city, '-')
ELSE '-'
END patient_city,
CASE
WHEN aimcust.customer_shipto_market_segment IN(22) THEN nvl(cst.cst_ship_zip_code, '-')
ELSE '-'
END patient_zip,
CASE
WHEN aimcust.customer_shipto_market_segment IN(22) THEN '-' 
WHEN aimcust.customer_shipto_market_segment IS NULL THEN '-' 
ELSE
CASE
WHEN (from_unixtime(unix_timestamp(TO_DATE(ro.ro_patient_dob), 'yyyy-MM-dd'), 'MM-dd-yyyy'))='null' THEN '-'
WHEN (from_unixtime(unix_timestamp(TO_DATE(ro.ro_patient_dob), 'yyyy-MM-dd'), 'MM-dd-yyyy')) IS NULL THEN '-'
ELSE from_unixtime(unix_timestamp(TO_DATE(ro.ro_patient_dob), 'yyyy-MM-dd'), 'MM-dd-yyyy')
END END birth,
CASE
WHEN aimcust.customer_shipto_market_segment IN(22) THEN '-' 
WHEN aimcust.customer_shipto_market_segment IS NULL THEN '-' 
ELSE
CASE
WHEN ro_patient_location='null' THEN '-'
WHEN ro_patient_location IS NULL THEN '-'
ELSE ro_patient_location END END ro_patient_location,
CASE
WHEN aimcust.customer_shipto_market_segment IN(22) THEN '-'
WHEN aimcust.customer_shipto_market_segment IS NULL THEN '-' 
ELSE
CASE
WHEN ro_patient_phone='null' THEN '-'
WHEN ro_patient_phone IS NULL THEN '-'
ELSE ro_patient_phone END END ro_patient_phone,
nvl(prd.prd_description,'-') prd_description,
CASE
WHEN rop_ar_serial_number_fk='null' THEN '-'
WHEN rop_ar_serial_number_fk IS NULL THEN '-'
ELSE rop_ar_serial_number_fk END rop_ar_serial_number_fk,nvl(ro.POST_ACUTE_RO_NUMBER,0) ro_number,
nvl(rop.rop_status_code,'-') rop_status_code,
CASE
WHEN ro_purchase_order_number='null' THEN '-'
WHEN ro_purchase_order_number IS NULL THEN '-'
ELSE ro_purchase_order_number
END ro_purchase_order_number,nvl(cast(rop.rop_placed_time_stamp AS string),'-') rop_placed_time_stamp,
nvl(cast(rop.rop_last_round_time_stamp AS string),'-') rop_last_round_time_stamp,
nvl(cast(rop.rop_dc_time_stamp AS string),'-') rop_dc_time_stamp,
nvl(ro.ro_customer_group,'-') ro_customer_group,
nvl(rac.customer_number,'-') customer_number,
nvl(rac.customer_name,'-') customer_name,
CASE
WHEN rop_vac_transitionable_flag='null' THEN '-'
WHEN rop_vac_transitionable_flag IS NULL THEN '-'
ELSE rop_vac_transitionable_flag
END rop_vac_transitionable_flag,
nvl(rop.rop_pk,0) rop_pk,
CASE
WHEN rop_claims_release_emp_fk ='null' THEN '-'
WHEN rop_claims_release_emp_fk IS NULL THEN '-'
ELSE rop_claims_release_emp_fk
END CSR,
CASE
WHEN cst_ship_address_1='null' THEN '-'
WHEN cst_ship_address_1 IS NULL THEN '-'
ELSE cst_ship_address_1
END cst_ship_address_1,
CASE
WHEN cst_ship_address_2='null' THEN '-'
WHEN cst_ship_address_2 IS NULL THEN '-'
ELSE cst_ship_address_2
END cst_ship_address_2,
nvl(concat(cast(ro.ro_linked_ship_to_cst_fk AS string), ' ', '-', ' ', ro.ro_linked_facility_name),'-') Transitioned_Facility,
CASE
WHEN ro.ro_linked_facility_address1= 'null' THEN '-'
WHEN ro.ro_linked_facility_address1 IS NULL THEN '-'
ELSE ro.ro_linked_facility_address1
END Transitioned_Facility_address,
CASE
WHEN ro.ro_linked_facility_city= 'null' THEN '-'
WHEN ro.ro_linked_facility_city IS NULL THEN '-'
ELSE ro.ro_linked_facility_city
END Transitioned_Facility_city,
CASE
WHEN ro.ro_linked_facility_zip_code= 'null' THEN '-'
WHEN ro.ro_linked_facility_zip_code IS NULL THEN '-'
ELSE ro.ro_linked_facility_zip_code
END Transitioned_Facility_zip,nvl(cast(rop.rop_stop_bill_time_stamp AS string),'-') rop_stop_bill_time_stamp,
nvl(cast(rop.rop_start_bill_time_stamp AS string),'-') rop_start_bill_time_stamp,nvl(cast(rop.rop_created_time_stamp AS string),'-') rop_created_time_stamp,
nvl(cast(rop.rop_void_verify_time_stamp AS string),'-') rop_void_verify_time_stamp,
--#MP 1/15/2021 CASTP-138 Added PENDING PLACEMENT and SHIP PENDING. Removed Pending values.
CASE
   WHEN rop.rop_avail_for_ship_date IS NULL AND rop.rop_void_verify_time_stamp IS NULL AND rop.rop_placed_time_stamp IS NULL THEN 'SHIP PENDING'
   WHEN rop.rop_avail_for_ship_date IS NOT NULL AND rop.rop_placed_time_stamp IS NULL AND rop.rop_void_verify_time_stamp IS NULL THEN 'PENDING PLACEMENT'
   WHEN rop.rop_placed_time_stamp IS NOT NULL AND rop.rop_void_verify_time_stamp IS NULL AND rop.rop_stop_bill_time_stamp IS NULL THEN 'PLACED'
   WHEN rop.rop_void_verify_time_stamp IS NOT NULL THEN 'CANCELLED'
   WHEN rop.rop_placed_time_stamp IS NOT NULL AND rop.rop_void_verify_time_stamp IS NULL AND rop.rop_stop_bill_time_stamp IS NOT NULL THEN 'CLOSED'
END order_status,
CASE
WHEN concat(wo_requestor_account_number, ' ', '-', ' ', wo_caller_company)='null,null' THEN '-'
WHEN concat(wo_requestor_account_number, ' ', '-', ' ', wo_caller_company) IS NULL THEN '-'
WHEN (wo_requestor_account_number='null'OR wo_requestor_account_number IS NULL)AND (wo_caller_company<>'null'OR wo_caller_company IS NOT NULL) THEN wo_caller_company
WHEN (wo_requestor_account_number<>'null'OR wo_requestor_account_number IS NOT NULL)AND (wo_caller_company='null'OR wo_caller_company IS NULL) THEN wo_requestor_account_number
ELSE concat(wo_requestor_account_number, ' ', '-', ' ', wo_caller_company)
END requestor_name,
CASE
WHEN wo.wo_requestor_address_1= 'null' THEN '-'
WHEN wo.wo_requestor_address_1 IS NULL THEN '-'
ELSE wo.wo_requestor_address_1
END requestor_address,
CASE
WHEN wo.wo_requestor_city= 'null' THEN '-'
WHEN wo.wo_requestor_city IS NULL THEN '-'
ELSE wo.wo_requestor_city
END requestor_city,
CASE
WHEN wo.wo_requestor_zip_code= 'null'THEN '-'
WHEN wo.wo_requestor_zip_code IS NULL THEN '-'
ELSE wo.wo_requestor_zip_code
END requestor_zip,
CASE
WHEN wo.wo_caller_phone= 'null' THEN '-'
WHEN wo.wo_caller_phone IS NULL THEN '-'
ELSE wo.wo_caller_phone
END requestor_phone_number,
CASE
WHEN wo_rs_tracking_number='null' THEN '-'
WHEN wo_rs_tracking_number IS NULL THEN '-'
ELSE wo_rs_tracking_number
END tracking_number,
CASE
WHEN payor.customer_billto_name='null' THEN '-'
WHEN payor.customer_billto_name IS NULL THEN '-'
ELSE payor.customer_billto_name
END payer_name,
nvl(ro.ro_status_code,'-') ro_status_code,
CASE
WHEN aimcust.mmc_flag='null' THEN '-'
WHEN aimcust.mmc_flag IS NULL THEN '-'
ELSE aimcust.mmc_flag
END mmc_flag,
CASE
WHEN rop.rop_consign = 'HOM' THEN 'Y'
ELSE 'N'
END Ready_Care_Flag,
CASE
WHEN aimcust.customer_sva_enabled_flag='null' THEN '-'
WHEN aimcust.customer_sva_enabled_flag IS NULL THEN '-'
ELSE aimcust.customer_sva_enabled_flag
END customer_sva_enabled_flag,
CASE
WHEN ((aimcust.customer_sva_enabled_flag = 'Y')
AND (payor.customer_billto_name LIKE '%MARATHON%'
OR payor.customer_billto_name LIKE '%VA MEDICAL%'
OR payor.customer_billto_name LIKE '%V A MEDICAL%'
OR payor.customer_billto_name LIKE '%TRICAR%')) THEN 'Y'
ELSE 'N'
END VA_System,
CASE
WHEN rop.rop_free_reason_code IN ('60','45') THEN 'Y'ELSE 'N'END charity_care_Status,
CASE WHEN ro.ro_status_code IN ('SNC','VNC','MNC','HNC','ANC') THEN 'Y'ELSE 'N'END cba ,
CASE WHEN rop.rop_created_time_stamp>rop.rop_avail_for_ship_date THEN 'Rebill' ELSE '-'END Rebill,
CASE
WHEN wo_code.ct_description ='null' THEN '-'
WHEN wo_code.ct_description IS NULL THEN '-'
ELSE wo_code.ct_description
END wo_cancel_code_description,
CASE
WHEN void_code.ct_description ='null' THEN '-'
WHEN void_code.ct_description IS NULL THEN '-'
ELSE void_code.ct_description
END ROP_void_code_description,rop.rop_avail_for_ship_date,
--# CTS update :removed nsl_name
-----ro.nsl_name
case 
when (rop.rop_barcode like '%RPLN%') OR (rop.rop_product_used_fk like '%WNDSL%')  then 'KCI Repair Loaner' 
when (rop.rop_product_used_fk like '%WND%CO%') or (rop.rop_product_used_fk like '%WNDINC%') 	then 'Owned Unit'
else 'KCI Unit'
end Unit_type,
case 
when (rop.rop_product_ordered_fk = 'WNDAAI' and rop.rop_product_used_fk = 'WNDACT') THEN 'N'
when rop.rop_product_ordered_fk = rop.rop_product_used_fk  then 'N' 
else 'Y' 
end Substituted_Product,
rop.rop_needed_time_stamp,rop.rop_pickup_time_stamp,rop.rop_delivered_time_stamp,
CASE
WHEN coalesce(cst_flags.gold_cust_flag,'null') ='null' THEN 'N'
ELSE cst_flags.gold_cust_flag
END gold_cust_flag
,CASE
WHEN length(coalesce(cst_rstr.facility_id,'')) = 0  THEN '-'
ELSE cst_rstr.facility_id
END facility_id,
CASE WHEN coalesce(cst_rstr.chain_name,'') = ''  THEN '-' ELSE cst_rstr.chain_name END asset_owner_chain, --#MP 5/10/2021 [CASTP-161] 
CASE WHEN coalesce(cst_asst.chain,'') = ''  THEN '-' ELSE cst_asst.chain  END participant_chain, --#MP 5/10/2021 [CASTP-161]
CASE WHEN coalesce(cst_asst.barcode,'') <> '' THEN cst_asst.barcode
ELSE COALESCE(rop.rop_barcode,'-') END barcode
,rop.rop_order_source,sc.ct_description channel--#MP 4/26/2021 [CASTP-146]
, CASE  --#MP 4/26/2021 [CASTP-148]
   WHEN rop_needed_time_stamp IS NULL THEN (round((unix_timestamp(rop_avail_for_ship_date) - unix_timestamp(rop_created_time_stamp))/3600))
   WHEN rop_needed_time_stamp IS NOT NULL THEN (round((unix_timestamp(rop_needed_time_stamp) - unix_timestamp(rop_avail_for_ship_date))/3600))
  END Time_to_Release 
,case  --#MP 4/26/2021 [CASTP-167]
	when wo2.wo_type_code = 'PCK' and wo2.wo_status_code <> 'CLO' Then datediff(Now(), wo2.wo_created_time_stamp) 
	when wo2.wo_type_code = 'PCK' and wo2.wo_status_code = 'CLO' Then datediff(rop_pickup_time_stamp,wo2.wo_created_time_stamp)
end Pickup_age
,WO3.CT_DESCRIPTION WO_Transport_description --#MP 4/26/2021 [CASTP-171]
FROM
(SELECT a.*,b.* FROM sand_cast.hero_Round_ATR_Transitions a LEFT JOIN hero.genesis_rental_order b ON b.ro_pk = a.POST_ACUTE_RO_NUMBER) ro,
(SELECT * FROM hero.genesis_customer WHERE cst_pk NOT IN (9999999999,0,5555555555,8888888888,216200) ) cst,
hero.genesis_rental_order_product rop
INNER JOIN
(SELECT * FROM hero.genesis_service_center
WHERE svc_pk NOT IN ('341','405','0') )svc on(rop.ROP_SVC_FK = svc.SVC_PK)
LEFT OUTER JOIN hero.genesis_ro_claim_data cd on(ro.RO_PK=cd.ROC_RO_FK)
LEFT JOIN aim.aim_caregiver_dim cgv ON cgv.caregiver_mstrid = cd.roc_hha_smg_mstr_id_fk
LEFT OUTER JOIN hero.genesis_product prd on(rop.ROP_PRODUCT_USED_FK = prd.PRD_PRODUCT_PK)
LEFT OUTER JOIN
(SELECT wo_rop_fk,wo_requestor_account_number,wo_caller_company,wo_rs_tracking_number,wo_requestor_address_1,wo_requestor_city,
wo_requestor_zip_code,wo_caller_phone,wo_cancel_reason,wo_type_code
FROM hero.genesis_work_order
WHERE (wo_requestor_account_number<>'null'
OR wo_requestor_account_number IS NULL)
AND wo_type_code='DEL'
GROUP BY 1,2,3,4,5,6,7,8,9,10) wo
ON rop.ROP_PK = wo.WO_ROP_FK
LEFT OUTER JOIN hero.genesis_employee e on(e.EMP_PK = cst_last_order_emp_fk)
LEFT OUTER JOIN hero.genesis_ra_site_uses_all_snap rss ON (ro.RO_BILL_TO_SITE_USE_ID = rss.SITE_USE_ID)
LEFT OUTER JOIN hero.genesis_ra_addresses_all_snap ras ON (rss.ADDRESS_ID = ras.ADDRESS_ID)
LEFT OUTER JOIN hero.genesis_ra_customers_snap rac on(ras.CUSTOMER_ID = rac.CUSTOMER_ID)
LEFT OUTER JOIN
(SELECT *
FROM aim.aim_customer_shipto_dim
WHERE customer_shipto_status NOT LIKE 'PENDING'
AND customer_shipto_indirect_flag = 'N') aimcust ON cst.ra_shipto_site_id = aimcust.customer_shipto_site_use_id
----#MP Mario Change here
LEFT OUTER JOIN
(SELECT * FROM aim.aim_customer_zipterr_xref_transposed_vw t
--# CTS update : Alignment
     WHERE ((t.sales_credit_type_code IN ('SSR','WHS','ATR','TSV','TMV','AWT','ISR','TSR'))
         and(t.customer_key not in (select * from sand_cast.aim_order_Non_Home))) AND t.region LIKE 'A%' AND t.region <>'A0'
 ) t ON t.customer_key =aimcust.customer_shipto_key
LEFT OUTER JOIN aim.aim_customer_billto_dim payor ON ro.ro_bill_to_site_use_id=payor.customer_billto_site_use_id
LEFT OUTER JOIN hero.genesis_code wo_code ON ((upper(wo_code.ct_column) LIKE '%WO_CANCEL_REASON_ALL%')
AND wo_code.ct_code = wo_cancel_reason
AND wo.wo_rop_fk=rop.rop_pk)
LEFT OUTER JOIN hero.genesis_code void_code on((upper(void_code.ct_column) IN ('ROP_VOID_TYPE','ROP_VOID_TYPE_SUB_REQ'))
AND void_code.ct_code = rop_void_type_code
AND wo.wo_rop_fk=rop.rop_pk)
LEFT OUTER JOIN aim.aim_physician_dim phy ON phy.physician_npi = ro.ro_npi
--#MP
left join (select customer_account_number, customer_key
			from  aim.aim_customer_dim	
			where  customer_site_use_code = 'SHIP_TO'  and customer_indirect_flag='N'
			group by 1,2)acd
			on acd.customer_account_number = cst.ra_customer_number
LEFT  JOIN aim.aim_customer_flag_dim cst_flags on cst_flags.customer_key = acd.customer_key
LEFT  JOIN aim.gold_customer_roster cst_rstr on cst_rstr.customer_account_number  = cst.ra_customer_number
LEFT  JOIN aim.gold_customer_assets cst_asst on cst_asst.serial_number  = rop.rop_ar_serial_number_fk
--#MP CASTP-137 4/26/2021 Update accounts where same account number has different facility name
left join AIM.AIM_CUSTOMER_DIM CGV2 on 
	  CGV2.customer_site_use_code = CASE WHEN aimcust.customer_shipto_market_segment IN (22) 
			  						THEN 'CAREGIVER'ELSE 'SHIP_TO' END
		      and CGV2.CUSTOMER_ACCOUNT_NUMBER = CASE WHEN aimcust.customer_shipto_market_segment IN (22) 
		      										  THEN COALESCE(CGV.caregiver_account_number,'-99999') 
		      										  ELSE CST.ra_customer_number END 
		      and CGV2.customer_indirect_flag='N' and customer_Status <> 'PENDING'
--#MP 4/26/2021 [CASTP-146] #21-Identify the order source for each VAC(R) rental order created.
LEFT OUTER JOIN hero.genesis_code sc on sc.ct_column = 'order_source' and sc.ct_code = rop.rop_order_source
--#MP 4/26/2021 [CASTP-167] #17-Calculate the age of Pick Up work orders.
LEFT join (select a.* from (select wo_rop_fk,wo_type_code,wo_status_code,wo_created_time_stamp,row_number() OVER (PARTITION BY wo_rop_fk ORDER BY MAX(wo_created_time_stamp) DESC) AS rn
from hero.genesis_work_order where wo_type_code = 'PCK' group by 1,2,3,4) a where rn = 1) wo2 on wo2.wo_rop_fk = rop.rop_pk 
--#MP 4/26/2021 [CASTP-171] #22-Identify delivery method for each VAC(R) Rental Order.
left join (SELECT wo_rop_fk,A.CT_DESCRIPTION 
FROM (select a.WO_ROP_FK,B.CT_DESCRIPTION from (select wo_rop_fk,wo_type_code,wo_status_code,WO_TRANSPORT_METHOD_CODE
,wo_created_time_stamp,row_number() OVER (PARTITION BY wo_rop_fk ORDER BY MAX(wo_created_time_stamp) DESC) AS rn
from hero.genesis_work_order where  wo_rop_fk >0 AND wo_type_code = 'DEL' AND WO_STATUS_CODE = 'CLO'group by 1,2,3,4,5) A 
LEFT JOIN hero.genesis_code  B ON B.CT_COLUMN = 'wo_transport_method'
								 AND B.CT_CODE = A.WO_TRANSPORT_METHOD_CODE
where rn = 1) A) wo3 on rop.rop_pk = wo3.wo_rop_fk
WHERE ro.RO_CST_FK=cst.CST_PK
AND ro.RO_PK=rop.ROP_RO_FK AND (rop.rop_stop_bill_time_stamp IS NULL
OR round(datediff(now(), rop.rop_stop_bill_time_stamp)) <= 30) AND rop.ROP_PRODUCT_USED_FK LIKE 'WND%'
AND (svc.svc_pk IS NOT NULL OR svc_pk<>'000')
AND (rop.rop_void_verify_time_stamp IS NULL OR round(datediff(now(), rop.rop_void_verify_time_stamp)) <= 30)
--AND (aimcust.customer_shipto_market_segment NOT IN (51,52,53,99) OR aimcust.customer_shipto_market_segment IS NULL)
AND aimcust.customer_shipto_market_segment IS NOT NULL
AND ((concat(ro.ro_patient_last_name, ", ", ro.ro_patient_first_name)) NOT LIKE '%UNIT %'
AND (concat(ro.ro_patient_last_name, ", ", ro.ro_patient_first_name)) NOT LIKE '%PLUS%'
AND (concat(ro.ro_patient_last_name, ", ", ro.ro_patient_first_name)) NOT LIKE '%CUST%')
GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,
41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67
,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93
,94,95,96,97,98,99;
compute stats sand_cast.hero_round_ATR_TRANS_initial_layer;

---------------------------------------------------------------------- ATR TRANSITIONS QUERY end -------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------- OMP QUERY start ---------------------------------------------------------------------------------------------------------------
--#MP 1/15/2021 [CASTP-187] #26-Add additional columns to sand_cast.omp_data from hero.genesis_vac_wound_status_tracking which represent wound length, width, and depth.We need to delineate wound dimension by Length, Width, and Depth into separate columns
--#MP 1/15/2021 [CASTP-237] #1.3-Add Gold Program Flag to omp_data reporting table.

DROP TABLE IF EXISTS sand_cast.omp_data_initial_layer;
CREATE TABLE sand_cast.omp_data_initial_layer stored as parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/omp_data_initial_layer/' AS
SELECT WNDST.*,
CASE
WHEN WNDST.order_status='PENDING' THEN (round((unix_timestamp(trunc(now(), 'MI'))-unix_timestamp(rop_created_time_stamp))/3600))
WHEN WNDST.order_status='CLOSED' THEN nvl(round(datediff(WNDST.rop_stop_bill_time_stamp, WNDST.rop_start_bill_time_stamp))+1, 0)
WHEN WNDST.order_status='PLACED' THEN nvl(round(datediff(now(), WNDST.rop_start_bill_time_stamp))+1, 0)
WHEN WNDST.order_status='CANCELLED' THEN 0
END therapy_days
--# CTS update :removed nsl_name
--wndst.geography_leader nsl_name
FROM
(SELECT
--# CTS update :removed geography
--WOUND.geography geography,
WOUND.region region,
WOUND.district district,
WOUND.territory_code,WOUND.inside_sales_flag,wound.ins_supervisor,
wound.supervisor_name,
--# CTS update :removed nsl_name
--wound.geography_leader,
wound.territory_name,
Wound.jca_role jca_role,
--# CTS Update
Wound.customer_shipto_market_category customer_shipto_market_category,
---#CTS Update : Added Market Segment
Wound.market_segment,
wound.salesrep_name salesrep_name_align,
wound.regionvicepresident_name,
wound.districtmanager_name,
CASE
WHEN WOUND.caregiver_name='null' THEN '-'
WHEN WOUND.caregiver_name IS NULL THEN '-'
ELSE WOUND.caregiver_name
END caregiver_name,
CASE
WHEN Wound.caregiver_account_number='null' THEN '-'
WHEN Wound.caregiver_account_number IS NULL THEN '-'
ELSE Wound.caregiver_account_number
END caregiver_account_number,
CASE
WHEN Wound.caregiver_type='null' THEN '-'
WHEN Wound.caregiver_type IS NULL THEN '-'
ELSE Wound.caregiver_type
END caregiver_type,
nvl(WOUND.ro_number, 0) ro_number,
CASE
WHEN WOUND.patient_name='null' THEN '-'
WHEN WOUND.patient_name IS NULL THEN '-'
ELSE WOUND.patient_name
END patient_name,
CASE
WHEN (from_unixtime(unix_timestamp(TO_DATE(birth), 'yyyy-MM-dd'), 'MM-dd-yyyy'))='null' THEN '-'
WHEN (from_unixtime(unix_timestamp(TO_DATE(birth), 'yyyy-MM-dd'), 'MM-dd-yyyy')) IS NULL THEN '-'
ELSE from_unixtime(unix_timestamp(TO_DATE(birth), 'yyyy-MM-dd'), 'MM-dd-yyyy')
END birth,
CASE
WHEN WOUND.physician_name='null' THEN '-'
WHEN WOUND.physician_name IS NULL THEN '-'
WHEN WOUND.physician_name=', ' THEN '-'
ELSE WOUND.physician_name
END physician_name,
CASE
WHEN WOUND.physician_phone='null' THEN '-'
WHEN WOUND.physician_phone IS NULL THEN '-'
WHEN WOUND.physician_phone ='nul-l-' THEN '-'
ELSE WOUND.physician_phone
END physician_phone,
nvl(cast(wound.ro_npi AS string), '-') ro_npi,
CASE
WHEN wound.Transitioned_Facility='null' THEN '-'
WHEN wound.Transitioned_Facility IS NULL THEN '-'
ELSE wound.Transitioned_Facility
END Transitioned_Facility,
CASE
WHEN WOUND.payor_name='null' THEN '-'
WHEN WOUND.payor_name IS NULL THEN '-'
ELSE WOUND.payor_name
END payor_name,
CASE
WHEN WOUND.wound_type_description='null' THEN '-'
WHEN WOUND.wound_type_description IS NULL THEN '-'
ELSE WOUND.wound_type_description
END wound_type_description,
CASE
WHEN WOUND.wound_woundbed_appearance='null' THEN '-'
WHEN WOUND.wound_woundbed_appearance IS NULL THEN '-'
ELSE WOUND.wound_woundbed_appearance
END wound_woundbed_appearance,
CASE
WHEN WOUND.wound_boneexposed='null' THEN '-'
WHEN WOUND.wound_boneexposed IS NULL THEN '-'
ELSE WOUND.wound_boneexposed
END wound_boneexposed,
CASE
WHEN WOUND.wound_debri_type='null' THEN '-'
WHEN WOUND.wound_debri_type IS NULL THEN '-'
ELSE WOUND.wound_debri_type
END wound_debri_type,
nvl(cast(WOUND.patient_admit_date AS string), '-') patient_admit_date,
nvl(cast(WOUND.rop_stop_bill_time_stamp AS string), '-') rop_stop_bill_time_stamp,
nvl(cast(wound.rop_created_time_stamp AS string), '-') rop_created_time_stamp,
nvl(cast(wound.rop_start_bill_time_stamp AS string), '-') rop_start_bill_time_stamp,
CASE
WHEN WOUND.wound_status='null' THEN '-'
WHEN WOUND.wound_status IS NULL THEN '-'
ELSE WOUND.wound_status
END wound_status,
nvl(cast(WOUND.wound_number AS string), '-') wound_number,
nvl(cast(WOUND.wound_evaluation_date AS string), '-') wound_evaluation_date,
CASE
WHEN WOUND.wound_location_orientation_direction='null' THEN '-'
WHEN WOUND.wound_location_orientation_direction IS NULL THEN '-'
ELSE WOUND.wound_location_orientation_direction
END wound_location_orientation_direction,
CASE
WHEN WOUND.wound_exudate_code_description='null' THEN '-'
WHEN WOUND.wound_exudate_code_description IS NULL THEN '-'
ELSE WOUND.wound_exudate_code_description
END wound_exudate_code_description,
wound.vws_wound_length,	
wound.vws_wound_width, 	
wound.vws_wound_depth,
nvl(cast(round(WOUND.wound_size, 2) AS string), '-') wound_size,
WOUND.SNAP_Size_Criteria_Met,
CASE
WHEN WOUND.customer_sva_enabled_flag='null' THEN '-'
WHEN WOUND.customer_sva_enabled_flag IS NULL THEN '-'
ELSE WOUND.customer_sva_enabled_flag
END customer_sva_enabled_flag,
WOUND.SNAP_Tier,
WOUND.VA_System,
CASE
WHEN (WOUND.rop_avail_for_ship_date IS NULL
AND WOUND.rop_void_verify_time_stamp IS NULL)
OR (WOUND.rop_placed_time_stamp IS NULL
AND WOUND.rop_void_verify_time_stamp IS NULL) THEN 'PENDING'
WHEN WOUND.rop_placed_time_stamp IS NOT NULL
AND WOUND.rop_void_verify_time_stamp IS NULL
AND WOUND.rop_stop_bill_time_stamp IS NULL THEN 'PLACED'
WHEN WOUND.rop_void_verify_time_stamp IS NOT NULL THEN 'CANCELLED'
WHEN WOUND.rop_placed_time_stamp IS NOT NULL
AND WOUND.rop_void_verify_time_stamp IS NULL
AND WOUND.rop_stop_bill_time_stamp IS NOT NULL THEN 'CLOSED'
END order_status,
-- # CTS Update : replaced market_category with market_segment logic for inside_sales_management
case 
when market_segment IN ( 1, 2, 4 , 5, 6, 40, 89, 17, 20) and Placement_type = 'Acute Orders' and wound.jca_role = 'TSR' then 'INSIDE SALES SUPERVISOR'
when market_segment IN (14, 15, 16, 19, 33, 34, 50) and Placement_type IN ('Post Acute Transitions','Organic') and wound.jca_role = 'ISR' 
 then 'INSIDE SALES SUPERVISOR' ELSE ''
end inside_sales_managment,
CASE
WHEN coalesce(wound.gold_cust_flag,'null') ='null' THEN 'N'
ELSE wound.gold_cust_flag
END gold_cust_flag

FROM
(SELECT tmpf.patient_name,
tmpf.patient_admit_date,
tmpf.patient_ssn,
tmpf.birth,
tmpf.rop_stop_bill_time_stamp,
tmpf.rop_start_bill_time_stamp,
tmpf.rop_created_time_stamp,
tmpf.rop_avail_for_ship_date,
tmpf.rop_void_verify_time_stamp,
tmpf.rop_placed_time_stamp,
tmpf.caregiver_name,
tmpf.caregiver_account_number,
tmpf.caregiver_type,
tmpf.territory_code,
--#MP Mario Change here
tmpf.inside_sales_flag,tmpf.ins_supervisor,tmpf.supervisor_name,
--#MP CAST1-92
--# CTS update :removed nsl_name
---tmpf.geography_leader,
tmpf.territory_name,
tmpf.jca_role,
tmpf.salesrep_name,
--# CTS update :removed geography
----tmpf.geography, 
tmpf.region,
tmpf.regionvicepresident_name,
tmpf.district,
tmpf.districtmanager_name,
tmpf.payor_name,
tmpf.physician_name,
tmpf.physician_phone,
tmpf.ro_npi,
tmpf.Transitioned_Facility,
tmpf.ro_number,
tmpf.wound_number,
tmpf.wound_type_description,
tmpf.wound_location_orientation_direction,
tmpf.vws_wound_length,	
tmpf.vws_wound_width, 	
tmpf.vws_wound_depth,
TO_DATE(tmpf.wound_evaluation_date) AS wound_evaluation_date,
tmpf.wound_status,
tmpf.wound_exudate_code_description,
tmpf.wound_woundbed_appearance,
tmpf.vws_boneexposed AS wound_boneexposed,
tmpf.wound_debri_type,
tmpf.SNAP_Size_Criteria_Met,
tmpf.customer_sva_enabled_flag,
tmpf.SNAP_Tier,
tmpf.VA_System,
tmpf.customer_shipto_market_category,
---CTS Update : Added Market_segment
tmpf.market_segment,
-- # CTS Update : replaced market_category with market_segment logic for Placement_Type
CASE
WHEN market_segment IN ( 1, 2, 4 , 5, 6, 40, 89, 17, 20) THEN 'Acute Orders'
WHEN tmpf.Transitioned_Facility<>'-' AND market_segment IN (22, 14, 15, 16, 19, 33, 34, 50) THEN 'Post Acute Transitions'
WHEN tmpf.Transitioned_Facility='-'  AND market_segment IN (22, 14, 15, 16, 19, 33, 34, 50) THEN 'Organic'
END 'Placement_Type',
tmpf.gold_cust_flag,
SUM(tmpf.vws_wound_length * tmpf.vws_wound_width * tmpf.vws_wound_depth) AS wound_size,
---#CTS--Added JCA ROLE in PARTITION BY
row_number() OVER (PARTITION BY tmpf.ro_number,tmpf.jca_role,
tmpf.Wound_number ORDER BY MAX(tmpf.wound_evaluation_date) DESC) AS rn
FROM
(SELECT DISTINCT pta.name AS patient_name,
pta.admdate AS patient_admit_date,
pta.ssno patient_ssn,
pta.birth,
cgv.caregiver_name_1 AS caregiver_name,
cgv.caregiver_account_number,
cgv.caregiver_bus_type AS caregiver_type,
--#MP Mario Change here
s.territory_code,s.inside_sales_flag,s.ins_supervisor,s.supervisor_name,
cast(null as string) territory_name,
s.sales_credit_type_code jca_role,
s.salesrep_name,
--# CTS update :removed geography
--s.geography geography,
s.region region,
s.rvp_name regionvicepresident_name,
--#MP CAST1-92
--# CTS update :removed nsl_name
--s.geography_leader,
s.district,
s.dm_name districtmanager_name,
pd.payor_name,
CASE
WHEN concat(phy.physician_last_name, ', ', phy.physician_first_name) IS NULL THEN '-'
WHEN concat(phy.physician_last_name, ', ', phy.physician_first_name) ='null, null' THEN '-'
WHEN concat(phy.physician_last_name, ', ', phy.physician_first_name)=', ' THEN '-'
ELSE concat(phy.physician_last_name, ', ', phy.physician_first_name)
END physician_name,
ro.ro_npi,
ro.ro_cancel_time_stamp,
--#MP
nvl(concat(cast(ro.ro_linked_ship_to_cst_fk AS string), ' ', '-', ' ', ro.ro_linked_facility_name),'-') Transitioned_Facility,
CASE
WHEN phy.physician_bus_mail_phone = 'null' THEN '-'
WHEN phy.physician_bus_mail_phone IS NULL THEN '-'
WHEN phy.physician_bus_mail_phone ='nul-l-' THEN '-'
ELSE phy.physician_bus_mail_phone
END physician_phone,
ord.rop_avail_for_ship_date,
ord.rop_placed_time_stamp,
ord.rop_void_verify_time_stamp,
ord.rop_stop_bill_time_stamp,
ord.rop_start_bill_time_stamp,
ord.rop_created_time_stamp,
cs.customer_sva_enabled_flag,
CASE
WHEN ((SNAP_Size_Criteria_Met = 'Y')
AND (payor_name LIKE '%MARATHON%'OR payor_name LIKE '%VA MEDICAL%'OR payor_name LIKE '%V A MEDICAL%'
OR payor_name LIKE '%TRICAR%'OR payor_name LIKE '%MEDICARE DMAC%')
AND cgv.caregiver_bus_type IN ('HA','HAC')) THEN 'Tier 1'
WHEN SNAP_Size_Criteria_Met = 'N' THEN 'NA'
ELSE 'Tier 2'
END SNAP_Tier,
CASE
WHEN ((cs.customer_sva_enabled_flag = 'Y')OR (payor_name LIKE '%MARATHON%'OR payor_name LIKE '%VA MEDICAL%'
OR payor_name LIKE '%V A MEDICAL%'OR payor_name LIKE '%TRICAR%')) THEN 'Y'
ELSE 'N'
END VA_System,wnd.*,sw.territory_code AS wcr_territory_code,s.customer_shipto_market_category,
-----#CTS Update : Added Market Segment
cs.customer_market_segment market_segment,
gold_cust_flag
FROM delair.patient_ptmaster pta
JOIN delair.patient_ptorder pto ON pto.acctno = pta.acctno
AND pto.admitno = pta.admitno
JOIN
(SELECT rop_ro_fk,
rop_avail_for_ship_date,
rop_placed_time_stamp,
rop_void_verify_time_stamp,
rop_stop_bill_time_stamp,
rop_created_time_stamp,
rop_start_bill_time_stamp
FROM hero.genesis_rental_order_product
WHERE ROP_PRODUCT_USED_FK LIKE 'WND%'
GROUP BY 1,2,3,4,5,6,7) ord ON ord.rop_ro_fk=pto.ro_fk
AND coalesce(ord.rop_stop_bill_time_stamp, to_date(now())) >= date_sub(to_date(now()), 30)
JOIN delair.patient_ptaddr ptaddr ON ptaddr.acctno = pta.acctno
AND ptaddr.addrno = 1
JOIN hero.genesis_ro_claim_data roc ON roc.roc_ro_fk = pto.ro_fk
JOIN aim.aim_caregiver_dim cgv ON cgv.caregiver_mstrid = roc.roc_hha_smg_mstr_id_fk
JOIN hero.genesis_rental_order ro ON ro.ro_pk = pto.ro_fk
JOIN aim.aim_customer_dim cs ON cs.customer_site_use_id = ro.ro_ship_to_site_use_id
AND cs.customer_site_use_code = 'SHIP_TO'
AND cs.customer_indirect_flag = 'N'
-- #MP CHANGE BELOW
JOIN (SELECT *      FROM aim.aim_customer_zipterr_xref_transposed_vw t
--# CTS update : Alignment
 WHERE (t.sales_credit_type_code IN ('SSR','WHS','ATR','TSV','TMV','AWT','ISR','TSR'))
     ) s ON s.customer_key = cs.customer_key
JOIN aim.aim_customer_dim cb ON cb.customer_site_use_id = ro.ro_bill_to_site_use_id
AND cb.customer_site_use_code = 'BILL_TO'
JOIN aim.aim_payor_dim pd ON pd.payor_insno = cast(cb.customer_account_number AS bigint)
AND cb.customer_account_site_id = pd.payor_inssub
JOIN aim.aim_physician_dim phy ON phy.physician_npi = ro.ro_npi
JOIN
(SELECT tmp.*
FROM
(SELECT vwi.vwi_ro_fk AS ro_number,
vwi.vwi_wound_number AS wound_number,
gc3.ct_description AS wound_age,
vwt.vwt_wound_description AS wound_type_description,
concat(coalesce(gvwc3.vwc_description, ' '), '/', coalesce(gvwc4.vwc_description, ' '), '/', coalesce(gvwc5.vwc_description, ' ')) AS wound_location_orientation_direction,
vws.vws_wound_length,
vws.vws_wound_width,
vws.vws_wound_depth,
(CASE
WHEN vwi.vwi_ro_fk = 22525167 THEN to_date('2017-09-19')
ELSE vws.vws_evaluation_date
END) AS wound_evaluation_date,
gvwc7.vwc_description AS wound_status,
gvwc2.vwc_description AS wound_exudate_appearance,
gvwc1.vwc_description AS wound_exudate_code_description,
gvwc6.vwc_description AS wound_woundbed_appearance,
vws.vws_boneexposed,
vws.vws_wound_debri_date AS wound_debri_date,
gc4.ct_description AS wound_debri_type,
CASE
WHEN (vws.vws_wound_length <=13
AND vws.vws_wound_width <=13
AND vws.vws_wound_depth <= 1) THEN 'Y'
ELSE 'N'
END SNAP_Size_Criteria_Met,
row_number () OVER (PARTITION BY vwi_seq_pk,
vws_seq_pk 
ORDER BY gvwc7.vwc_description DESC) AS rownum
FROM hero.genesis_vac_wound_information vwi
JOIN hero.genesis_vac_wound_status_tracking vws ON vwi.vwi_seq_pk = vws.vws_vwi_seq_fk
JOIN hero.genesis_vac_wound_types vwt ON vwi.vwi_wound_type_code = vwt.vwt_wound_type_code
LEFT JOIN hero.genesis_vac_wound_codes gvwc1 ON vws.vws_wound_exudate_code=gvwc1.vwc_code
LEFT JOIN hero.genesis_vac_wound_codes gvwc2 ON vws.vws_exudate_app=gvwc2.vwc_code
LEFT JOIN hero.genesis_vac_wound_codes gvwc3 ON vwi.vwi_vwc_location = gvwc3.vwc_code AND gvwc3.vwc_type = 'L'
LEFT JOIN hero.genesis_vac_wound_codes gvwc4 ON vwi.vwi_vwc_orientation = gvwc4.vwc_code AND gvwc4.vwc_type = 'O'
LEFT JOIN hero.genesis_vac_wound_codes gvwc5 ON vwi.vwi_vwc_direction = gvwc5.vwc_code AND gvwc5.vwc_type = 'D'
LEFT JOIN hero.genesis_code gc3 ON vwi.vwi_wound_age=gc3.ct_code AND gc3.ct_column='vwi_wound_age'
LEFT JOIN hero.genesis_vac_wound_codes gvwc6 ON vws.vws_woundbed_app = gvwc6.vwc_code
LEFT JOIN hero.genesis_vac_wound_codes gvwc7 ON vws.vws_wound_status = gvwc7.vwc_code
LEFT JOIN hero.genesis_code gc4 ON vws.vws_wound_debri_type = gc4.ct_code AND gc4.ct_column = 'vws_wound_debri_type') tmp
WHERE tmp.rownum = 1 ) wnd ON wnd.ro_number = pto.ro_fk
--#MP change
LEFT JOIN (SELECT * FROM aim.aim_customer_zipterr_xref_transposed_vw WHERE region LIKE 'A%' AND region <>'A0'
and  customer_key not in (select * from sand_cast.aim_order_Non_Home)) sw ON sw.customer_key = cs.customer_key
-- # CTS Update
--AND cs.customer_market_category = sw.customer_shipto_market_category
AND sw.sales_credit_type_code = 'AE'
AND sw.salesrep_name <> 'KINETIC CONCEPTS' 
LEFT  JOIN aim.aim_customer_flag_dim cst_flags on cst_flags.customer_key = cs.customer_key
--# CTS Update : data restriction moved to final table 
-- where 
--  NOT (s.customer_shipto_market_category ='ACUTE' and s.sales_credit_type_code in('ISR'))
--      AND
--  NOT (s.customer_shipto_market_category in ('EXTENDED','HOME') AND nvl(concat(cast(ro.ro_linked_ship_to_cst_fk AS string), ' ', '-', ' ', ro.ro_linked_facility_name),'-')='-' and s.sales_credit_type_code in('TSR','ATR'))
) tmpf
--#MP 1/15/2021 [CASTP-305] Removed rn <= 3 check to allow all wound evaluations to flow through
WHERE tmpf.rop_void_verify_time_stamp IS NULL
GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27
,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50) wound
WHERE  rop_stop_bill_time_stamp IS NULL) WNDST ;
compute stats sand_cast.omp_data_initial_layer;

DROP TABLE IF EXISTS sand_cast.omp_data;
--#MP 1/15/2021 [CASTP-187] #26-Add additional columns to sand_cast.omp_data from hero.genesis_vac_wound_status_tracking which represent wound length, width, and depth.We need to delineate wound dimension by Length, Width, and Depth into separate columns
--#MP 1/15/2021 [CASTP-237] #1.3-Add Gold Program Flag to omp_data reporting table. 
CREATE TABLE sand_cast.omp_data stored as parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/omp_data/' AS
SELECT
--# CTS update :removed geography
--A.geography,
A.region,
A.district,
A.territory_code,
A.territory_name,
A.jca_role,
B.salesrep_name AS salesrep_name_align,
B.rvp_name AS regionvicepresident_name,
B.dm_name AS districtmanager_name,
A.inside_sales_flag,a.ins_supervisor,a.supervisor_name,
A.caregiver_name,
A.caregiver_account_number,
A.caregiver_type,
A.ro_number,
A.patient_name,
A.birth,
A.physician_name,
A.physician_phone,
A.ro_npi,
A.transitioned_facility,
A.payor_name,
A.wound_type_description,
A.wound_woundbed_appearance,
A.wound_boneexposed,
A.wound_debri_type,
A.patient_admit_date,
A.rop_stop_bill_time_stamp,
A.rop_created_time_stamp,
A.rop_start_bill_time_stamp,
A.wound_status,
A.wound_number,
A.wound_evaluation_date,
A.wound_location_orientation_direction,
A.wound_exudate_code_description,
A.vws_wound_length wound_length,
A.vws_wound_width wound_width, 
A.vws_wound_depth wound_depth,
A.wound_size,
A.snap_size_criteria_met,
A.customer_sva_enabled_flag,
A.snap_tier,
A.va_system,
A.order_status,
A.therapy_days,
--# CTS update :removed nsl_name
--A.nsl_name,
a.inside_sales_managment,a.gold_cust_flag 
FROM
(SELECT*
FROM sand_cast.omp_data_initial_layer
--# CTS Update : data restriction moved to final table instead of initial layer
-- where 
--  NOT (customer_shipto_market_category ='ACUTE' and jca_role in('ISR'))
--      AND
--  NOT (customer_shipto_market_category in ('EXTENDED','HOME') AND nvl(Transitioned_Facility,'-')='-' and jca_role in('TSR','ATR'))
 --# CTS update : Alignment exclusion rule for market_segement and jca_role
WHERE (jca_role in('ATR','TSR','TMV') AND market_segment in( 1, 2, 4 , 5, 6, 17, 20, 40, 89))
OR (jca_role in ('SSR','WHS') AND market_segment IN ( 1, 2, 4 , 5, 6, 40, 89))
OR (jca_role in ('ISR','AWT','TSV') AND market_segment IN (14, 15, 16, 17, 20, 19, 33, 34, 50))
OR (jca_role in ('ATR','TSR') AND market_segment IN (14, 15, 16, 19, 33, 34, 50))
OR (jca_role in ('ATR','AWT','TSV','TMV') AND market_segment IN (22)) 
) A
INNER JOIN
(SELECT *FROM sand_cast.aim_order_rvp_dm_rep_names WHERE territory_code is not null) B
WHERE A.territory_code = B.territory_code AND A.region = B.region;
compute stats sand_cast.omp_data;

----------------------------------------------------- OMP QUERY end --------------------------------------------------------------------------------------------------------------

--------------------------------------------------- ROUND QUERY start -------------------------------------------------------------------------------------------------------------

--#MP 1/15/2021 [CASTP-138] #13-Update sand_cast.hero_round_pending to include orders with a 'Ship Pending' status.
--#MP 1/15/2021 [CASTP-141] #15-Add additional column to sand_cast.hero_round_pending from hero.genesis_rental_order_product which identify an order's 'Needed By' date. 
--#MP 1/15/2021 [CASTP-142] #20-Add additional column to sand_cast.hero_round_pending from hero.genesis_rental_order_product which identify an order's delivered date timestamp.
--#MP 1/15/2021 [CASTP-170] #16-Add additional column to sand_cast.hero_round_pending from hero.genesis_rental_order_product which identify an order's Pick Up Date.
--#MP 1/15/2021 [CASTP-158] #10-Identify product substitution within sand_cast.hero_round_pending for each VAC(R) Rental Order.
--#MP 1/15/2021 [CASTP-162] #6-Update the sand_cast.hero_round_pending table with barcode of delivered asset. 
--#MP 1/15/2021 [CASTP-236] #1.2-Add Gold Program Flag to hero_round_pending reporting table. - 
--#MP 1/15/2021 [CASTP-240] #2.2-Add fields from Gold Customer Roster table to hero_round_pending reporting table. - FACILITY ID, CHAIN NAME
--#MP 1/15/2021 [CASTP-239] #3.2-Add fields from Gold Customer Assets table to hero_round_pending reporting table - BARCODE as gold_asset_barcode
--#MP 4/26/2021 [CASTP-146] #21-Identify the order source for each VAC(R) rental order created.
--#MP 4/26/2021 [CASTP-148] #27-Update reporting_comops.hero_round_pending to include a 'Time to Release' metric.
--#MP 4/26/2021 [CASTP-167] #17-Calculate the age of Pick Up work orders.
--#MP 4/26/2021 [CASTP-171] #22-Identify delivery method for each VAC(R) Rental Order.
--#MP 5/10/2021 [CASTP-161] #8-Flag Gold Asset Delivery errors in reporting_comops.hero_round_pending
DROP TABLE IF EXISTS sand_cast.hero_Round_initial_layer;
CREATE TABLE sand_cast.hero_Round_initial_layer stored AS parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/hero_Round_initial_layer/' AS
SELECT "-" AS item_description,
"-" AS inventory_item_id,
"-" AS roa_pm_fk,
"-" AS ship_date,
"-" AS description,
nvl(svc.svc_pk,'-') svc_pk,
nvl(svc.svc_description,'-') svc_description,
--# CTS update :removed geography
--nvl(t.geography,'-') geography,
nvl(t.region,'-') region,
nvl(t.district,'-') district,
nvl(t.TERRITORY_CODE,'-') TERRITORY_CODE,
--#MP Mario Change here
nvl(t.inside_sales_flag,'-') inside_sales_flag,
nvl(t.ins_supervisor,'-') ins_supervisor,nvl(t.supervisor_name,'-') supervisor_name,
cast(null as string) territory_name,
nvl(t.salesrep_name,'-') salesrep_name_align,
nvl(t.rvp_name,'-') regionvicepresident_name,
nvl(t.dm_name,'-') districtmanager_name,
nvl(t.sales_credit_type_code,'-') jca_role,
nvl(aimcust.customer_shipto_market_category,'-') Market_Category,
---CTS Update ---Added market segment
aimcust.customer_shipto_market_segment  market_Segment,
CASE
WHEN ra_customer_number = 'null' THEN '-'
WHEN ra_customer_number IS NULL THEN '-'
ELSE ra_customer_number
END ra_customer_number,
nvl(cst.cst_pk,0) cst_pk,
nvl(cst.cst_name,'-') cst_name,
-- # CTS Update : replaced market_category with market_segment logic
--#MP CASTP-137 4/26/2021 Update accounts where same account number has different facility name
CASE
WHEN aimcust.customer_shipto_market_segment IN (22) THEN CASE
WHEN concat(cgv.caregiver_account_number, ' ', '-', ' ', cgv.caregiver_name_1)='null' THEN 'NO CAREGIVER AVAILABLE'
WHEN concat(cgv.caregiver_account_number, ' ', '-', ' ', cgv.caregiver_name_1) IS NULL THEN 'NO CAREGIVER AVAILABLE'
ELSE concat(cgv.caregiver_account_number, ' ', '-', ' ', CASE WHEN cgv.caregiver_account_number = '0' THEN cgv.caregiver_name_1 ELSE cgv2.customer_name END)
END
ELSE CASE
WHEN concat(ra_customer_number, ' ', '-', ' ', cst_name)='null' THEN 'NO FACILITY AVAILABLE'
WHEN concat(ra_customer_number, ' ', '-', ' ', cst_name) IS NULL THEN 'NO FACILITY AVAILABLE'
ELSE concat(ra_customer_number, ' ', '-', ' ', cgv2.customer_name)
END
END Facility_Caregiver,
CASE
WHEN aimcust.customer_shipto_market_segment IN (22) THEN CASE
WHEN cgv.caregiver_account_number ='null' THEN 'NO CAREGIVER AVAILABLE'
WHEN cgv.caregiver_account_number IS NULL THEN 'NO CAREGIVER AVAILABLE'
ELSE cgv.caregiver_account_number
END
ELSE CASE
WHEN ra_customer_number ='null' THEN 'NO FACILITY AVAILABLE'
WHEN ra_customer_number IS NULL THEN 'NO FACILITY AVAILABLE'
ELSE ra_customer_number
END
END Facility_Number,
CASE
WHEN aimcust.customer_shipto_market_segment IN (22) THEN CASE
WHEN cgv.caregiver_physical_address_1 ='null' THEN '-'
WHEN cgv.caregiver_physical_address_1 IS NULL THEN '-'
ELSE cgv.caregiver_physical_address_1
END
ELSE CASE
WHEN cst.cst_ship_address_1='null' THEN '-'
WHEN cst.cst_ship_address_1 IS NULL THEN '-'
ELSE cst.cst_ship_address_1
END
END Facility_Caregiver_Address,
CASE
WHEN aimcust.customer_shipto_market_segment IN (22) THEN CASE
WHEN cgv.caregiver_physical_city ='null' THEN '-'
WHEN cgv.caregiver_physical_city IS NULL THEN '-'
ELSE cgv.caregiver_physical_city
END
ELSE CASE
WHEN cst.cst_ship_city ='null' THEN '-'
WHEN cst.cst_ship_city IS NULL THEN '-'
ELSE cst.cst_ship_city
END
END Facility_Caregiver_City,
CASE
WHEN aimcust.customer_shipto_market_segment IN (22) THEN CASE
WHEN cgv.caregiver_physical_zip ='null' THEN '-'
WHEN cgv.caregiver_physical_zip IS NULL THEN '-'
ELSE cgv.caregiver_physical_zip
END
ELSE CASE
WHEN cst.cst_ship_zip_code ='null' THEN '-'
WHEN cst.cst_ship_zip_code IS NULL THEN '-'
ELSE cst.cst_ship_zip_code
END
END Facility_Caregiver_Zip,
CASE
WHEN aimcust.customer_shipto_market_segment IN (22) THEN CASE
WHEN cgv.caregiver_physical_phone='null' THEN '-'
WHEN cgv.caregiver_physical_phone IS NULL THEN '-'
ELSE cgv.caregiver_physical_phone
END
ELSE CASE
WHEN cst.cst_contact_phone_number ='null' THEN '-'
WHEN cst.cst_contact_phone_number IS NULL THEN '-'
ELSE cst.cst_contact_phone_number
END
END Facility_Caregiver_Phone_Number,
CASE
WHEN cgv.caregiver_physical_address_2 = 'null' THEN '-'
WHEN cgv.caregiver_physical_address_2 IS NULL THEN '-'
ELSE cgv.caregiver_physical_address_2
END name_2,
nvl(cgv.caregiver_bus_type,'-') caregiver_type,
CASE
WHEN concat(phy.physician_last_name, ', ', phy.physician_first_name) IS NULL THEN '-'
WHEN concat(phy.physician_last_name, ', ', phy.physician_first_name) ='null, null' THEN '-'
WHEN concat(phy.physician_last_name, ', ', phy.physician_first_name)=', ' THEN '-'
ELSE concat(phy.physician_last_name, ', ', phy.physician_first_name)
END physician_name,
nvl(cast(ro.ro_npi AS string),'-') ro_npi,
CASE
WHEN phy.physician_bus_mail_phone='null' THEN '-'
WHEN phy.physician_bus_mail_phone='nul-l-' THEN '-'
WHEN phy.physician_bus_mail_phone IS NULL THEN '-'
ELSE phy.physician_bus_mail_phone
END ro_doctor_phone,
CASE
WHEN aimcust.customer_shipto_market_segment IN(22) THEN nvl(cst.cst_name, '-')
WHEN aimcust.customer_shipto_market_segment IS NULL THEN '-' 
ELSE nvl(concat(ro.ro_patient_last_name, ", ", ro.ro_patient_first_name), '-')
END patient_name,
CASE
WHEN aimcust.customer_shipto_market_segment IN(22) THEN nvl(cst.cst_ship_address_1, '-')
ELSE '-'
END patient_adress,
CASE
WHEN aimcust.customer_shipto_market_segment IN(22) THEN nvl(cst.cst_ship_city, '-')
ELSE '-'
END patient_city,
CASE
WHEN aimcust.customer_shipto_market_segment IN(22) THEN nvl(cst.cst_ship_zip_code, '-')
ELSE '-'
END patient_zip,
CASE
WHEN aimcust.customer_shipto_market_segment IN(22) THEN '-' 
WHEN aimcust.customer_shipto_market_segment IS NULL THEN '-' 
ELSE
CASE
WHEN (from_unixtime(unix_timestamp(TO_DATE(ro.ro_patient_dob), 'yyyy-MM-dd'), 'MM-dd-yyyy'))='null' THEN '-'
WHEN (from_unixtime(unix_timestamp(TO_DATE(ro.ro_patient_dob), 'yyyy-MM-dd'), 'MM-dd-yyyy')) IS NULL THEN '-'
ELSE from_unixtime(unix_timestamp(TO_DATE(ro.ro_patient_dob), 'yyyy-MM-dd'), 'MM-dd-yyyy')
END END birth,
CASE
WHEN aimcust.customer_shipto_market_segment IN(22) THEN '-' 
WHEN aimcust.customer_shipto_market_segment IS NULL THEN '-' 
ELSE
CASE
WHEN ro_patient_location='null' THEN '-'
WHEN ro_patient_location IS NULL THEN '-'
ELSE ro_patient_location END END ro_patient_location,
CASE
WHEN aimcust.customer_shipto_market_segment IN(22) THEN '-'
WHEN aimcust.customer_shipto_market_segment IS NULL THEN '-' 
ELSE
CASE
WHEN ro_patient_phone='null' THEN '-'
WHEN ro_patient_phone IS NULL THEN '-'
ELSE ro_patient_phone END END ro_patient_phone,
nvl(prd.prd_description,'-') prd_description,
CASE
WHEN rop_ar_serial_number_fk='null' THEN '-'
WHEN rop_ar_serial_number_fk IS NULL THEN '-'
ELSE rop_ar_serial_number_fk
END rop_ar_serial_number_fk,
nvl(ro.ro_pk,0) ro_number,nvl(rop.rop_status_code,'-') rop_status_code,
CASE
WHEN ro_purchase_order_number='null' THEN '-'
WHEN ro_purchase_order_number IS NULL THEN '-'
ELSE ro_purchase_order_number
END ro_purchase_order_number,nvl(cast(rop.rop_placed_time_stamp AS string),'-') rop_placed_time_stamp
,nvl(cast(rop.rop_last_round_time_stamp AS string),'-') rop_last_round_time_stamp
,nvl(cast(rop.rop_dc_time_stamp AS string),'-') rop_dc_time_stamp
,nvl(ro.ro_customer_group,'-') ro_customer_group
,nvl(rac.customer_number,'-') customer_number,nvl(rac.customer_name,'-') customer_name,
CASE
WHEN rop_vac_transitionable_flag='null' THEN '-'
WHEN rop_vac_transitionable_flag IS NULL THEN '-'
ELSE rop_vac_transitionable_flag
END rop_vac_transitionable_flag,
nvl(rop.rop_pk,0) rop_pk,
CASE
WHEN rop_claims_release_emp_fk ='null' THEN '-'
WHEN rop_claims_release_emp_fk IS NULL THEN '-'
ELSE rop_claims_release_emp_fk
END CSR,
CASE
WHEN cst_ship_address_1='null' THEN '-'
WHEN cst_ship_address_1 IS NULL THEN '-'
ELSE cst_ship_address_1
END cst_ship_address_1,
CASE
WHEN cst_ship_address_2='null' THEN '-'
WHEN cst_ship_address_2 IS NULL THEN '-'
ELSE cst_ship_address_2
END cst_ship_address_2,
nvl(concat(cast(ro.ro_linked_ship_to_cst_fk AS string), ' ', '-', ' ', ro.ro_linked_facility_name),'-') Transitioned_Facility,
CASE
WHEN ro.ro_linked_facility_address1= 'null' THEN '-'
WHEN ro.ro_linked_facility_address1 IS NULL THEN '-'
ELSE ro.ro_linked_facility_address1
END Transitioned_Facility_address,
CASE
WHEN ro.ro_linked_facility_city= 'null' THEN '-'
WHEN ro.ro_linked_facility_city IS NULL THEN '-'
ELSE ro.ro_linked_facility_city
END Transitioned_Facility_city,
CASE
WHEN ro.ro_linked_facility_zip_code= 'null' THEN '-'
WHEN ro.ro_linked_facility_zip_code IS NULL THEN '-'
ELSE ro.ro_linked_facility_zip_code
END Transitioned_Facility_zip,nvl(cast(rop.rop_stop_bill_time_stamp AS string),'-') rop_stop_bill_time_stamp
,nvl(cast(rop.rop_start_bill_time_stamp AS string),'-') rop_start_bill_time_stamp
,nvl(cast(rop.rop_created_time_stamp AS string),'-') rop_created_time_stamp,
nvl(cast(rop.rop_void_verify_time_stamp AS string),'-') rop_void_verify_time_stamp,
--#MP
CASE
   WHEN rop_avail_for_ship_date IS NULL AND rop_void_verify_time_stamp IS NULL AND rop_placed_time_stamp IS NULL THEN 'SHIP PENDING'
   WHEN rop_avail_for_ship_date IS NOT NULL AND rop_placed_time_stamp IS NULL AND rop_void_verify_time_stamp IS NULL THEN 'PENDING PLACEMENT'
   WHEN rop_placed_time_stamp IS NOT NULL AND rop_void_verify_time_stamp IS NULL AND rop_stop_bill_time_stamp IS NULL THEN 'PLACED'
   WHEN rop_void_verify_time_stamp IS NOT NULL THEN 'CANCELLED'
   WHEN rop_placed_time_stamp IS NOT NULL AND rop_void_verify_time_stamp IS NULL AND rop_stop_bill_time_stamp IS NOT NULL THEN 'CLOSED'
END order_status,
CASE
WHEN concat(wo_requestor_account_number, ' ', '-', ' ', wo_caller_company)='null,null' THEN '-'
WHEN concat(wo_requestor_account_number, ' ', '-', ' ', wo_caller_company) IS NULL THEN '-'
WHEN (wo_requestor_account_number='null'OR wo_requestor_account_number IS NULL)
AND (wo_caller_company<>'null'OR wo_caller_company IS NOT NULL) THEN wo_caller_company
WHEN (wo_requestor_account_number<>'null'OR wo_requestor_account_number IS NOT NULL)AND (wo_caller_company='null'
OR wo_caller_company IS NULL) THEN wo_requestor_account_number
ELSE concat(wo_requestor_account_number, ' ', '-', ' ', wo_caller_company)
END requestor_name,
CASE
WHEN wo.wo_requestor_address_1= 'null' THEN '-'
WHEN wo.wo_requestor_address_1 IS NULL THEN '-'
ELSE wo.wo_requestor_address_1
END requestor_address,
CASE
WHEN wo.wo_requestor_city= 'null' THEN '-'
WHEN wo.wo_requestor_city IS NULL THEN '-'
ELSE wo.wo_requestor_city
END requestor_city,
CASE
WHEN wo.wo_requestor_zip_code= 'null'THEN '-'
WHEN wo.wo_requestor_zip_code IS NULL THEN '-'
ELSE wo.wo_requestor_zip_code
END requestor_zip,
CASE
WHEN wo.wo_caller_phone= 'null' THEN '-'
WHEN wo.wo_caller_phone IS NULL THEN '-'
ELSE wo.wo_caller_phone
END requestor_phone_number,
CASE
WHEN wo_rs_tracking_number='null' THEN '-'
WHEN wo_rs_tracking_number IS NULL THEN '-'
ELSE wo_rs_tracking_number
END tracking_number,
CASE
WHEN payor.customer_billto_name ='null' THEN '-'
WHEN payor.customer_billto_name IS NULL THEN '-'
ELSE payor.customer_billto_name
END payer_name,
nvl(ro.ro_status_code,'-') ro_status_code,
CASE
WHEN aimcust.mmc_flag='null' THEN '-'
WHEN aimcust.mmc_flag IS NULL THEN '-'
ELSE aimcust.mmc_flag
END mmc_flag,
CASE
WHEN rop.rop_consign = 'HOM' THEN 'Y'
ELSE 'N'
END Ready_Care_Flag,
CASE
WHEN aimcust.customer_sva_enabled_flag='null' THEN '-'
WHEN aimcust.customer_sva_enabled_flag IS NULL THEN '-'
ELSE aimcust.customer_sva_enabled_flag
END customer_sva_enabled_flag,
CASE
WHEN ((aimcust.customer_sva_enabled_flag = 'Y')
OR (payor.customer_billto_name LIKE '%MARATHON%'
OR payor.customer_billto_name LIKE '%VA MEDICAL%'
OR payor.customer_billto_name LIKE '%V A MEDICAL%'
OR payor.customer_billto_name LIKE '%TRICAR%')) THEN 'Y'
ELSE 'N'
END VA_System,
CASE
WHEN rop.rop_free_reason_code IN ('60','45') THEN 'Y'
ELSE 'N'
END charity_care_Status,
CASE
WHEN ro.ro_status_code IN ('SNC','VNC','MNC','HNC','ANC') THEN 'Y'
ELSE 'N'
END cba ,
CASE
WHEN rop.rop_created_time_stamp>rop.rop_avail_for_ship_date THEN 'Rebill'
ELSE '-'
END Rebill,
CASE
WHEN wo_code.ct_description ='null' THEN '-'
WHEN wo_code.ct_description IS NULL THEN '-'
ELSE wo_code.ct_description
END wo_cancel_code_description,
CASE
WHEN void_code.ct_description ='null' THEN '-'
WHEN void_code.ct_description IS NULL THEN '-'
ELSE void_code.ct_description
END ROP_void_code_description,rop.rop_avail_for_ship_date,
--#MP CAST1-92
--# CTS update :removed nsl_name
--t.geography_leader nsl_name,
case 
when (rop.rop_barcode like '%RPLN%') OR (rop.rop_product_used_fk like '%WNDSL%')  then 'KCI Repair Loaner' 
when (rop.rop_product_used_fk like '%WND%CO%') or (rop.rop_product_used_fk like '%WNDINC%') 	then 'Owned Unit'
else 'KCI Unit'
end Unit_type,
case 
when (rop.rop_product_ordered_fk = 'WNDAAI' and rop.rop_product_used_fk = 'WNDACT') THEN 'N'
when rop.rop_product_ordered_fk = rop.rop_product_used_fk  then 'N' 
else 'Y'
end Substituted_Product,
rop.rop_needed_time_stamp,rop.rop_pickup_time_stamp,rop.rop_delivered_time_stamp,
CASE
WHEN coalesce(cst_flags.gold_cust_flag,'null') ='null' THEN 'N'
ELSE cst_flags.gold_cust_flag
END gold_cust_flag
,CASE
WHEN length(coalesce(cst_rstr.facility_id,'')) = 0  THEN '-'
ELSE cst_rstr.facility_id
END facility_id,
CASE WHEN coalesce(cst_rstr.chain_name,'') = ''  THEN '-'ELSE cst_rstr.chain_name END asset_owner_chain,
CASE WHEN coalesce(cst_asst.chain,'') = ''  THEN '-' ELSE cst_asst.chain END participant_chain,
CASE WHEN coalesce(cst_asst.barcode,'') <> '' THEN cst_asst.barcode
ELSE COALESCE(rop.rop_barcode,'-') END barcode
--#MP 4/26/2021 [CASTP-146]
,rop.rop_order_source,sc.ct_description channel
--#MP 4/26/2021 [CASTP-148]
, CASE
   WHEN rop_needed_time_stamp IS NULL THEN (round((unix_timestamp(rop_avail_for_ship_date) - unix_timestamp(rop_created_time_stamp))/3600))
   WHEN rop_needed_time_stamp IS NOT NULL THEN (round((unix_timestamp(rop_needed_time_stamp) - unix_timestamp(rop_avail_for_ship_date))/3600))
  END Time_to_Release 
,case  --#MP 4/26/2021 [CASTP-167]
	when wo2.wo_type_code = 'PCK' and wo2.wo_status_code <> 'CLO' Then datediff(Now(), wo2.wo_created_time_stamp) 
	when wo2.wo_type_code = 'PCK' and wo2.wo_status_code = 'CLO' Then datediff(rop_pickup_time_stamp,wo2.wo_created_time_stamp)
end Pickup_age
,WO3.CT_DESCRIPTION WO_Transport_description --#MP 4/26/2021 [CASTP-171]
FROM hero.genesis_rental_order ro,
(SELECT * FROM hero.genesis_customer WHERE cst_pk NOT IN (9999999999,0,5555555555,8888888888,216200) ) cst,
hero.genesis_rental_order_product rop
INNER JOIN (SELECT * FROM hero.genesis_service_center WHERE svc_pk NOT IN ('341','405','0') )svc on(rop.ROP_SVC_FK = svc.SVC_PK)
LEFT OUTER JOIN hero.genesis_ro_claim_data cd on(ro.RO_PK=cd.ROC_RO_FK)
LEFT JOIN aim.aim_caregiver_dim cgv ON cgv.caregiver_mstrid = cd.roc_hha_smg_mstr_id_fk
LEFT OUTER JOIN hero.genesis_product prd on(rop.ROP_PRODUCT_USED_FK = prd.PRD_PRODUCT_PK)
LEFT OUTER JOIN
(SELECT wo_rop_fk,
wo_requestor_account_number,
wo_caller_company,
wo_rs_tracking_number,
wo_requestor_address_1,
wo_requestor_city,
wo_requestor_zip_code,
wo_caller_phone,
wo_cancel_reason,
wo_type_code
FROM hero.genesis_work_order
WHERE (wo_requestor_account_number<>'null' OR wo_requestor_account_number IS NULL) AND wo_type_code='DEL'
GROUP BY 1,	2,	3,	4,	5,	6,	7,	8,	9,	10) wo ON rop.ROP_PK = wo.WO_ROP_FK
LEFT OUTER JOIN hero.genesis_employee e on(e.EMP_PK = cst_last_order_emp_fk)
LEFT OUTER JOIN hero.genesis_ra_site_uses_all_snap rss ON (ro.RO_BILL_TO_SITE_USE_ID = rss.SITE_USE_ID)
LEFT OUTER JOIN hero.genesis_ra_addresses_all_snap ras ON (rss.ADDRESS_ID = ras.ADDRESS_ID)
LEFT OUTER JOIN hero.genesis_ra_customers_snap rac on(ras.CUSTOMER_ID = rac.CUSTOMER_ID)
LEFT OUTER JOIN
(SELECT * FROM aim.aim_customer_shipto_dim WHERE customer_shipto_status NOT LIKE 'PENDING'AND customer_shipto_indirect_flag = 'N') aimcust
       ON cst.ra_shipto_site_id = aimcust.customer_shipto_site_use_id
-- #MP change below
LEFT OUTER JOIN
(SELECT *
FROM aim.aim_customer_zipterr_xref_transposed_vw t
-- # CTS update : Alignment
WHERE (t.sales_credit_type_code IN ('SSR','WHS','ATR','TSV','TMV','AWT','ISR','TSR'))
AND t.region LIKE 'A%' AND t.region <>'A0') t ON t.customer_key =aimcust.customer_shipto_key
LEFT OUTER JOIN aim.aim_customer_billto_dim payor ON ro.ro_bill_to_site_use_id=payor.customer_billto_site_use_id
LEFT OUTER JOIN hero.genesis_code wo_code ON ((upper(wo_code.ct_column) LIKE '%WO_CANCEL_REASON_ALL%')
AND wo_code.ct_code = wo_cancel_reason
AND wo.wo_rop_fk=rop.rop_pk)
LEFT OUTER JOIN hero.genesis_code void_code on((upper(void_code.ct_column) IN ('ROP_VOID_TYPE','ROP_VOID_TYPE_SUB_REQ'))
AND void_code.ct_code = rop_void_type_code AND wo.wo_rop_fk=rop.rop_pk)
LEFT OUTER JOIN aim.aim_physician_dim phy ON phy.physician_npi = ro.ro_npi
--#MP
left join (select customer_account_number, customer_key
			from  aim.aim_customer_dim	
			where  customer_site_use_code = 'SHIP_TO'  and customer_indirect_flag='N'
			group by 1,2)acd on acd.customer_account_number = cst.ra_customer_number	
left join aim.aim_customer_flag_dim cst_flags on cst_flags.customer_key = acd.customer_key
left join aim.gold_customer_roster cst_rstr on cst_rstr.customer_account_number  = cst.ra_customer_number
left join aim.gold_customer_assets cst_asst on cst_asst.serial_number  = rop.rop_ar_serial_number_fk
--#MP CASTP-137 4/26/2021 Update accounts where same account number has different facility name
left join AIM.AIM_CUSTOMER_DIM CGV2 on 
	  CGV2.customer_site_use_code = CASE WHEN aimcust.customer_shipto_market_segment IN (22) 
			  						THEN 'CAREGIVER'ELSE 'SHIP_TO' END
		      and CGV2.CUSTOMER_ACCOUNT_NUMBER = CASE WHEN aimcust.customer_shipto_market_segment IN (22) 
		      										  THEN COALESCE(CGV.caregiver_account_number,'-99999') 
		      										  ELSE CST.ra_customer_number END 
		      and CGV2.customer_indirect_flag='N' and customer_Status <> 'PENDING'
--#MP 4/26/2021 [CASTP-146] #21-Identify the order source for each VAC(R) rental order created.
LEFT OUTER JOIN hero.genesis_code sc on sc.ct_column = 'order_source' and sc.ct_code = rop.rop_order_source
--#MP 4/26/2021 [CASTP-167] #17-Calculate the age of Pick Up work orders.
LEFT join (select a.* from (select wo_rop_fk,wo_type_code,wo_status_code,wo_created_time_stamp,row_number() OVER (PARTITION BY wo_rop_fk ORDER BY MAX(wo_created_time_stamp) DESC) AS rn
from hero.genesis_work_order where wo_type_code = 'PCK' group by 1,2,3,4) a where rn = 1) wo2 on wo2.wo_rop_fk = rop.rop_pk 
left join (SELECT wo_rop_fk,A.CT_DESCRIPTION 
FROM (select a.WO_ROP_FK,B.CT_DESCRIPTION from (select wo_rop_fk,wo_type_code,wo_status_code,WO_TRANSPORT_METHOD_CODE
,wo_created_time_stamp,row_number() OVER (PARTITION BY wo_rop_fk ORDER BY MAX(wo_created_time_stamp) DESC) AS rn
from hero.genesis_work_order where  wo_rop_fk >0 AND wo_type_code = 'DEL' AND WO_STATUS_CODE = 'CLO'group by 1,2,3,4,5) A 
LEFT JOIN hero.genesis_code  B ON B.CT_COLUMN = 'wo_transport_method'
								 AND B.CT_CODE = A.WO_TRANSPORT_METHOD_CODE
where rn = 1) A) wo3 on rop.rop_pk = wo3.wo_rop_fk
WHERE ro.RO_CST_FK=cst.CST_PK
AND ro.RO_PK=rop.ROP_RO_FK
AND (rop.rop_stop_bill_time_stamp IS NULL OR round(datediff(now(), rop.rop_stop_bill_time_stamp)) <= 30)
AND rop.ROP_PRODUCT_USED_FK LIKE 'WND%'
AND (svc.svc_pk IS NOT NULL OR svc_pk<>'000')
AND (rop.rop_void_verify_time_stamp IS NULL OR round(datediff(now(), rop.rop_void_verify_time_stamp)) <= 30)
--# CTS Update : removed customer_shipto_market_segment restriction as we are adding it in hero_round_base_layer
--AND (aimcust.customer_shipto_market_segment NOT IN (51,52,53,99) OR aimcust.customer_shipto_market_segment IS NOT NULL)
AND aimcust.customer_shipto_market_segment IS NOT NULL
AND ((concat(ro.ro_patient_last_name, ", ", ro.ro_patient_first_name)) NOT LIKE '%UNIT %'
AND (concat(ro.ro_patient_last_name, ", ", ro.ro_patient_first_name)) NOT LIKE '%PLUS%'
AND (concat(ro.ro_patient_last_name, ", ", ro.ro_patient_first_name)) NOT LIKE '%CUST%')
and (aimcust.customer_shipto_key not in (select * from sand_cast.aim_order_Non_Home))
GROUP BY 1,	2,	3,	4,	5,	6,	7,	8,	9,	10,	11,	12,	13,	14,	15,	16,	17,	18,	19,	20,	21,	22,	23,	24,	25,	26,	27,	28,	29,	30,	31,	32,	33,	34,	35,	36,	37,	38,	39,	40,
41,	42,	43,	44,	45,	46,	47,	48,	49,	50,	51,	52,	53,	54,	55,	56,	57,	58,	59,	60,	61,	62,	63,	64,	65,	66,	67
,	68,	69,	70,	71,	72,	73,	74,	75,	76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95
,96,97,98,99;
compute stats sand_cast.hero_Round_initial_layer;
--#MP 4/26/2021 [CASTP-146] #21-Identify the order source for each VAC(R) rental order created.
--#MP 4/26/2021 [CASTP-148] #27-Update reporting_comops.hero_round_pending to include a 'Time to Release' metric.
--#MP 4/26/2021 [CASTP-167] #17-Calculate the age of Pick Up work orders.
--#MP 4/26/2021 [CASTP-171] #22-Identify delivery method for each VAC(R) Rental Order.
DROP TABLE IF EXISTS sand_cast.hero_round_base_layer;
CREATE TABLE sand_cast.hero_round_base_layer stored as parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/hero_round_base_layer/' AS
SELECT a.*,
-- CTS Update : replaced market_category with Market_segment logic update for inside_sales_managment
CASE 
	WHEN (market_segment IN ( 1, 2, 4 , 5, 6, 40, 89, 17, 20) and placement_type = 'Acute Orders' and jca_role = 'TSR') or 
	     (market_segment IN (14, 15, 16, 19, 33, 34, 50) and placement_type in ('Post Acute Transitions','Organic') and jca_role = 'ISR') 
	     THEN 'INSIDE SALES SUPERVISOR'
	     ELSE ''
END  inside_sales_managment	
FROM (SELECT  CASE 
WHEN roa_pm_fk='-' THEN 'VAC'
ELSE 'SNAP'
END 'Product_type',tmp.*,
-- CTS Update : replaced market_category with Market_segment logic update for Placement_Type
CASE
WHEN market_segment IN ( 1, 2, 4 , 5, 6, 40, 89, 17, 20) THEN 'Acute Orders'
WHEN Transitioned_Facility<>'-' AND market_segment IN (22, 14, 15, 16, 19, 33, 34, 50) THEN 'Post Acute Transitions'
WHEN Transitioned_Facility='-'  AND market_segment IN (22, 14, 15, 16, 19, 33, 34, 50) THEN 'Organic'
END  'Placement_Type',
--# CTS Update : market_category_type added
CASE
when jca_role in ('ATR','SSR','WHS','TMV','TSR') and market_segment in (1,2,4,5,6,40,89) THEN 'ACUTE'
when jca_role in ('ATR','AWT','TSV','TSR','ISR','WHS') and market_segment in (14,15,16,19,33,34,50) THEN 'SNF'
when jca_role in ('AWT','TSV','TMV','TSR','ISR','ATR') and market_segment in (17,20) THEN 'LTAC'
when jca_role in ('ATR','AWT','TSV','TMV','WHS') and market_segment in (22) THEN 'HOME'
ELSE market_category
END 'market_category_type'
FROM
(SELECT item_description,inventory_item_id,roa_pm_fk,ship_date,description,svc_pk,svc_description,
-----# CTS update :removed geography
--geography,
region,district,territory_code,inside_sales_flag,
ins_supervisor,	supervisor_name,territory_name,salesrep_name_align,regionvicepresident_name,districtmanager_name,jca_role,market_category,
---#CTS Update : Added market_segment
market_segment,ra_customer_number,
cst_pk,	cst_name,facility_caregiver,facility_number,facility_caregiver_address,facility_caregiver_city,facility_caregiver_zip,facility_caregiver_phone_number,
name_2,caregiver_type,physician_name,ro_npi,ro_doctor_phone,patient_name,patient_adress,patient_city,patient_zip,birth,ro_patient_location,ro_patient_phone,
prd_description,rop_ar_serial_number_fk,ro_number,rop_status_code,ro_purchase_order_number,rop_placed_time_stamp,rop_last_round_time_stamp,
rop_dc_time_stamp,ro_customer_group,customer_number,customer_name,rop_vac_transitionable_flag,rop_pk,csr,cst_ship_address_1,cst_ship_address_2,
transitioned_facility,transitioned_facility_address,transitioned_facility_city,transitioned_facility_zip,rop_stop_bill_time_stamp,rop_start_bill_time_stamp,
cast(rop_created_time_stamp as timestamp) rop_created_time_stamp ,rop_void_verify_time_stamp,order_status,requestor_name,requestor_address,requestor_city,requestor_zip,requestor_phone_number,
tracking_number,payer_name,ro_status_code,mmc_flag,ready_care_flag,customer_sva_enabled_flag,va_system,charity_care_status,cba,rebill,
wo_cancel_code_description,rop_void_code_description,
rop_avail_for_ship_date,
--# CTS update :removed nsl_name
---nsl_name,
Unit_type,substituted_product,rop_needed_time_stamp,rop_pickup_time_stamp,rop_delivered_time_stamp,gold_cust_flag,	facility_id,asset_owner_chain,participant_chain,	barcode
,rop_order_source, channel,Time_to_Release,Pickup_age,WO_TRANSPORT_DESCRIPTION
FROM sand_cast.hero_Round_initial_layer
--#MP Changed per Deb Meyers
--# CTS update : blocking condition for market_segement and jca_role
WHERE (jca_role in('ATR','TSR','TMV') AND market_segment in( 1, 2, 4 , 5, 6, 17, 20, 40, 89))
OR (jca_role in ('SSR','WHS') AND market_segment IN ( 1, 2, 4 , 5, 6, 40, 89))
OR (jca_role in ('ISR','AWT','TSV') AND market_segment IN (14, 15, 16, 17, 20, 19, 33, 34, 50))
OR (jca_role in ('ATR','TSR') AND market_segment IN (14, 15, 16, 19, 33, 34, 50))
--# CTS update :removed ATR and TMV for base orders table
OR (jca_role in ('AWT','TSV') AND market_segment IN (22))  --OR (jca_role in ('ATR','AWT','TSV','TMV') AND market_segment IN (22))     
--#MP Per Deb. M. remove snap from the dashboard	
       --UNION ALL SELECT *
--FROM sand_cast.hero_round_snap_base_layer
UNION ALL SELECT *
FROM sand_cast.hero_round_ATR_TRANS_initial_layer
) tmp WHERE market_category <> 'UNKNOWN'
AND market_category IS NOT NULL
AND market_category <> '-' )A
--#MP ACUTE , ORGANIC ISR,TSR should not flow through Per Deb. M.
where NOT (PLACEMENT_TYPE = 'Acute Orders' and JCA_ROLE = 'ISR')
          AND
-- # CTS Update : replaced market_category with market_segment logic
      NOT (market_segment IN (22, 14, 15, 16, 19, 33, 34, 50) and PLACEMENT_TYPE = 'Organic' AND JCA_ROLE in ('TSR','ATR'));
compute stats sand_cast.hero_round_base_layer;

-------------------------------------------------------------------------------------Ship pending reasons---------------------------------------
DROP TABLE IF EXISTS sand_cast.ship_pending_reasons_rdr_desc;
CREATE TABLE sand_cast.ship_pending_reasons_rdr_desc stored AS parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/ship_pending_reasons_rdr_desc/' AS
SELECT rdr_rop_fk,rdr_rpos,rdr_description AS rdr_description,rdr_created_date
FROM hero.genesis_rop_delay_reason rdr
WHERE to_date(rdr_created_date)=
(SELECT max(to_date(rdr_created_date))
FROM hero.genesis_rop_delay_reason a
WHERE a.rdr_rop_fk=rdr.rdr_rop_fk)
GROUP BY 1,2,3,4;
compute stats sand_cast.ship_pending_reasons_rdr_desc;
---------------------------------------------ship_pending_reasons_rdsr_desc--------------------------------------
DROP TABLE IF EXISTS sand_cast.ship_pending_reasons_rdsr_desc;
CREATE TABLE sand_cast.ship_pending_reasons_rdsr_desc stored AS parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/ship_pending_reasons_rdsr_desc/' AS
SELECT rdsr_rop_fk,rdsr_rpos,rdsr_description AS rdsr_description,rdsr_srpos,rdsr_created_date
FROM hero.genesis_rop_delay_sub_reason rdsr
WHERE to_date(rdsr_created_date)=
(SELECT max(to_date(rdsr_created_date))
FROM hero.genesis_rop_delay_sub_reason b
WHERE b.rdsr_rop_fk=rdsr.rdsr_rop_fk)
GROUP BY 1,2,3,4,5;
compute stats sand_cast.ship_pending_reasons_rdsr_desc;
---------------------------------------------ship_pending_reasons_rdro_desc--------------------------------------
DROP TABLE IF EXISTS sand_cast.ship_pending_reasons_rdro_desc;
CREATE TABLE sand_cast.ship_pending_reasons_rdro_desc stored AS parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/ship_pending_reasons_rdro_desc/' AS
SELECT rdro_rop_fk,rdro_rpos,rdro_description AS rdro_description,rdro_created_date
FROM hero.genesis_rop_delay_reason_option rdro
WHERE to_date(rdro_created_date)=
(SELECT max(to_date(rdro_created_date))
FROM hero.genesis_rop_delay_reason_option c
WHERE c.rdro_rop_fk=rdro.rdro_rop_fk)
GROUP BY 1,2,3,4;
compute stats sand_cast.ship_pending_reasons_rdro_desc;
---------------------------------------------ship_pending_reasons(Final reasons table joining all temporary reasons table)----------------------
DROP TABLE IF EXISTS sand_cast.ship_pending_reasons;
CREATE TABLE sand_cast.ship_pending_reasons stored AS parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/ship_pending_reasons/' AS
SELECT a.rdr_rop_fk,a.rdr_description,a.rdr_rpos,a.rdr_created_date,b.rdsr_description,b.rdsr_rpos
FROM sand_cast.ship_pending_reasons_rdr_desc a
LEFT JOIN sand_cast.ship_pending_reasons_rdsr_desc b ON a.rdr_rop_fk=b.rdsr_rop_fk
AND a.rdr_rpos=b.rdsr_rpos
GROUP BY 1,2,3,4,5,6;
compute stats sand_cast.ship_pending_reasons;

---------------------------------------------ship_pending_reasons end ---------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS sand_cast.hero_round_rpt_layer;
CREATE TABLE sand_cast.hero_round_rpt_layer stored AS parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/hero_round_rpt_layer/' AS
SELECT rop_pk,
CASE
WHEN rdr_description='null' THEN '-'
WHEN rdr_description IS NULL THEN '-'
ELSE rdr_description
END pending_reasons,
CASE
WHEN rdsr_description='null' THEN '-'
WHEN rdsr_description IS NULL THEN '-'
ELSE rdsr_description
END pending_sub_reasons
FROM sand_cast.hero_round_base_layer a
INNER JOIN sand_cast.ship_pending_reasons b ON a.rop_pk=b.rdr_rop_fk
--Add a group by because a pending can have multiple types  flowing through #MP
AND order_status in ('PENDING PLACEMENT','SHIP PENDING') group by 1,2,3;
compute stats sand_cast.hero_round_rpt_layer;
DROP TABLE IF EXISTS sand_cast.hero_round_final;
CREATE TABLE sand_cast.hero_round_final stored as parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/hero_round_final/' AS
SELECT tmp3.*,
CASE
WHEN order_status in ('PENDING PLACEMENT','SHIP PENDING') THEN pending_reasons
WHEN order_status='CANCELLED' THEN wo_cancel_code_description
ELSE '-'
END pending_or_cancel_reason,
CASE
WHEN order_status in ('PENDING PLACEMENT','SHIP PENDING') THEN pending_sub_reasons
WHEN order_status='CANCELLED' THEN rop_void_code_description
ELSE '-'
END pending_or_cancel_sub_reason
FROM
(SELECT tmp.*,
CASE
WHEN tmp2.pending_reasons='null' THEN '-'
WHEN tmp2.pending_reasons IS NULL THEN '-'
ELSE tmp2.pending_reasons
END pending_reasons,
CASE
WHEN tmp2.pending_sub_reasons='null' THEN '-'
WHEN tmp2.pending_sub_reasons IS NULL THEN '-'
ELSE tmp2.pending_sub_reasons
END pending_sub_reasons,
case
 WHEN order_status='SHIP PENDING' THEN (round((unix_timestamp(now()))-unix_timestamp(rop_created_time_stamp))/3600)
 WHEN order_status='PENDING PLACEMENT' THEN (round((unix_timestamp(now()))-unix_timestamp(rop_avail_for_ship_date))/3600)
 WHEN order_status='CLOSED' THEN nvl(round(datediff(rop_stop_bill_time_stamp, rop_start_bill_time_stamp))+1, 0)
 WHEN order_status='PLACED' THEN nvl(round(datediff(now(), rop_start_bill_time_stamp))+1, 0)
 WHEN order_status='CANCELLED' THEN 0
end aged 
FROM
(SELECT x.*,
y.ord_cnt
FROM
(SELECT *
FROM sand_cast.hero_round_base_layer
WHERE territory_code <> '-'
AND rop_created_time_stamp>=to_date('2018-07-01')
AND territory_code NOT LIKE 'DCS99%') x,
(SELECT facility_caregiver,
order_status,
territory_code,
count(DISTINCT ro_number) ord_cnt
FROM sand_cast.hero_round_base_layer
WHERE territory_code <> '-'
AND rop_created_time_stamp>=to_date('2018-07-01')
GROUP BY 1,2,3) y
WHERE x.facility_caregiver = y.facility_caregiver
AND x.order_status = y.order_status
AND x.territory_code=y.territory_code) tmp
LEFT OUTER JOIN sand_cast.hero_round_rpt_layer tmp2 ON tmp.rop_pk=tmp2.rop_pk) tmp3;
compute stats sand_cast.hero_round_final;
 
-------------------------------------------------------------------final order table-------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS sand_cast.hero_round_pending;
--#MP 1/15/2021 [CASTP-138] #13-Update sand_cast.hero_round_pending to include orders with a 'Ship Pending' status.
--#MP 1/15/2021 [CASTP-141] #15-Add additional column to sand_cast.hero_round_pending from hero.genesis_rental_order_product which identify an order's 'Needed By' date. 
--#MP 1/15/2021 [CASTP-142] #20-Add additional column to sand_cast.hero_round_pending from hero.genesis_rental_order_product which identify an order's delivered date timestamp.
--#MP 1/15/2021 [CASTP-170] #16-Add additional column to sand_cast.hero_round_pending from hero.genesis_rental_order_product which identify an order's Pick Up Date.
--#MP 1/15/2021 [CASTP-158] #10-Identify product substitution within sand_cast.hero_round_pending for each VAC(R) Rental Order.
--#MP 1/15/2021 [CASTP-162] #6-Update the sand_cast.hero_round_pending table with barcode of delivered asset. 
--#MP 1/15/2021 [CASTP-147] #14-Update sand_cast.hero_round_pending age calculation for orders with a 'Ship Pending' status. aged calc to take into consideration the newly created orders_status
--#MP 1/15/2021 [CASTP-236] #1.2-Add Gold Program Flag to hero_round_pending reporting table. - 
--#MP 1/15/2021 [CASTP-240] #2.2-Add fields from Gold Customer Roster table to hero_round_pending reporting table. - FACILITY ID, CHAIN NAME
--#MP 1/15/2021 [CASTP-239] #3.2-Add fields from Gold Customer Assets table to hero_round_pending reporting table - BARCODE as gold_asset_barcode
--#MP 4/26/2021 [CASTP-132] #12-Update reporting_comops.hero_round_pending to include essential contact information for the CSR that is assigned to the VAC� order.
--#MP 4/26/2021 [CASTP-137] Update accounts where same account number has different facility name
--#MP 4/26/2021 [CASTP-146] #21-Identify the order source for each VAC(R) rental order created.
--#MP 4/26/2021 [CASTP-148] reporting_comops.hero_round_pending to include a 'Time to Release' metric.
--#MP 4/26/2021 [CASTP-167] #17-Calculate the age of Pick Up work orders.
--#MP 4/26/2021 [CASTP-168] Which bucket do Pending Pick Ups for VAC(R) fall under?
--#MP 4/26/2021 [CASTP-171] #22-Identify delivery method for each VAC(R) Rental Order.
--#MP 4/26/2021 [CASTP-175] #25-Classify orders based on Age Bucket.
--#MP 5/10/2021 [CASTP-161] #8-Flag Gold Asset Delivery errors in reporting_comops.hero_round_pending
--#MP 5/10/2021 [CASTP-169] #18-Create a field called Pickup Status.
--#MP 6/15/2021 [CASTP-1264] Add Delivery Work order number
CREATE TABLE sand_cast.hero_round_pending stored as parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/hero_round_pending/' AS
SELECT A.product_type,
A.item_description,
A.inventory_item_id,
A.roa_pm_fk,
A.ship_date,
A.description,
A.svc_pk,
A.svc_description,
--# CTS update :removed geography
--A.geography,
A.region,
A.district,
A.territory_code,
A.territory_name,
B.salesrep_name AS salesrep_name_align,
B.rvp_name AS regionvicepresident_name,
B.dm_name AS districtmanager_name,
--#MP Mario Change here
case when a.jca_role = 'ATR' then t.inside_sales_flag else a.inside_sales_flag end inside_sales_flag,
a.ins_supervisor,a.supervisor_name,
a.jca_role,
A.market_category ,
---CTS Update : Added Market_segment
A.Market_Segment,
A.ra_customer_number,
A.cst_pk,
A.cst_name,
A.facility_caregiver,
A.facility_Number,
A.facility_caregiver_address,
A.facility_caregiver_city,
A.facility_caregiver_zip,
A.facility_caregiver_phone_number,
A.name_2,
A.caregiver_type,
A.physician_name,
A.ro_npi,
A.ro_doctor_phone,
A.patient_name,
A.patient_adress,
A.patient_city,
A.patient_zip,
A.birth,
A.ro_patient_location,
A.ro_patient_phone,
A.prd_description,
A.rop_ar_serial_number_fk,
A.ro_number,
A.rop_status_code,
A.ro_purchase_order_number,
A.rop_placed_time_stamp,
A.rop_last_round_time_stamp,
A.rop_dc_time_stamp,
A.ro_customer_group,
A.customer_number,
A.customer_name,
A.rop_vac_transitionable_flag,
A.rop_pk,
A.csr,
Oemp.first_name release_csr_first_name,Oemp.last_name release_csr_last_name --#MP 4/26/2021 [CASTP-132]
,ak3.`3m_email_address` release_csr_3m_email,Oemp.work_number release_csr_work_number,
A.cst_ship_address_1,
A.cst_ship_address_2,
A.transitioned_facility,
A.transitioned_facility_address,
A.transitioned_facility_city,
A.transitioned_facility_zip,
A.rop_stop_bill_time_stamp,
A.rop_start_bill_time_stamp,
A.rop_created_time_stamp,
A.rop_void_verify_time_stamp,
A.order_status,
A.requestor_name,
A.requestor_address,
A.requestor_city,
A.requestor_zip,
A.requestor_phone_number,
A.tracking_number,
A.payer_name,
A.ro_status_code,
A.mmc_flag,
A.ready_care_flag,
A.customer_sva_enabled_flag,
A.va_system,
A.charity_care_status,
A.cba,
A.rebill,
A.wo_cancel_code_description,
A.rop_void_code_description,
A.placement_type,
--# CTS Update : Added market_category_type
A.market_category_type,
A.aged,
A.ord_cnt,
A.pending_reasons, 
A.pending_sub_reasons,
A.pending_or_cancel_reason,
A.pending_or_cancel_sub_reason,
(adddate(now(),-1)) as data_refresh_date,
--# CTS update :removed nsl_name
--A.nsl_name,
A.inside_sales_managment,
A.gold_cust_flag,A.Facility_id,A.participant_chain,A.asset_owner_chain, 
case when participant_chain <> asset_owner_chain then 'Y' else 'N'end Delivery_error, --#MP 5/10/2021 [CASTP-161]
A.BarCode,A.Unit_type,
A.substituted_product,A.rop_needed_time_stamp,A.rop_pickup_time_stamp,A.rop_delivered_time_stamp,
A.Time_to_Release
,A.rop_order_source,channel,A.Pickup_age
, case when Pickup_age < 5 then '<5 days' ----#MP 4/26/2021 [CASTP-168]
	   when Pickup_age >= 5 then  '>=5 days' 
	   ELSE ''
  end Pickup_age_bucket
,A.WO_TRANSPORT_DESCRIPTION Delivery_method--#MP 4/26/2021 [CASTP-175]
,case when a.order_status in ('CLOSED','PLACED')
	then 
		case 	 --#MP 4/26/2021 [CASTP-175]
		    when A.aged >=120 then "120+"
			when A.aged >=90  then "90+"
			when A.aged >=60  then "60+"
			when A.aged >= 30 then "30+"
			when A.aged <30 then "<30"
							ELSE ''
		end
	else null
 END aged_bucket,
 case when Order_status = 'PLACED' AND ROP_DISCHARGED_TIMESTAMP IS NOT NULL
          AND ao.wo_pickup_timestamp is not null and ao.rop_stop_bill_timestamp is not NULL 
          and ao.rop_pickup_timestamp is null then 'PENDING' ELSE 'PICKED UP'
 END PICKUP_STATUS,WOC.WO_PK_DEL Delivery_WO
FROM  sand_cast.hero_round_final A
INNER JOIN
(SELECT * FROM sand_cast.aim_order_rvp_dm_rep_names WHERE territory_code is not null) B on	A.territory_code = B.territory_code 
														AND A.region = B.region
--#MP changed
left join hero.genesis_customer gc on a.cst_pk=gc.cst_pk
left join aim.aim_customer_dim c   on c.customer_site_use_id=gc.ra_shipto_site_id and customer_account_number not like '%Z%' 
									  and customer_key>0 and customer_indirect_flag='N'
LEFT JOIN (select customer_key, inside_sales_flag from aim.aim_customer_zipterr_xref_transposed_vw				  
					group by 1,2) t on t.customer_key = c.customer_key  
--#MP 4/26/2021 [CASTP-132] #12-Update reporting_comops.hero_round_pending to include essential contact information for the CSR that is assigned to the VAC� order.
left join aim.aim_kci_3m_emp_xref ak3 on ak3.legacy_employee_number = a.csr 
left join hero.genesis_oracle_emp Oemp on Oemp.employee_number = a.csr and Oemp.person_type = 'EMPLOYEE'
 						 and Oemp.termination_date is null 
--#MP 5/10/2021 [CASTP-169]
LEFT JOIN aim.aim_orders_dim AO  ON AO.rop_number = A.ROP_PK AND AO.ROP_TYPE = 'VAC'
LEFT JOIN (select wo_ar_serial_number_fk, MAX(WO_PK) WO_PK_DEL from hero.genesis_work_order_vw 
where wo_type_CODE = 'DEL' GROUP BY 1 ) WOC ON WOC.wo_ar_serial_number_fk = A.rop_ar_serial_number_fk
WHERE 
 ((A.jca_role = 'TMV' AND A.Market_Segment IN ( 1, 2, 4 , 5, 6, 40, 89) AND A.placement_type = 'Acute Orders')
OR (A.jca_role = 'ATR' AND A.Market_Segment IN ( 1, 2, 4 , 5, 6, 40, 89, 17, 20) AND A.placement_type = 'Acute Orders')
OR (A.jca_role = 'ATR' AND A.Market_Segment IN (14, 15, 16, 19, 33, 34, 50) AND A.placement_type <> 'Acute Orders')
--# CTS update :removed inside_sales_flag
OR (A.jca_role = 'ATR' AND A.Market_Segment IN (22) AND A.placement_type <> 'Acute Orders')-- AND A.inside_sales_flag = 'N')
OR (A.jca_role = 'TSV' AND A.Market_Segment IN (22, 14, 15, 16, 19, 33, 34, 50))
--#MP Mario Change here
OR (A.jca_role ='ISR' AND A.Market_Segment IN (17, 20, 14, 15, 16, 19, 33, 34, 50) )
OR (A.jca_role = 'TSR' AND A.Market_Segment IN (1, 2, 4 , 5, 6, 40, 89, 17, 20, 14, 15, 16, 19, 33, 34, 50) )
--# CTS update : Alignment
OR ((A.jca_role ='SSR' AND A.placement_type = 'Acute Orders'))
OR (A.jca_role in ('WHS','AWT')))
--#CTS--Added Group by
GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 
36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 
71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90,91,92,93,94,95,96,97,98,99,100,101,102,103,
104,105,106,107,108,109,110,111,112,113,114,115,116,117,118;
compute stats sand_cast.hero_round_pending;

---------------------------OMP removing extra RO NUMBERS not present in round query start --------------------------------------------------------------------------------------

DROP TABLE IF EXISTS sand_cast.omp_data_base_layer;
CREATE TABLE sand_cast.omp_data_base_layer stored as parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/omp_data_base_layer/' AS
SELECT A.*
FROM sand_cast.omp_data A
INNER JOIN
(SELECT DISTINCT ro_number
FROM sand_cast.hero_round_pending) B ON A.ro_number = B.ro_number;
compute stats sand_cast.omp_data_base_layer;

--#MP 1/15/2021 [CASTP-187] #26-Add additional columns to sand_cast.omp_data from hero.genesis_vac_wound_status_tracking which represent wound length, width, and depth.We need to delineate wound dimension by Length, Width, and Depth into separate columns
--#MP 1/15/2021 [CASTP-237] #1.3-Add Gold Program Flag to omp_data reporting table.
DROP TABLE IF EXISTS sand_cast.omp_data;
CREATE TABLE sand_cast.omp_data stored as parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/omp_data/' AS
SELECT 
--# CTS update :removed geography
--geography,
region,district,territory_code,territory_name,inside_sales_flag,ins_supervisor,supervisor_name,
jca_role,salesrep_name_align,regionvicepresident_name,
districtmanager_name,caregiver_name,caregiver_account_number,caregiver_type,ro_number,patient_name,
birth,physician_name,physician_phone,ro_npi,transitioned_facility,payor_name,wound_type_description,wound_woundbed_appearance,
wound_boneexposed,wound_debri_type,patient_admit_date,rop_stop_bill_time_stamp,rop_created_time_stamp,rop_start_bill_time_stamp,
wound_status,wound_number,wound_evaluation_date,wound_location_orientation_direction,wound_exudate_code_description
,wound_length,wound_width,wound_depth,
wound_size,snap_size_criteria_met,customer_sva_enabled_flag,snap_tier,va_system,order_status,therapy_days,
--# CTS update :removed nsl_name
--nsl_name,
gold_cust_flag
FROM sand_cast.omp_data_base_layer 
--#CTS --Added Group by
GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 
36, 37, 38, 39, 40, 41, 42,43,44,45,46;
compute stats sand_cast.omp_data;

---------------------------OMP removing extra RO NUMBERS not present in round query end ---------------------------------------------------------------------------------------

-----------------------------------------------PROFILE QUERY Start -----------------------------------------------------------
drop table IF EXISTS sand_cast.zip_terr_1008_v2 ;
create table sand_cast.zip_terr_1008_v2 stored as parquet 
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/zip_terr_1008_v2/' AS
SELECT (CASE WHEN sales_credit_type_code = 'AE' THEN regexp_replace (territory_code,'WCR','AE') 
WHEN sales_credit_type_code IN ('SAE','SAE9') THEN regexp_replace (territory_code,'AES','SAE') 
ELSE territory_code END) region,
concat('K',salesrep_number) userid,salesrep_name rvp_name,
--(CASE WHEN name LIKE 'DUAL%' AND sales_credit_type_code IN ('TMV','TSV') THEN 'TDV' ELSE sales_credit_type_code END)
sales_credit_type_code jca_role
FROM aim.aim_territory_salesrep_xref_vw
WHERE territory_code IS NOT NULL AND sales_credit_type_code NOT IN ('BDM','BDM9','VCS','TSA')
GROUP BY 1,2,3,4
union all
select district region,concat('K',employee_number) userid, dm_name rvp_name,'DMS' jca_role
from aim.aim_district_dm_xref_vw where District like ('R%')
group by 1,2,3,4
union all
select translate(region,'R','A') region,concat('K',employee_number) userid, rvp_name rvp_name,'RVP' jca_role
from aim.aim_region_rvp_xref_vw  where translate(region,'R','A') like ('A%') and translate(region,'R','A')<>'A0' and start_date>='2020-01-01'
group by 1,2,3,4;
compute stats sand_cast.zip_terr_1008_v2;

drop table IF EXISTS sand_cast.zip_terr_1008_v3 ;
create table sand_cast.zip_terr_1008_v3 stored as parquet 
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/zip_terr_1008_v3/' AS
select a.*,p.email_address,p.uid_3m from 
sand_cast.zip_terr_1008_v2 a
left join
(select attribute21 as uid_3m,employee_number,email_address, max(effective_end_date) from ebs_usawt.apps_per_all_people_f
where trunc(now(),'dd') between trunc(effective_start_date,'dd') and trunc(effective_end_date,'dd') 
and employee_number <> 'null'
group by 1,2,3
) p
on a.userid = concat('K',p.employee_number);
compute stats sand_cast.zip_terr_1008_v3;

drop table IF EXISTS sand_cast.orders_userprofile_table_2019 ;
create table sand_cast.orders_userprofile_table_2019 stored as parquet 
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/orders_userprofile_table_2019/' AS
Select jca_role,
  region RDT,
  rvp_name name,
  case  when userid = 'K19068' then 'beesond'
        when userid = 'K20174' then 'dragoa'
        when userid = 'K20054' then 'johnsore'
        when userid = 'K20016' then 'dalyk'
        when userid = 'K19363' then 'deschenp'
        when userid = 'K13586' then 'dillonc'
        when userid = 'K17267' then 'fruges'
        when userid = 'K20055' then 'Jacobsod'
        when userid = 'K13723' then 'lallyr'
        when userid = 'K16740' then 'lemairet'
        when userid = 'K20042' then 'Oconnorm'
        when userid = 'K14726' then 'parhams'
        when userid = 'K14680' then 'sandersr'
        when userid = 'K10513' then 'schutzep'
        when userid = 'K20035' then 'Sholb'
        when userid = 'K14785' then 'Sommerfd'
        when userid = 'K20196' then 'KS458'
        when userid = 'K17806' then 'stollm'
        when userid = 'K16699' then 'brownk'
        when userid = 'K14557' then 'budnikb'
        when userid = 'KD305' then 'donohoem'
        when userid = 'K13992' then 'Dunawayd'
        when userid = 'KJ100' then 'Jacksonr'
        when userid = 'KK098' then 'kilbanep'
        when userid = 'KM171' then 'myersr'
        when userid = 'K15394' then 'greved'
        when userid = 'K18797' then 'adamsg'
        when userid = 'K15715' then 'arvinp'
        when userid = 'K18410' then 'boudreap'
        when userid = 'K14669' then 'boykinj'
        when userid = 'K20058' then 'KC004'
        when userid = 'K17541' then 'weberb'
        when userid = 'K19371' then 'dominguc'
        when userid = 'K20046' then 'KW501'
        when userid = 'K17138' then 'falconev'
        when userid = 'K10335' then 'KimmelG'
        when userid = 'K18644' then 'ladnerp'
        when userid = 'K18000' then 'lightr'
        when userid = 'K18491' then 'loffmanj'
        when userid = 'K16515' then 'manningk'
        when userid = 'K16964' then 'maragose'
        when userid = 'K12833' then 'morrissc'
        when userid = 'K18084' then 'overstrm'
        when userid = 'K18384' then 'rodmaris'
        when userid = 'K15584' then 'steubem'
        when userid = 'K20050' then 'Wadesonj'
        when userid = 'K15167' then 'wilsonc'
        when userid = 'K72446' then 'hugerm'
        Else userid
  end userid,
  uid_3m, 
email_address
from 
sand_cast.zip_terr_1008_v3 ;
compute stats sand_cast.orders_userprofile_table_2019;

drop table if exists sand_cast.userprofile_round_table_2019;
create table sand_cast.userprofile_round_table_2019 stored as parquet 
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/userprofile_round_table_2019/' AS
SELECT r.*,(CASE WHEN r.territory_code=bdm.territory_code AND r.district=bdm.district THEN bdm.bdm_user_id ELSE CAST(NULL AS STRING) END) bdm_user_id,
concat(bdm.bdm_user_id,d_userid,t_userid) bdt_userid FROM
(
select y.jca_role,y.rdt as territory_code,y.name as salesrep_name, y.userid as t_userid,y.uid_3m as t_uid_3m,y.email_address as t_email_address,
x.rdt as district,x.name as dm_name, x.userid as d_userid,x.uid_3m as d_uid_3m,x.email_address as d_email_address,
z.rdt as region,z.name as rvp_name, z.userid as r_userid,z.uid_3m as r_uid_3m,z.email_address as r_email_address,
concat(x.userid,y.userid) dt_userid, concat(x.uid_3m,y.uid_3m) dt_3m_userid
from 
(select a.*,b.territory_code, 
b.region, 
b.district
from sand_cast.orders_userprofile_table_2019 a
left join 
(select sales_credit_type_code,territory_code,region,district 
from aim.aim_customer_zipterr_xref_transposed_vw where region like 'A%' and region <>'A0'
group by sales_credit_type_code,territory_code,region,district) b on
b.territory_code = a.rdt) y
left join
sand_cast.orders_userprofile_table_2019 x
on x.rdt=y.district
left join 
sand_cast.orders_userprofile_table_2019 z
on z.rdt=y.region
where district is not null
) r
LEFT JOIN
(
SELECT district,bdm_territory_code,territory_code,b.userid bdm_user_id FROM reporting_comops.smrplus_bdm_names_daily a
LEFT JOIN 

(SELECT (CASE WHEN sales_credit_type_code = 'AE' THEN regexp_replace (territory_code,'WCR','AE') 
             WHEN sales_credit_type_code IN ('SAE','SAE9') THEN regexp_replace (territory_code,'AES','SAE') 
        ELSE territory_code END) region,
       concat('K',(CASE WHEN salesrep_number='119987' THEN '01803838'
            WHEN salesrep_number='112121' THEN '01805197'
            ELSE salesrep_number END)) userid,
       salesrep_name rvp_name,
       (CASE WHEN sales_credit_type_code = 'BDM9' THEN 'BDM' ELSE sales_credit_type_code END) jca_role
FROM aim.aim_territory_salesrep_xref_vw
WHERE territory_code IS NOT NULL AND sales_credit_type_code IN ('BDM','BDM9')
GROUP BY 1,2,3,4) b
ON a.bdm_territory_code=b.region
GROUP BY 1,2,3,4
) bdm
ON r.territory_code=bdm.territory_code AND r.district=bdm.district;
compute stats sand_cast.userprofile_round_table_2019;
-----------------------------------------------PROFILE QUERY end -------------------------------------------------------------------------------------------------------------