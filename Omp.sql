
DROP TABLE IF EXISTS reporting_comops.omp_data_initial_layer;


CREATE TABLE reporting_comops.omp_data_initial_layer stored as parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/reporting_comops.db/omp_data_initial_layer/' AS
SELECT WNDST.*,
     CASE
         WHEN WNDST.order_status='PENDING' THEN (round((unix_timestamp(trunc(now(), 'MI'))-unix_timestamp(rop_created_time_stamp))/3600))
         WHEN WNDST.order_status='CLOSED' THEN nvl(round(datediff(WNDST.rop_stop_bill_time_stamp, WNDST.rop_start_bill_time_stamp))+1, 0)
         WHEN WNDST.order_status='PLACED' THEN nvl(round(datediff(now(), WNDST.rop_start_bill_time_stamp))+1, 0)
         WHEN WNDST.order_status='CANCELLED' THEN 0
     END therapy_days,
     CASE
    WHEN WNDST.geography='EAST' THEN 'Patton,Jennifer'
    WHEN WNDST.geography='WEST' THEN  'Moller,Kim'
    ELSE  'Bauer,Chris'
    END nsl_name
FROM
(SELECT WOUND.geography geography,
        WOUND.region region,
        WOUND.district district,
        WOUND.territory_code,
        wound.territory_name,
        Wound.jca_role jca_role,
        wound.salesrep_name salesrep_name_align,
        wound.regionvicepresident_name,
        wound.districtmanager_name,
        CASE
            WHEN WOUND.caregiver_name='null' THEN '-'
            WHEN WOUND.caregiver_name IS NULL THEN '-'
            ELSE WOUND.caregiver_name
        END caregiver_name,
        CASE
            WHEN Wound.caregiver_account_number='null' THEN '-'
            WHEN Wound.caregiver_account_number IS NULL THEN '-'
            ELSE Wound.caregiver_account_number
        END caregiver_account_number,
        CASE
            WHEN Wound.caregiver_type='null' THEN '-'
            WHEN Wound.caregiver_type IS NULL THEN '-'
            ELSE Wound.caregiver_type
        END caregiver_type,
        nvl(WOUND.ro_number, 0) ro_number,
        CASE
            WHEN WOUND.patient_name='null' THEN '-'
            WHEN WOUND.patient_name IS NULL THEN '-'
            ELSE WOUND.patient_name
        END patient_name,
        CASE
            WHEN (from_unixtime(unix_timestamp(TO_DATE(birth), 'yyyy-MM-dd'), 'MM-dd-yyyy'))='null' THEN '-'
            WHEN (from_unixtime(unix_timestamp(TO_DATE(birth), 'yyyy-MM-dd'), 'MM-dd-yyyy')) IS NULL THEN '-'
            ELSE from_unixtime(unix_timestamp(TO_DATE(birth), 'yyyy-MM-dd'), 'MM-dd-yyyy')
        END birth,
        CASE
            WHEN WOUND.physician_name='null' THEN '-'
            WHEN WOUND.physician_name IS NULL THEN '-'
            WHEN WOUND.physician_name=', ' THEN '-'
            ELSE WOUND.physician_name
        END physician_name,
        CASE
            WHEN WOUND.physician_phone='null' THEN '-'
            WHEN WOUND.physician_phone IS NULL THEN '-'
            WHEN WOUND.physician_phone ='nul-l-' THEN '-'
            ELSE WOUND.physician_phone
        END physician_phone,
        nvl(cast(wound.ro_npi AS string), '-') ro_npi,
        CASE
            WHEN wound.Transitioned_Facility='null' THEN '-'
            WHEN wound.Transitioned_Facility IS NULL THEN '-'
            ELSE wound.Transitioned_Facility
        END Transitioned_Facility,
        CASE
            WHEN WOUND.payor_name='null' THEN '-'
            WHEN WOUND.payor_name IS NULL THEN '-'
            ELSE WOUND.payor_name
        END payor_name,
        CASE
            WHEN WOUND.wound_type_description='null' THEN '-'
            WHEN WOUND.wound_type_description IS NULL THEN '-'
            ELSE WOUND.wound_type_description
        END wound_type_description,
        CASE
            WHEN WOUND.wound_woundbed_appearance='null' THEN '-'
            WHEN WOUND.wound_woundbed_appearance IS NULL THEN '-'
            ELSE WOUND.wound_woundbed_appearance
        END wound_woundbed_appearance,
        CASE
            WHEN WOUND.wound_boneexposed='null' THEN '-'
            WHEN WOUND.wound_boneexposed IS NULL THEN '-'
            ELSE WOUND.wound_boneexposed
        END wound_boneexposed,
        CASE
            WHEN WOUND.wound_debri_type='null' THEN '-'
            WHEN WOUND.wound_debri_type IS NULL THEN '-'
            ELSE WOUND.wound_debri_type
        END wound_debri_type,
        nvl(cast(WOUND.patient_admit_date AS string), '-') patient_admit_date,
        nvl(cast(WOUND.rop_stop_bill_time_stamp AS string), '-') rop_stop_bill_time_stamp,
        nvl(cast(wound.rop_created_time_stamp AS string), '-') rop_created_time_stamp,
        nvl(cast(wound.rop_start_bill_time_stamp AS string), '-') rop_start_bill_time_stamp,
        CASE
            WHEN WOUND.wound_status='null' THEN '-'
            WHEN WOUND.wound_status IS NULL THEN '-'
            ELSE WOUND.wound_status
        END wound_status,
        nvl(cast(WOUND.wound_number AS string), '-') wound_number,
        nvl(cast(WOUND.wound_evaluation_date AS string), '-') wound_evaluation_date,
        CASE
            WHEN WOUND.wound_location_orientation_direction='null' THEN '-'
            WHEN WOUND.wound_location_orientation_direction IS NULL THEN '-'
            ELSE WOUND.wound_location_orientation_direction
        END wound_location_orientation_direction,
        CASE
            WHEN WOUND.wound_exudate_code_description='null' THEN '-'
            WHEN WOUND.wound_exudate_code_description IS NULL THEN '-'
            ELSE WOUND.wound_exudate_code_description
        END wound_exudate_code_description,
        nvl(cast(WOUND.wound_dem AS string), '-') wound_dem_L_W_D,
        nvl(cast(round(WOUND.wound_size, 2) AS string), '-') wound_size,
        WOUND.SNAP_Size_Criteria_Met,
        CASE
            WHEN WOUND.customer_sva_enabled_flag='null' THEN '-'
            WHEN WOUND.customer_sva_enabled_flag IS NULL THEN '-'
            ELSE WOUND.customer_sva_enabled_flag
        END customer_sva_enabled_flag,
        WOUND.SNAP_Tier,
        WOUND.VA_System,
        CASE
            WHEN (WOUND.rop_avail_for_ship_date IS NULL
                  AND WOUND.rop_void_verify_time_stamp IS NULL)
                 OR (WOUND.rop_placed_time_stamp IS NULL
                     AND WOUND.rop_void_verify_time_stamp IS NULL) THEN 'PENDING'
            WHEN WOUND.rop_placed_time_stamp IS NOT NULL
                 AND WOUND.rop_void_verify_time_stamp IS NULL
                 AND WOUND.rop_stop_bill_time_stamp IS NULL THEN 'PLACED'
            WHEN WOUND.rop_void_verify_time_stamp IS NOT NULL THEN 'CANCELLED'
            WHEN WOUND.rop_placed_time_stamp IS NOT NULL
                 AND WOUND.rop_void_verify_time_stamp IS NULL
                 AND WOUND.rop_stop_bill_time_stamp IS NOT NULL THEN 'CLOSED'
        END order_status
 FROM
   (SELECT tmpf.patient_name,
           tmpf.patient_admit_date,
           tmpf.patient_ssn,
           tmpf.birth,
           tmpf.rop_stop_bill_time_stamp,
           tmpf.rop_start_bill_time_stamp,
           tmpf.rop_created_time_stamp,
           tmpf.rop_avail_for_ship_date,
           tmpf.rop_void_verify_time_stamp,
           tmpf.rop_placed_time_stamp,
           tmpf.caregiver_name,
           tmpf.caregiver_account_number,
           tmpf.caregiver_type,
           tmpf.territory_code,
           tmpf.territory_name,
           tmpf.jca_role,
           tmpf.salesrep_name,
           tmpf.geography,
           tmpf.region,
           tmpf.regionvicepresident_name,
           tmpf.district,
           tmpf.districtmanager_name,
           tmpf.payor_name,
           tmpf.physician_name,
           tmpf.physician_phone,
           tmpf.ro_npi,
           tmpf.Transitioned_Facility,
           tmpf.ro_number,
           tmpf.wound_number,
           tmpf.wound_type_description,
           tmpf.wound_location_orientation_direction,
           concat(cast(tmpf.vws_wound_length AS string), ' x ', cast(tmpf.vws_wound_width AS string), ' x ', cast(tmpf.vws_wound_depth AS string)) AS 'wound_dem',
           TO_DATE(tmpf.wound_evaluation_date) AS wound_evaluation_date,
           tmpf.wound_status,
           tmpf.wound_exudate_code_description,
           tmpf.wound_woundbed_appearance,
           tmpf.vws_boneexposed AS wound_boneexposed,
           tmpf.wound_debri_type,
           tmpf.SNAP_Size_Criteria_Met,
           tmpf.customer_sva_enabled_flag,
           tmpf.SNAP_Tier,
           tmpf.VA_System,
           SUM(tmpf.vws_wound_length * tmpf.vws_wound_width * tmpf.vws_wound_depth) AS wound_size,
           row_number() OVER (PARTITION BY tmpf.ro_number,
                                           tmpf.Wound_number
                              ORDER BY MAX(tmpf.wound_evaluation_date) DESC) AS rn
    FROM
      (SELECT DISTINCT pta.name AS patient_name,
                       pta.admdate AS patient_admit_date,
                       pta.ssno patient_ssn,
                       pta.birth,
                       cgv.caregiver_name_1 AS caregiver_name,
                       cgv.caregiver_account_number,
                       cgv.caregiver_bus_type AS caregiver_type,
                       s.territory_code,
                       cast(null as string) territory_name,
                       s.sales_credit_type_code jca_role,
                       s.salesrep_name,
                       s.geography geography,
                       s.region region,
                       s.rvp_name regionvicepresident_name,
                       s.district,
                       s.dm_name districtmanager_name,
                       pd.payor_name,
                       CASE
                           WHEN concat(phy.physician_last_name, ', ', phy.physician_first_name) IS NULL THEN '-'
                           WHEN concat(phy.physician_last_name, ', ', phy.physician_first_name) ='null, null' THEN '-'
                           WHEN concat(phy.physician_last_name, ', ', phy.physician_first_name)=', ' THEN '-'
                           ELSE concat(phy.physician_last_name, ', ', phy.physician_first_name)
                       END physician_name,
                       ro.ro_npi,
                       ro.ro_cancel_time_stamp,
                       concat(cast(ro.ro_linked_ship_to_cst_fk AS string), ' ', '-', ' ', ro.ro_linked_facility_name) Transitioned_Facility,
                       CASE
                           WHEN phy.physician_bus_mail_phone = 'null' THEN '-'
                           WHEN phy.physician_bus_mail_phone IS NULL THEN '-'
                           WHEN phy.physician_bus_mail_phone ='nul-l-' THEN '-'
                           ELSE phy.physician_bus_mail_phone
                       END physician_phone,
                       ord.rop_avail_for_ship_date,
                       ord.rop_placed_time_stamp,
                       ord.rop_void_verify_time_stamp,
                       ord.rop_stop_bill_time_stamp,
                       ord.rop_start_bill_time_stamp,
                       ord.rop_created_time_stamp,
                       cs.customer_sva_enabled_flag,
                       CASE
                           WHEN ((SNAP_Size_Criteria_Met = 'Y')
                                 AND (payor_name LIKE '%MARATHON%'
                                      OR payor_name LIKE '%VA MEDICAL%'
                                      OR payor_name LIKE '%V A MEDICAL%'
                                      OR payor_name LIKE '%TRICAR%'
                                      OR payor_name LIKE '%MEDICARE DMAC%')
                                 AND cgv.caregiver_bus_type IN ('HA',
                                                                'HAC')) THEN 'Tier 1'
                           WHEN SNAP_Size_Criteria_Met = 'N' THEN 'NA'
                           ELSE 'Tier 2'
                       END SNAP_Tier,
                       CASE
                           WHEN ((cs.customer_sva_enabled_flag = 'Y')
                                 OR (payor_name LIKE '%MARATHON%'
                                     OR payor_name LIKE '%VA MEDICAL%'
                                     OR payor_name LIKE '%V A MEDICAL%'
                                     OR payor_name LIKE '%TRICAR%')) THEN 'Y'
                           ELSE 'N'
                       END VA_System,
                       wnd.*,
                       sw.territory_code AS wcr_territory_code
       FROM delair.patient_ptmaster pta
       JOIN delair.patient_ptorder pto ON pto.acctno = pta.acctno
       AND pto.admitno = pta.admitno
       JOIN
         (SELECT rop_ro_fk,
                 rop_avail_for_ship_date,
                 rop_placed_time_stamp,
                 rop_void_verify_time_stamp,
                 rop_stop_bill_time_stamp,
                 rop_created_time_stamp,
                 rop_start_bill_time_stamp
          FROM hero.genesis_rental_order_product
          WHERE ROP_PRODUCT_USED_FK LIKE 'WND%'
          GROUP BY 1,2,3,4,5,6,7) ord 
          ON ord.rop_ro_fk=pto.ro_fk
       AND coalesce(ord.rop_stop_bill_time_stamp, to_date(now())) >= date_sub(to_date(now()), 30)
       JOIN delair.patient_ptaddr ptaddr ON ptaddr.acctno = pta.acctno
       AND ptaddr.addrno = 1
       JOIN hero.genesis_ro_claim_data roc ON roc.roc_ro_fk = pto.ro_fk
       JOIN aim.aim_caregiver_dim cgv ON cgv.caregiver_mstrid = roc.roc_hha_smg_mstr_id_fk
       JOIN hero.genesis_rental_order ro ON ro.ro_pk = pto.ro_fk
       JOIN aim.aim_customer_dim cs ON cs.customer_site_use_id = ro.ro_ship_to_site_use_id
       AND cs.customer_site_use_code = 'SHIP_TO'
       AND cs.customer_indirect_flag = 'N'
       JOIN  (select * FROM aim.aim_customer_zipterr_xref_transposed_vw where region LIKE 'A%' AND region <>'A0') s 
        ON s.customer_key = cs.customer_key
        AND s.primary_flag='Y'
       JOIN aim.aim_customer_dim cb ON cb.customer_site_use_id = ro.ro_bill_to_site_use_id
       AND cb.customer_site_use_code = 'BILL_TO'
       JOIN aim.aim_payor_dim pd ON pd.payor_insno = cast(cb.customer_account_number AS bigint)
       AND cb.customer_account_site_id = pd.payor_inssub
       JOIN aim.aim_physician_dim phy ON phy.physician_npi = ro.ro_npi
       JOIN
         (SELECT tmp.*
          FROM
            (SELECT vwi.vwi_ro_fk AS ro_number,
                    vwi.vwi_wound_number AS wound_number,
                    gc3.ct_description AS wound_age,
                    vwt.vwt_wound_description AS wound_type_description,
                    concat(coalesce(gvwc3.vwc_description, ' '), '/', coalesce(gvwc4.vwc_description, ' '), '/', coalesce(gvwc5.vwc_description, ' ')) AS wound_location_orientation_direction,
                    vws.vws_wound_length,
                    vws.vws_wound_width,
                    vws.vws_wound_depth,
                    (CASE
                         WHEN vwi.vwi_ro_fk = 22525167 THEN to_date('2017-09-19')
                         ELSE vws.vws_evaluation_date
                     END) AS wound_evaluation_date,
                    gvwc7.vwc_description AS wound_status,
                    gvwc2.vwc_description AS wound_exudate_appearance,
                    gvwc1.vwc_description AS wound_exudate_code_description,
                    gvwc6.vwc_description AS wound_woundbed_appearance,
                    vws.vws_boneexposed,
                    vws.vws_wound_debri_date AS wound_debri_date,
                    gc4.ct_description AS wound_debri_type,
                    CASE
                        WHEN (vws.vws_wound_length <=13
                              AND vws.vws_wound_width <=13
                              AND vws.vws_wound_depth <= 1) THEN 'Y'
                        ELSE 'N'
                    END SNAP_Size_Criteria_Met,
                    row_number () OVER (PARTITION BY vwi_seq_pk,
                                                     vws_seq_pk
                                        ORDER BY gvwc7.vwc_description DESC) AS rownum
             FROM hero.genesis_vac_wound_information vwi
             JOIN hero.genesis_vac_wound_status_tracking vws ON vwi.vwi_seq_pk = vws.vws_vwi_seq_fk
             JOIN hero.genesis_vac_wound_types vwt ON vwi.vwi_wound_type_code = vwt.vwt_wound_type_code
             LEFT JOIN hero.genesis_vac_wound_codes gvwc1 ON vws.vws_wound_exudate_code=gvwc1.vwc_code
             LEFT JOIN hero.genesis_vac_wound_codes gvwc2 ON vws.vws_exudate_app=gvwc2.vwc_code
             LEFT JOIN hero.genesis_vac_wound_codes gvwc3 ON vwi.vwi_vwc_location = gvwc3.vwc_code
             AND gvwc3.vwc_type = 'L'
             LEFT JOIN hero.genesis_vac_wound_codes gvwc4 ON vwi.vwi_vwc_orientation = gvwc4.vwc_code
             AND gvwc4.vwc_type = 'O'
             LEFT JOIN hero.genesis_vac_wound_codes gvwc5 ON vwi.vwi_vwc_direction = gvwc5.vwc_code
             AND gvwc5.vwc_type = 'D'
             LEFT JOIN hero.genesis_code gc3 ON vwi.vwi_wound_age=gc3.ct_code
             AND gc3.ct_column='vwi_wound_age'
             LEFT JOIN hero.genesis_vac_wound_codes gvwc6 ON vws.vws_woundbed_app = gvwc6.vwc_code
             LEFT JOIN hero.genesis_vac_wound_codes gvwc7 ON vws.vws_wound_status = gvwc7.vwc_code
             LEFT JOIN hero.genesis_code gc4 ON vws.vws_wound_debri_type = gc4.ct_code
             AND gc4.ct_column = 'vws_wound_debri_type') tmp
          WHERE tmp.rownum = 1 ) wnd ON wnd.ro_number = pto.ro_fk
       LEFT JOIN (SELECT * FROM aim.aim_customer_zipterr_xref_transposed_vw WHERE region LIKE 'A%' AND region <>'A0') sw ON sw.customer_key = cs.customer_key---------------------@wbTag note
       AND cs.customer_market_category = sw.customer_shipto_market_category
       AND sw.sales_credit_type_code = 'WCR'
       AND sw.salesrep_name <> 'KINETIC CONCEPTS') tmpf
    WHERE tmpf.rop_void_verify_time_stamp IS NULL
    GROUP BY 1,	2,	3,	4,	5,	6,	7,	8,	9,	10,	11,	12,	13,	14,	15,	16,	17,	18,	19,	20,	21,	22,	23,	24,	25,	26,	27,	28,	29,	30,	31,	32,	33,	34,	35,	36,	37,	38,	39,	40,	41,42) wound
 WHERE rn <= 3
   AND rop_stop_bill_time_stamp IS NULL) WNDST ;

compute stats reporting_comops.omp_data_initial_layer;


DROP TABLE IF EXISTS reporting_comops.omp_data;


CREATE TABLE reporting_comops.omp_data stored as parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/reporting_comops.db/omp_data/' AS
SELECT A.geography,
     A.region,
     A.district,
     A.territory_code,
     A.territory_name,
     A.jca_role,
     B.salesrep_name AS salesrep_name_align,
     B.rvp_name AS regionvicepresident_name,
     B.dm_name AS districtmanager_name,
     A.caregiver_name,
     A.caregiver_account_number,
     A.caregiver_type,
     A.ro_number,
     A.patient_name,
     A.birth,
     A.physician_name,
     A.physician_phone,
     A.ro_npi,
     A.transitioned_facility,
     A.payor_name,
     A.wound_type_description,
     A.wound_woundbed_appearance,
     A.wound_boneexposed,
     A.wound_debri_type,
     A.patient_admit_date,
     A.rop_stop_bill_time_stamp,
     A.rop_created_time_stamp,
     A.rop_start_bill_time_stamp,
     A.wound_status,
     A.wound_number,
     A.wound_evaluation_date,
     A.wound_location_orientation_direction,
     A.wound_exudate_code_description,
     A.wound_dem_l_w_d,
     A.wound_size,
     A.snap_size_criteria_met,
     A.customer_sva_enabled_flag,
     A.snap_tier,
     A.va_system,
     A.order_status,
     A.therapy_days,
     A.nsl_name
FROM
(SELECT*
 FROM reporting_comops.omp_data_initial_layer) A
INNER JOIN
(SELECT *
 FROM reporting_comops.aim_order_rvp_dm_rep_names
 WHERE territory_code is not null) B
WHERE A.territory_code = B.territory_code
AND A.region = B.region;

compute stats reporting_comops.omp_data;
