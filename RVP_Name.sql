UNION ALL
SELECT 'RVP' sales_credit_type_code,
        region,
        CAST(NULL AS STRING) district,
        CAST(NULL AS STRING) territory_code,
        rvp_name,
        CAST(NULL AS STRING) dm_name,
        CAST(NULL AS STRING) salesrep_name,
        'REGION' rollup 
FROM aim.aim_customer_zipterr_xref_transposed_vw 
WHERE sales_credit_type_code IN ('TMV','TSV','ATR') 
AND territory_code NOT LIKE '%999%' AND region LIKE 'A%' AND region <>'A0'
GROUP BY 1,2,3,4,5,6,7,8;
compute stats reporting_comops.aim_order_rvp_dm_rep_names;