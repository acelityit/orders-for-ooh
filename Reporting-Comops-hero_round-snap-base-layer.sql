DROP TABLE IF EXISTS reporting_comops.hero_round_snap_base_layer;
CREATE TABLE reporting_comops.hero_round_snap_base_layer stored AS parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/reporting_comops.db/hero_round_snap_base_layer/' AS
SELECT * FROM reporting_comops.hero_round_snap_initial_layer WHERE jca_role in('TMV','TDV','TSV','ATR');
compute stats reporting_comops.hero_round_snap_base_layer;