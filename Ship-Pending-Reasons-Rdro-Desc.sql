
DROP TABLE IF EXISTS reporting_comops.ship_pending_reasons_rdro_desc;


CREATE TABLE reporting_comops.ship_pending_reasons_rdro_desc stored AS parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/reporting_comops.db/ship_pending_reasons_rdro_desc/' AS
SELECT rdro_rop_fk,
     rdro_rpos,
     rdro_description AS rdro_description,
     rdro_created_date
FROM hero.genesis_rop_delay_reason_option rdro
WHERE to_date(rdro_created_date)=
  (SELECT max(to_date(rdro_created_date))
   FROM hero.genesis_rop_delay_reason_option c
   WHERE c.rdro_rop_fk=rdro.rdro_rop_fk)
GROUP BY 1,2,3,4;

compute stats reporting_comops.ship_pending_reasons_rdro_desc;