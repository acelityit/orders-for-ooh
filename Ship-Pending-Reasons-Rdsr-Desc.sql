DROP TABLE IF EXISTS reporting_comops.ship_pending_reasons_rdsr_desc;


CREATE TABLE reporting_comops.ship_pending_reasons_rdsr_desc stored AS parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/reporting_comops.db/ship_pending_reasons_rdsr_desc/' AS
SELECT rdsr_rop_fk,
     rdsr_rpos,
     rdsr_description AS rdsr_description,
     rdsr_srpos,
     rdsr_created_date
FROM hero.genesis_rop_delay_sub_reason rdsr
WHERE to_date(rdsr_created_date)=
  (SELECT max(to_date(rdsr_created_date))
   FROM hero.genesis_rop_delay_sub_reason b
   WHERE b.rdsr_rop_fk=rdsr.rdsr_rop_fk)
GROUP BY 1,2,3,4,5;

compute stats reporting_comops.ship_pending_reasons_rdsr_desc;