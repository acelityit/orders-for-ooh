--===============================================================================
--  Imported into project on 7/30/2020
--  Ship pending reasons
--===============================================================================

DROP TABLE IF EXISTS reporting_comops.ship_pending_reasons_rdr_desc;


CREATE TABLE reporting_comops.ship_pending_reasons_rdr_desc stored AS parquet
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/reporting_comops.db/ship_pending_reasons_rdr_desc/' AS
SELECT rdr_rop_fk,
     rdr_rpos,
     rdr_description AS rdr_description,
     rdr_created_date
FROM hero.genesis_rop_delay_reason rdr
WHERE to_date(rdr_created_date)=
  (SELECT max(to_date(rdr_created_date))
   FROM hero.genesis_rop_delay_reason a
   WHERE a.rdr_rop_fk=rdr.rdr_rop_fk)
GROUP BY 1,2,3,4;

compute stats reporting_comops.ship_pending_reasons_rdr_desc;