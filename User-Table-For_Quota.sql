DROP TABLE IF EXISTS reporting_comops.userprofile_quota_table_2020;
CREATE TABLE reporting_comops.userprofile_quota_table_2020 stored 
AS
parquet location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/reporting_comops.db/userprofile_quota_table_2020/'
AS
SELECT 
CASE WHEN jca_role like 'AES%' THEN 'SAE' 
     WHEN jca_role like 'WCR%' THEN 'AE' 
     ELSE jca_role END AS jca_role,
CASE WHEN jca_role like 'AES%' THEN CONCAT('SAE',SUBSTR(territory_code,4,4))
     WHEN jca_role like 'WCR%' THEN CONCAT('AE',SUBSTR(territory_code,4,4))
     ELSE territory_code END AS territory_code,
       salesrep_name,
       t_userid,
       t_email_address,
       district,
       dm_name,
       d_userid,
       d_email_address,
       region,
CASE WHEN jca_role like 'AES%' THEN dm_name
     WHEN jca_role like 'WCR%' THEN dm_name
     ELSE rvp_name END AS rvp_name,
CASE WHEN jca_role like 'AES%' THEN d_userid
     WHEN jca_role like 'WCR%' THEN d_userid
     ELSE r_userid END AS r_userid,
       r_email_address,
       dt_userid
FROM reporting_comops.userprofile_round_table_2019;
compute stats reporting_comops.userprofile_quota_table_2020;
