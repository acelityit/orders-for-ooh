
DROP TABLE IF EXISTS reporting_comops.hero_snap;


CREATE TABLE reporting_comops.hero_snap stored AS parquet 
location 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/reporting_comops.db/hero_snap/' AS
SELECT rop.rop_ro_fk,
     rop.rop_pk,
     rop.rop_cst_fk,
     rop.rop_svc_fk,
     rop.ROP_PRODUCT_USED_FK,
     CASE
         WHEN rop_ar_serial_number_fk='null' THEN '-'
         WHEN rop_ar_serial_number_fk IS NULL THEN '-'
         ELSE rop_ar_serial_number_fk
     END rop_ar_serial_number_fk,
     nvl(rop.rop_status_code,
         '-') rop_status_code,
        nvl(cast(rop.rop_placed_time_stamp AS string),
            '-') rop_placed_time_stamp,
           nvl(cast(rop.rop_last_round_time_stamp AS string),
               '-') rop_last_round_time_stamp,
              nvl(cast(rop.rop_dc_time_stamp AS string),
                  '-') rop_dc_time_stamp,
                 CASE
                     WHEN rop_vac_transitionable_flag='null' THEN '-'
                     WHEN rop_vac_transitionable_flag IS NULL THEN '-'
                     ELSE rop_vac_transitionable_flag
                 END rop_vac_transitionable_flag,
                 CASE
                     WHEN rop_claims_release_emp_fk ='null' THEN '-'
                     WHEN rop_claims_release_emp_fk IS NULL THEN '-'
                     ELSE rop_claims_release_emp_fk
                 END CSR,
                 CASE
                     WHEN rop_consign ='null' THEN '-'
                     WHEN rop_consign IS NULL THEN '-'
                     ELSE rop_consign
                 END rop_consign,
                 nvl(cast(rop.rop_avail_for_ship_date AS string),
                     '-') rop_avail_for_ship_date,
                    nvl(cast(rop.rop_start_bill_time_stamp AS string),
                        '-') rop_start_bill_time_stamp,
                       nvl(cast(rop.rop_void_verify_time_stamp AS string),
                           '-') rop_void_verify_time_stamp,
                          nvl(cast(rop.rop_stop_bill_time_stamp AS string),
                              '-') rop_stop_bill_time_stamp,
                             genesis_mtl_system_items_snap.description AS item_description,
                             genesis_mtl_system_items_snap.inventory_item_id,
                             genesis_rental_order_prod_accessory.roa_pm_fk,
                             nvl(cast(genesis_rental_order_prod_accessory.roa_ship_date AS string),
                                 '-') 'Ship_date',
                                      rop.rop_void_type_code,
                                      rop.rop_void_subreason_code,
                                      genesis_mtl_oracle_categories_mv.description,
                                      rop.rop_created_time_stamp,
                                      CASE
                                          WHEN rop.rop_avail_for_ship_date IS NULL
                                               AND rop.rop_void_verify_time_stamp IS NULL THEN 'PENDING'
                                          WHEN rop.rop_created_time_stamp IS NOT NULL
                                               AND rop.rop_void_verify_time_stamp IS NULL
                                               AND rop.rop_stop_bill_time_stamp IS NULL THEN 'PLACED'
                                          WHEN rop.rop_void_verify_time_stamp IS NOT NULL THEN 'CANCELLED'
                                          WHEN rop.rop_created_time_stamp IS NOT NULL
                                               AND rop.rop_void_verify_time_stamp IS NULL
                                               AND rop.rop_stop_bill_time_stamp IS NOT NULL THEN 'CLOSED'
                                      END order_status,
                                      CASE
                                          WHEN rop.rop_free_reason_code IN ('60',
                                                                            '45') THEN 'Y'
                                          ELSE 'N'
                                      END charity_care_Status,
                                      CASE
                                          WHEN rop.rop_created_time_stamp>rop.rop_avail_for_ship_date THEN 'Rebill'
                                          ELSE '-'
                                      END Rebill
FROM hero.genesis_rental_order_product rop,
   hero.genesis_mtl_system_items_snap,
   hero.genesis_rental_order_prod_accessory,
   hero.genesis_mtl_oracle_categories_mv
WHERE (rop.rop_pk = genesis_rental_order_prod_accessory.roa_rop_fk)
AND (genesis_mtl_system_items_snap.inventory_item_id= genesis_rental_order_prod_accessory.roa_inventory_item_id)
AND (genesis_mtl_system_items_snap.organization_id= 1)
AND (genesis_mtl_oracle_categories_mv.category_id = genesis_rental_order_prod_accessory.roa_item_category_id)
AND (genesis_rental_order_prod_accessory.roa_pm_fk IN ('SKTA10X10',
                                                       'SKTA15X15',
                                                       'BKTF14X11',
                                                       'BKTF14X11S',
                                                       'BKTF14X11S/10',
                                                       'BKTF14X11/10',
                                                       'SKTF14X34',
                                                       'SKTF14X34/10',
                                                       'SNPA100US',
                                                       'SNPA125US',
                                                       'SNPA075US',
                                                       'SNPA125PLUS',
                                                       'STPALP',
                                                       'STPAMP',
                                                       'STPASP',
                                                       'SRNG10',
                                                       'SKTF10X10',
                                                       'SKTF15X15',
                                                       'SKTF15X15/10',
                                                       'SKTF20X20',
                                                       'STPAL',
                                                       'STPAM',
                                                       'STPAS',
                                                       'SNPA100US/10',
                                                       'SNPA125US/10',
                                                       'SNPA075US/10',
                                                       'SNPA125PLUS/10',
                                                       'SK
    TF10X10/10',
                                                       'SKTF20X20/10')
     AND rop.rop_created_time_stamp >= '2018-01-01')
GROUP BY 1,	2,	3,	4,	5,	6,	7,	8,	9,	10,	11,	12,	13,	14,	15,	16,	17,	18,	19,	20,	21,	22,	23,	24,	25,	26,	27,	28;

compute stats reporting_comops.hero_snap;